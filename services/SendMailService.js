const nodemailer = require("nodemailer");

module.exports = {
  send: products => {
    return new Promise((resolve, reject) => {
      let transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "",
          pass: ""
        }
      });

      var receiver_list = [
        { email: "" },
        { email: "" },
        { email: "" }
      ];

      var list = "";
      var counter = 1;
      const product_list = products.map(x => {
        list += counter + ". " + x.name + "<br>";
        counter++;
      });
      //   const send_list = [];
      for (let send of receiver_list) {
        let mailOptions = {
          from: '', // sender address
          to: send.email, // list of receivers
          subject: "[WISHSTOCK] Daily products update list.", // Subject line
          html: list // html body
        };
        // .sendMail is not wait until it finish
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject(error);
          }
          //   send_list.push(info.messageId);
        });
      }
      resolve({ message: "sent" });
    });
  }
};
