const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id)
        .then(user => {
            done(null, user);
        })
})

passport.use(
    new GoogleStrategy({
        clientID: config.googleClientID,
        clientSecret: config.googleClientSecret,
        callbackURL: '/auth/google/callback',
        proxy: true
    }, 
    async (accessToken,refreshToken, profile, done) => {
        const existingUser = await User.findOne({googleId: profile.id});
        if(existingUser){
            console.log(existingUser);
            //we already have a record with the given profile ID
            console.log('exist');
            return done(null, existingUser);
        }
        console.log(profile);
        const user = await new User({googleId: profile.id, fullName: profile.displayName}).save()
        return done(null, user);
        
    })
);
