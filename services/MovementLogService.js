const { WS_Movement_Logs_Flat } = require("../models");
module.exports = {
  //Param detail
  /**
   * order_info: {
   *  order_prestashop_id
   *  order_status
   *  version_number //will be 1 by default
   * }
   * admin_info: {
   *  id,
   *  name
   * }
   * order_detail: {
   *  product_name,
   *  product_quantity,
   *  product_reference,
   *  product_barcode,
   *  is_set,
   *  product_parent_id,
   *  scan_quantity,
   *  scan_result
   * }
   */
  create: (order_detail, admin_info) => {
    return new Promise((resolve, reject) => {
      for (let detail of order_detail) {
        detail.user_id = admin_info.id;
        detail.user_name = admin_info.name;
      }
      WS_Movement_Logs_Flat.bulkCreate(order_detail).then(moment_list => {
        resolve(moment_list);
      });
    });
  }
};
