const passport = require('passport');
const jwtStrategy = require('passport-jwt').Strategy;
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
const { Admin } = require('../models');
passport.serializeUser((admin, done) => {
    console.log('se')
    done(null, admin.id);
});

passport.deserializeUser((id, done) => {
    console.log('de')
    Admin.findById(id)
        .then(admin => {
            done(null, admin);
        })
})

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.jwtSecret;
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'localhost';
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    console.log('check', jwt_payload);;
    Admin.findById({id: jwt_payload.id}, 'fullName, email', function(err, employee) {
        if (err) {
            return done(err, false);
        }
        if (employee) {
            return done(null, employee);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));