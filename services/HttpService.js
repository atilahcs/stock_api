module.exports = {
    request: (data, url, method = 'get', header_key = null)=>{
        return new Promise((resolve, reject)=>{
            let request = require('request');
            let get_params = '?';
            if (method.toLowerCase() == 'get') {
                for (let key in data) {
                    if (get_params == '?')
                        get_params += key+'='+data[key];
                    else
                        get_params += '&'+key+'='+data[key];
                }
                url += get_params;
            }
            let headers = {
                "Content-Type": "application/json",
                "Content-Length": data.length
            }
            if (header_key)
                for (let key in header_key)
                    headers[key] = header_key[key]
            request({
                url: url,
                method: method,
                json: true,
                headers: headers,
                body: data
            },(err, response, body)=>{
                if (err || response.statusCode != 200) {
                    let message = "Something went wrong with request";
                    if (typeof err == 'string') message = err;
                    if (err) message = err.message;
                    if (body) message = body.message;
                    reject({ message });
                } else {
                    resolve(body);
                }
            })
        });
    }
}