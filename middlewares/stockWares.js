const sequelize = require('sequelize');
const { WS_Product, ProductQuantity, WS_Movement_Logs_Flat } = require('../models');
module.exports = {
    run: async (dataArray)=> {
        return new Promise(async (resolve, reject)=>{
            console.log(dataArray.page);
             for(let i = 1; i < dataArray.totalPage; i++){
                pageNumber = i * 3000;
                dataArray.page = i;
                let product = await WS_Product.findAll({
                    subQuery: false,
                    distinct: true,
                    include: [
                        { 
                            model: ProductQuantity,
                            where:{ location_id: 1}
                        }, 
                        { 
                            model: WS_Movement_Logs_Flat,
                            limit: 1,
                            required: false,
                            where: { 
                                createdAt: {[sequelize.Op.lt]: '2017-08-13'}
                            }, 
                            order: [['id', 'DESC']]
                        }
                    ],                  
                    offset: pageNumber, 
                    limit: 3000,
                });
                dataArray.data.push(product);
             };
            resolve(dataArray);
        });
    }
}