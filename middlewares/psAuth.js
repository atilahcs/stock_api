const { ErrorLog, AdminHistory } = require('../models');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
module.exports = async (req, res, next) => {
  let { ps_key } = req.headers;
  let { admin_id = 0, admin_firstname = '', admin_lastname = '', admin_email = '', admin_profile = 0, action = '' } = req.headers;
  await AdminHistory.create({
    admin_ps_id: admin_id,
    admin_email,
    admin_firstname,
    admin_lastname,
    admin_level: admin_profile,
    agent: 'Request from Prestashop',
    action: action + req.method + ' ' + req.originalUrl,
    created_time: new Date(),
    updated_time: new Date()
  });
  if (ps_key == config.PS_KEY) {
    return next();
  } else {
    let param = {
      headers: req.headers
    }
    await ErrorLog.create({
      controller_name: 'middlewares/psAuth',
      path: req.originalUrl,
      method: req.method,
      message: 'Unauthorized.',
      params: JSON.stringify(param),
      created_time: new Date()
    });
    return res.status(500).json({ error: 'Unauthorized.' });
  }
}