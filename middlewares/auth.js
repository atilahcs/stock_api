const { ErrorLog, AdminHistory, AdminToken } = require('../models');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
const jwt = require('jsonwebtoken');
module.exports = async (req, res, next) => {
  try {
    await AdminHistory.create({
      admin_ps_id: req.headers['admin-id'],
      admin_email: req.headers['admin-email'],
      admin_firstname: req.headers['admin-firstname'],
      admin_lastname: req.headers['admin-lastname'],
      admin_level: req.headers['admin-level'],
      agent: 'Request from Prestashop',
      action: req.headers['action'] + ' ' + req.method + ' ' + req.originalUrl,
      created_time: new Date(),
      updated_time: new Date()
    });
    const admin_token = await AdminToken.findOne({ where: { token: req.headers['admin-token'] }});
    if (!admin_token)
      throw { status: 401, message: 'Unauthorized.' };
    const decoded = await jwt.verify(req.headers['admin-token'], config.JWT_SECRET);
    if (decoded.eml != req.headers['admin-email'] || decoded.fn != req.headers['admin-firstname']
    || decoded.ln != req.headers['admin-lastname'] || decoded.psid != Number(req.headers['admin-id']))
      throw { status: 401, message: 'Invalid token' };
    return next();
  } catch (err) {
    let status = (err && err.status) ? err.status : 500;
    let message = (err && err.message) ? err.message : 'Something went wrong.';
    let param = {
        header: req.headers
    }
    await ErrorLog.create({
        controller_name: 'authentication/authAdminController',
        action_name: 'logout',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
    });
    return res.status(status).json({ error: message });
  }
};