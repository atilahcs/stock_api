const express = require("express");
// const mongoose = require("mongoose");
const Sequelize = require("sequelize");
const cookieSession = require("cookie-session");
const bodyParser = require("body-parser");
const passport = require("passport");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/config/config.json")[env];
var cors = require("cors");

// require('./models/logs/UserLoginLog');
require("./services/passport-jwt");

// mongoose.connect(config.mongoURI);

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [config.cookieKey]
  })
);
app.use(passport.initialize());
app.use(passport.session());

require("./routes/index")(app);
// if (process.env.NODE_ENV === 'production') {
//     app.use(express.static('client/build'));

//     const path = require('path');
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//     });
// }

console.log(
  `
                        .8 
                      .888
                    .8888'
                   .8888'
                   888'
                   8'
      .88888888888. .88888888888.
   .8888888888888888888888888888888.
 .8888888888888888888888888888888888.
.&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@:
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@:  Wishstock 2019
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%.
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%.
 '00000000000000000000000000000000000'
  '000000000000000000000000000000000'
   '000000000000000000000000000000'
     '###########################'
      '#######################'
         '#########''########'
           '""""""'  '"""""'
`
);
console.log("\x1b[36m%s\x1b[0m", config.database);
console.log(new Date());
const PORT = process.env.PORT || 5000;
app.listen(PORT);
