const OrderController = require("../controllers/order/OrderController");
const auth = require('../middlewares/auth');

module.exports = app => {
  app.get("/order", auth, OrderController.getList);
  app.get("/order/order-scan", auth, OrderController.order_scan.getList);
  app.get("/order/order-scan-movement", auth, OrderController.order_scan_movement.getList);
  app.get("/order/order-scan-version", auth, OrderController.order_scan_version.getList);
  app.get("/order/ps/:order_ps_id", auth, OrderController.getPsOrderDetail);
  app.get("/order/:order_ps_id", auth, OrderController.getDetail);
  app.get("/order/:order_ps_id/order-scan", auth, OrderController.order_scan.getDetail); //change to get detail due to it's one to one
  app.get("/order/:order_ps_id/order-scan-movement", auth, OrderController.order_scan_movement.getListByOrderId);
  app.get("/order/:order_ps_id/order-scan-version", auth, OrderController.order_scan_version.getListByOrderId);
  app.get("/order/:order_ps_id/movement", auth, OrderController.movement.getListByOrderId);
  app.get("/order/:order_ps_id/created", auth, OrderController.checkCreated);

  app.post('/order', auth, OrderController.create);

  app.put('/order/:order_ps_id', auth, OrderController.update);
};
