const StockCountController = require('../controllers/stockcount/StockCountController');
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get('/stockcount', auth, StockCountController.getList);
  app.get('/stockcount/:stock_count_id', auth, StockCountController.getDetail);

  app.post('/stockcount', auth, StockCountController.create);
  app.post('/stockcount/:stock_count_id/adjustment', auth, StockCountController.adjustment.create);
  app.post('/stockcount/:stock_count_id/adjust', auth, StockCountController.adjustment.adjust);

  app.put('/stockcount/:stock_count_id', auth, StockCountController.update);
  app.delete('/stockcount/:stock_count_id', auth, StockCountController.delete);
}