const MovementController = require('../controllers/MovementController');
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get('/movement', auth, MovementController.getList);
}