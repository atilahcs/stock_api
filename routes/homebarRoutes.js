const HomebarController = require('../controllers/homebar/HomebarController');
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get('/homebar', auth, HomebarController.getList);
  app.get('/homebar/barorder', auth, HomebarController.barorder.getList);
  app.get('/homebar/:homebar_id', auth, HomebarController.getDetail);
  app.get('/homebar/:homebar_id/barorder', auth, HomebarController.barorder.getListByBarId);
  app.get('/homebar/:homebar_id/barorder/:barorder_id', auth, HomebarController.barorder.getDetail);

  app.post('/homebar', auth, HomebarController.create);
  app.post('/homebar/:homebar_id/barorder', auth, HomebarController.barorder.create);

  app.put('/homebar/:homebar_id', auth, HomebarController.update);
  app.put('/homebar/:homebar_id/barorder/:barorder_id', auth, HomebarController.barorder.update);
  app.put('/homebar/:homebar_id/barorder/:barorder_id/status', auth, HomebarController.barorder.updateOrderStatus);

  app.delete('/homebar/:homebar_id', auth, HomebarController.delete);
  app.delete('/homebar/:homebar_id/barorder/:barorder_id', auth, HomebarController.barorder.delete);
}