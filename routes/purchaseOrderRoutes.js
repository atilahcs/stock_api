const PurchaseOrderController = require("../controllers/purchaseorder/PurchaseOrderController");
const auth = require('../middlewares/auth');

module.exports = app => {
    app.get("/po/:po_id", auth, PurchaseOrderController.getDetail);
    app.get("/po/:po_id/pdf", auth, PurchaseOrderController.pdf.generate);
    app.get("/po/:po_id/ro", auth, PurchaseOrderController.receiveorder.getRoList);
    app.get("/po/:po_id/log", auth, PurchaseOrderController.log.getList);
    app.get("/po", auth, PurchaseOrderController.getList);
    app.get('/po/email/sign-in', PurchaseOrderController.email.gmailSignin);
    app.get('/po/email/authorize', PurchaseOrderController.email.gmailAuthorize);
    
    app.post("/po", auth, PurchaseOrderController.create);
    app.post("/po/:po_id/email", auth, PurchaseOrderController.email.sendEmail);
    
    app.put("/po/:po_id", auth, PurchaseOrderController.update);
    app.put("/po/:po_id/status", auth, PurchaseOrderController.updatePoStatus);
    
    app.delete("/po/:po_id", auth, PurchaseOrderController.delete);
};