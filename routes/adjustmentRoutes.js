const AdjustmentController = require("../controllers/adjustment/AdjustmentController");
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get("/adjustment", auth, AdjustmentController.getList);
  app.get("/adjustment/:adjustment_id", auth, AdjustmentController.getDetail);

  app.post("/adjustment", auth, AdjustmentController.create);

  app.put("/adjustment/:adjustment_id", auth, AdjustmentController.update);
  app.put(
    "/adjustment/:adjustment_id/save", auth,
    AdjustmentController.saveAdjustment
  );
  app.put("/adjustment/:adjustment_id/adjust", auth, AdjustmentController.adjust);
  app.delete("/adjustment/:adjustment_id", auth, AdjustmentController.delete);
};
