const BranchLocationController = require('../controllers/BranchLocationController');
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get('/location', auth, BranchLocationController.getList);
  app.get('/location/:location_id', auth, BranchLocationController.getDetail);

  app.post('/location', auth, BranchLocationController.create);
  
  app.put('/location/:location_id', auth, BranchLocationController.update);

  app.delete('/location/:location_id', auth, BranchLocationController.delete);
}