const PrestashopController = require('../controllers/prestashop/IndexController');
const psAuth = require('../middlewares/psAuth');
module.exports = app => {
  app.post('/ps/supplier', psAuth, PrestashopController.supplier.create);
  app.put('/ps/supplier/:supplier_id', psAuth, PrestashopController.supplier.update);

  app.post('/ps/product', psAuth, PrestashopController.product.create);
  app.put('/ps/product/:product_id', psAuth, PrestashopController.product.update);
  app.put('/ps/product/:product_id/image', psAuth, PrestashopController.product.updateProductImage);
  app.put('/ps/product/:product_id/supplier', psAuth, PrestashopController.product.updateSupplier);
}