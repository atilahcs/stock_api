const ReceiveOrderController = require("../controllers/receiveorder/ReceiveOrderController");
const auth = require('../middlewares/auth');
const multer = require('multer');
const uploadStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'assets/img');
    },
    filename: (req, file, cb) => {
        let current_time = new Date();
        let file_name = file.originalname.split('.');
        let file_length = file_name.length;
        let { ro_id } = req.params;
        cb(null, 'RO_' + ro_id + '_' + current_time.getDate() + '-' +
            (current_time.getMonth() + 1) + '-' + current_time.getFullYear() + '_' +
            current_time.getHours() + '-' + current_time.getMinutes() + '-' +
            current_time.getSeconds() + '.' + file_name[file_length - 1]);
    }
});
const upload = multer({ storage: uploadStorage });

module.exports = app => {
    app.get("/ro/:ro_id", auth, ReceiveOrderController.getDetail);
    app.get("/ro/:ro_id/file", auth, ReceiveOrderController.file.getFileList);
    // app.get("/ro/:ro_id/file/:file_name", ReceiveOrderController.file.getFileList);
    app.get("/ro", auth, ReceiveOrderController.getList);

    app.post("/ro", auth, ReceiveOrderController.create);
    app.post("/ro/:ro_id/file", auth, upload.single('ro_file'), ReceiveOrderController.file.uploadFile);

};