const ProductController = require("./../controllers/product/ProductController");
const auth = require('../middlewares/auth');

module.exports = app => {
  app.get("/product", auth, ProductController.getList);
  // app.get("/product/import/first", ProductController.createOneTime);

  app.get("/product/barcode/:product_barcode", auth, ProductController.getDetailByBarcode);
  app.get("/product/:product_id", auth, ProductController.getDetail);
  app.get("/info/product", auth, ProductController.getDashboard);
  app.get(
    "/product/sync/all", auth,
    ProductController.quantity.syncQuantityToPrestashop
  );
  app.get(
    "/product/supplier/ps", auth,
    ProductController.productsupplier.getProductSupplierFromPs
  );
  app.get("/product/removed/ps", auth, ProductController.getRemovedFromPs);
  // app.get("/product/update/info", ProductController.updateInfoFromPs);
  // app.get("/product/movement", ws_productController.movement.get);
  // app.post("/product/movement/search", ws_productController.movement.search);

  app.get(
    "/product/:product_id/location", auth,
    ProductController.location.getLocationList
  );
  app.get(
    "/product/:product_id/location/:location_id", auth,
    ProductController.location.getLocation
  );
  app.get(
    "/product/:product_id/location/:location_id/movement", auth,
    ProductController.location.getLocationMovement
  );
  app.get("/product/:product_id/po", auth, ProductController.po.getPoList);
  app.get("/product/:product_id/ro", auth, ProductController.ro.getRoList);

  app.post(
    "/product/:product_id/location/:location_id", auth,
    ProductController.location.addProductLocation
  );
  app.post("/product/search", auth, ProductController.search);

  app.put(
    "/product/add-qty/location/:location_id", auth,
    ProductController.quantity.addQuantityList
  );
  app.put(
    "/product/remove-qty/location/:location_id", auth,
    ProductController.quantity.removeQuantityList
  );
  app.put(
    "/product/:product_id/add-qty/location/:location_id", auth,
    ProductController.quantity.addQuantity
  );
  app.put(
    "/product/:product_id/remove-qty/location/:location_id", auth,
    ProductController.quantity.removeQuantity
  );
  app.put("/product/:product_id/barcode", auth, ProductController.updateProductBarcode);
  app.put("/product", auth, ProductController.update);

  app.delete("/product", auth, ProductController.delete);
  app.delete(
    "/product/:product_id/location/:location_id", auth,
    ProductController.location.removeProductLocation
  );
  //these are not made for real use
  //for urgent case only!!!
  // DON'T DO IT WHEN U R DRUNK!!!
  // app.put('/product/:product_id/adjust-qty/location/:location_id', ProductController.quantity.adjustQuantity);
  // app.get('/product/:product_id/quantity/reset-qty/location/:location_id', ProductController.po.resetAllQuantityAtLocation);
  // app.get('/product/:product_id/quantity/reset-qty', ProductController.po.resetAllQuantity);
};
