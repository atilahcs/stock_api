const stockController = require("../controllers/stock/StockController");
const stockLogController = require("../controllers/stock/LogController");
const purchaseOrderController = require("../controllers/stock/PurchaseOrderController");
const auth = require('../middlewares/auth');

module.exports = app => {
  app.get("/", auth, stockController.run);
  app.get("/report", auth, stockController.getReportByDate);
  app.get("/reportbetween", auth, stockController.getMovementReport);
  app.get("/stocks/logs", auth, stockLogController.get);

  app.get("/report/po/all", auth, purchaseOrderController.getReportPurchaseOrderAll);
};
