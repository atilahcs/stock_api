const ActionController = require('../controllers/ActionController');
const auth = require('../middlewares/auth');
module.exports = app => {
  app.get('/action', auth, ActionController.getList);
  app.get('/action/:action_id', auth, ActionController.getDetail);

  app.post('/action', auth, ActionController.create);
  
  app.put('/action/:action_id', auth, ActionController.update);

  app.delete('/action/:action_id', auth, ActionController.delete);
}