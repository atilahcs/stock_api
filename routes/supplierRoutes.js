const SupplierController = require("../controllers/supplier/SupplierController");
const auth = require('../middlewares/auth');

module.exports = app => {
  app.get('/supplier/:supplier_id', auth, SupplierController.getDetail);
  app.get('/supplier', auth, SupplierController.getList);
  app.get("/supplier/:supplier_id/product", auth, SupplierController.product.getList);
  app.get("/supplier/:supplier_id/po", auth, SupplierController.po.getList);
  app.get("/supplier/:supplier_id/ro", auth, SupplierController.ro.getList);

  app.post("/supplier", auth, SupplierController.create);

  app.get("/supplier/ps", auth, SupplierController.product.getProductFromPs);
  app.get(
    "/supplierproduct/update/pidsid", auth,
    SupplierController.product.updateProductIdAndSupplierId
  );
  app.put("/supplier/:supplier_id/product", auth, SupplierController.product.update);
  app.put("/supplier/:supplier_id", auth, SupplierController.update);
};
