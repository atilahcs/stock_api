const productQuantityController = require("../controllers/product/ProductQuantityController");
const auth = require('../middlewares/auth');

module.exports = app => {
  //   app.get("/product", ws_productController.get);
  app.put("/productquantity/update", auth, productQuantityController.update);
};
