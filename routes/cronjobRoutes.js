const CronjobController = require('../controllers/cron/IndexController');
module.exports = app => {
  app.get('/cron/product/import', CronjobController.product.importAll);
  app.get('/cron/product/update/info', CronjobController.product.updateInfoFromPs);
  app.get("/cron/product/import/last", CronjobController.product.importLatest);

  app.get("/cron/supplier/import", CronjobController.supplier.importAll);
  app.get("/cron/supplier/import/last", CronjobController.supplier.importLatest);

  app.get("/cron/supplier/product/import", CronjobController.supplierProduct.importAllSupplierProduct);
}