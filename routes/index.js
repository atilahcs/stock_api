module.exports = app => {
  require("./stockRoutes")(app);
  require("./productRoutes")(app);
  require("./supplierRoutes")(app);
  require("./orderRoutes")(app);
  require("./productQuantityRoutes")(app);
  require("./purchaseOrderRoutes")(app);
  require("./receiveOrderRoutes")(app);
  require("./actionRoutes")(app);
  require("./branchLocationRoutes")(app);
  require("./movementRoutes")(app);
  require("./adjustmentRoutes")(app);
  require("./supplierRoutes")(app);
  require("./stockCountRoutes")(app);
  require("./homebarRoutes")(app);
  require("./authRoutes")(app);
  require("./prestashopRoutes")(app);
  require("./cronjobRoutes")(app);

  const StateController = require('../controllers/StateController');
  app.get('/state', StateController.getList);
};
