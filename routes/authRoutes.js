const AuthController = require('../controllers/authentication/authController');
module.exports = app => {
  app.post('/auth/login', AuthController.login);
  app.post('/auth/logout', AuthController.logout);
}