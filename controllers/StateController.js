const { ErrorLog } = require('../models');
const HttpService = require("../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
module.exports = {
  getList: async (req, res) => {
    try {
      let data = {
        country_id: 206
      }
      let header = {
        'api_key': config.ADMIN_API_KEY
      }
      const ps_result = await HttpService.request(data, config.wbUrl + "", 'get', header);
      if (ps_result.error)
        throw { message: ps_result.message }
      return res.status(200).json({ state_list: ps_result.state_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "prestashop/StateController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}