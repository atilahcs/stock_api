const pdPdfHelper = require('../../helpers/poPdfHelper');
const { PurchaseOrder, PurchaseOrderDetail, ErrorLog } = require('../../models');
const PdfPrinter = require('pdfmake');
module.exports = {
  generate: async (req, res) => {
    let { po_id } = req.params;
    try {
      const po_result = await PurchaseOrder.findOne({ where: { id: po_id }, include: [{ model: PurchaseOrderDetail }] });
      if (!po_result)
        throw 'Purchase Order not found.'

      const pdf_content = await pdPdfHelper.generateContent(po_id, po_result, po_result.PurchaseOrderDetails);
      let po_no = '00000' + po_id;
      let po_file_name = 'po_' + po_no.slice(-6);

      return await pdPdfHelper.sendFile(po_file_name, pdf_content, res);
      //download file, not working with postman
      // return res.status(200).download(`${po_file_location}${po_file_name}.pdf`, `${po_file_name}.pdf`);
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/PdfController',
        action_name: 'generate',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}