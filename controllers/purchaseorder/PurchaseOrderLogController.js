const { PurchaseOrderLog, ErrorLog } = require('../../models');
const filterHelper = require("../../helpers/filterHelper");
module.exports = {
  getList: async (req, res) => {
    let { po_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      filter += `po_id:${po_id}`;
      let filter_clause = await filterHelper(filter);
      const count = await PurchaseOrderLog.count({ where: filter_clause });
      const purchase_order_log_list = await PurchaseOrderLog.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, purchase_order_log_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/PurchaseOrderLogController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}