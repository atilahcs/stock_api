const { PurchaseOrder, ReceiveOrder, ReceiveOrderDetail, ErrorLog
} = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
module.exports = {
  getRoList: async (req, res) => {
    let { po_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.body;
    let { filter = '' } = req.body;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      filter += `;po_id:${po_id}`;
      let filter_clause = await filterHelper(filter);
      const count = await ReceiveOrder.count({ where: { po_id } });
      const receive_order_list = await ReceiveOrder.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, receive_order_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/ReceiveOrderController',
        action_name: 'getRoList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}