const {
  PurchaseOrder,
  PurchaseOrderDetail,
  PurchaseOrderLog,
  ErrorLog
} = require("../../models");
const model = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
const EmailController = require("./EmailController");
const PdfController = require("./PdfController");
const ReceiveOrderController = require("./ReceiveOrderController");
const PurchaseOrderLogController = require("./PurchaseOrderLogController");
module.exports = {
  getDetail: async (req, res) => {
    let { po_id } = req.params;
    try {
      let purchase_order = await PurchaseOrder.findOne({
        where: { id: po_id },
        include: [{ model: PurchaseOrderDetail }]
      });

      if (!purchase_order) throw { message: "PO not found." };

      return res.status(200).json({ purchase_order });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "purchaseorder/PurchaseOrderController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await PurchaseOrder.count({ where: filter_clause });
      const purchase_order_list = await PurchaseOrder.findAll({
        offset: offset,
        limit: limit < 0 ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, purchase_order_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "purchaseorder/PurchaseOrderController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let { po_info, po_detail, admin_info } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      if (!po_info || !po_detail) throw { message: "Information missing." };
      if (!po_info.payment_type)
        throw { message: "No payment method selected." };

      let po_info_model = {
        sid: po_info.sid,
        supplier_id: Number(po_info.supplier_id),
        supplier_name: po_info.supplier_name,
        address: po_info.address,
        tax_id: po_info.tax_id,
        contact_name: po_info.contact_name,
        contact_email: po_info.contact_email,
        contact_no: po_info.contact_no,
        delivery_address_id: po_info.delivery_address_id,
        delivery_address_name: po_info.delivery_address_name,
        delivery_address: po_info.delivery_address,
        purchaser_name: po_info.purchaser_name,
        purchaser_email: po_info.purchaser_email,
        purchaser_no: po_info.purchaser_no,
        payment_type: po_info.payment_type,
        total_price: po_info.total_price,
        inc_tax: po_info.inc_tax,
        status: "Waiting",
        final_price: po_info.final_price,
        internal_remark: po_info.internal_remark,
        remark: po_info.remark,
        created_user: admin_info.admin_name,
        order_date: po_info.order_date || new Date(),
        expect_delivery_date: po_info.expect_delivery_date,
        credit_date: po_info.credit_date ? Number(po_info.credit_date) : null,
        created_time: new Date(),
        updated_time: new Date()
      };
      const created_po = await PurchaseOrder.create(po_info_model, {
        transaction
      });

      for (let detail of po_detail) {
        detail.po_id = created_po.id;
        (detail.created_time = new Date()), (detail.updated_time = new Date());
      }
      await PurchaseOrderDetail.bulkCreate(po_detail, { transaction });

      await PurchaseOrderLog.create(
        {
          po_id: created_po.id,
          admin_id: admin_info.admin_id,
          admin_name: admin_info.admin_name,
          action: "Create",
          detail:
            admin_info.admin_name +
            " create po: " +
            created_po.id +
            ", supplier: " +
            po_detail.supplier_name,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );

      await transaction.commit();
      const po_result = await PurchaseOrder.findOne({
        where: { id: created_po.id },
        include: [{ model: PurchaseOrderDetail }]
      });
      return res.status(200).json({ purchase_order: po_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "purchaseorder/PurchaseOrderController",
        action_name: "create",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { po_id } = req.params;
    let transaction = await model.sequelize.transaction();
    try {
      let po_result = await PurchaseOrder.findOne(
        { where: { id: po_id } }
      );
      if (!po_result) throw { message: "Purchase Order does not exist." };
      let { po_info, po_detail, admin_info } = req.body;
      if (!po_info.payment_type)
        throw { message: "No payment method selected" };

      let po_info_model = {
        sid: po_info.sid,
        supplier_id: po_info.supplier_id,
        supplier_name: po_info.supplier_name,
        address: po_info.address,
        tax_id: po_info.tax_id,
        contact_name: po_info.contact_name,
        contact_no: po_info.contact_no,
        contact_email: po_info.contact_email,
        delivery_address_id: po_info.delivery_address_id,
        delivery_address_name: po_info.delivery_address_name,
        delivery_address: po_info.delivery_address,
        purchaser_name: po_info.purchaser_name,
        purchaser_no: po_info.purchaser_no,
        purchaser_email: po_info.purchaser_email,
        payment_type: po_info.payment_type,
        total_price: po_info.total_price,
        inc_tax: po_info.inc_tax,
        final_price: po_info.final_price,
        internal_remark: po_info.internal_remark,
        remark: po_info.remark,
        latest_updated_user: admin_info.admin_name,
        order_date: po_info.order_date || new Date(),
        expect_delivery_date: po_info.expect_delivery_date,
        credit_date: po_info.credit_date ? Number(po_info.credit_date) : null,
        updated_time: new Date()
      };

      const updated_po = await PurchaseOrder.update(po_info_model, {
        where: { id: po_id },
        transaction
      });
      for (let detail of po_detail) {
        detail.po_id = po_id;
        detail.created_time = new Date();
        detail.updated_time = new Date();
        delete detail.id;
      }
      await PurchaseOrderDetail.destroy({
        where: { po_id: po_id },
        transaction
      });
      await PurchaseOrderDetail.bulkCreate(po_detail, { transaction });

      await PurchaseOrderLog.create(
        {
          po_id: po_id,
          admin_id: admin_info.admin_id,
          admin_name: admin_info.admin_name,
          action: "Update",
          detail:
            admin_info.admin_name +
            " update po: " +
            updated_po[0].id +
            ", supplier: " +
            updated_po[0].supplier_name,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );

      await transaction.commit();
      const updated_po_result = await PurchaseOrder.findOne({
        where: { id: po_id },
        include: [{ model: PurchaseOrderDetail }]
      });
      return res.status(200).json({ purchase_order: updated_po_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "purchaseorder/PurchaseOrderController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let { po_id } = req.params;
    let transaction = await model.sequelize.transaction();
    try {
      const destroy_result = await PurchaseOrder.destroy({
        where: { id: po_id },
        transaction
      });
      await PurchaseOrderDetail.destroy({
        where: { po_id: po_id },
        transaction
      });
      await transaction.commit();
      return res.status(200).json({ deleted: destroy_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "purchaseorder/PurchaseOrderController",
        action_name: "delete",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  updatePoStatus: async (req, res) => {
    let { po_id } = req.params;
    let { status, admin_info } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      if (!status) throw { message: "Parameter missing" };

      let status_temp = status.toLowerCase();
      let status_array = ["waiting", "approved", "sent", "cancel"];
      if (!status_array.includes(status_temp))
        throw { message: "Unable to update" };

      let updated_obj = {
        status: status,
        updated_time: new Date()
      };
      if (status_temp == "approved") {
        let allow_user = [
          "jerome@wishbeer.com",
          "kim@wishbeer.com",
          "praew@wishbeer.com",
          "jump@wishbeer.com"
        ];
        if (allow_user.includes(admin_info.admin_email))
          updated_obj.approved_user = admin_info.admin_name;
        else throw { message: "You are not authorized!" };
      }

      const update_result = await PurchaseOrder.update(updated_obj, {
        where: { id: po_id },
        transaction
      });
      if (!update_result) throw { message: "Purchase Order not found" };

      const po_result = await PurchaseOrder.findOne({ where: { id: po_id } });

      await PurchaseOrderLog.create(
        {
          po_id: po_id,
          admin_id: admin_info.admin_id,
          admin_name: admin_info.admin_name,
          action: "Update status",
          detail:
            admin_info.admin_name +
            " update status: " +
            status +
            ", po: " +
            po_id +
            ", supplier: " +
            po_result.supplier_name,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );
      await transaction.commit();
      return res.status(200).json(true);
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "purchaseOrder/PurchaseOrderController",
        action_name: "updatePoStatus",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  email: EmailController,
  pdf: PdfController,
  receiveorder: ReceiveOrderController,
  log: PurchaseOrderLogController
};
