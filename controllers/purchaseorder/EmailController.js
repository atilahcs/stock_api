const { PurchaseOrder, PurchaseOrderDetail, ErrorLog } = require("../../models");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const poPdfHelper = require('../../helpers/poPdfHelper');
const emailSenderHelper = require('../../helpers/emailSenderHelper');
module.exports = {
  gmailSignin: async (req, res) => {
    try {
      const { google } = require('googleapis');

      const oauth2Client = new google.auth.OAuth2(
        config.EMAIL_CLIENT_ID,
        config.EMAIL_CLIENT_SECRET,
        config.EMAIL_REDIRECT_URL
      );

      const scopes = [
        'https://mail.google.com/',
        'email'
      ];

      const google_url = oauth2Client.generateAuthUrl({
        // 'online' (default) or 'offline' (gets refresh_token)
        access_type: 'offline',
        // If you only need one scope you can pass it as a string
        scope: scopes
      });
      return res.status(200).json({ url: google_url });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/EmailController',
        action_name: 'gmailSignin',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  gmailAuthorize: async (req, res) => {
    let { code = '' } = req.query;
    try {
      if (!code)
        throw 'Unauthorized to continue.'

      const { google } = require('googleapis');
      const gmail_client = new google.auth.OAuth2(
        config.EMAIL_CLIENT_ID,
        config.EMAIL_CLIENT_SECRET,
        config.EMAIL_REDIRECT_URL
      );

      // This will provide an object with the access_token and refresh_token.
      // Save these somewhere safe so they can be used at a later time.
      const { tokens } = await gmail_client.getToken(code);

      const { OAuth2Client } = require('google-auth-library');
      const client = new OAuth2Client(config.EMAIL_CLIENT_ID);
      const ticket = await client.verifyIdToken({
        idToken: tokens.id_token,
        audience: config.EMAIL_CLIENT_ID,
      });
      const payload = ticket.getPayload();
      return res.status(200).json({ tokens: tokens, email: payload.email });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/EmailController',
        action_name: 'gmailAuthorize',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  sendEmail: async (req, res) => {
    let { po_id } = req.params
    let { receiver, topic, message, cc = '', id_token, access_token } = req.body;
    try {
      if (!receiver || !topic || !message || !po_id || !id_token || !access_token)
        throw 'Information missing.';

      message = message.split('\n').join('<br>')
      const po_result = await PurchaseOrder.findOne({ where: { id: po_id }, include: [{ model: PurchaseOrderDetail }] });
      if (!po_result)
        throw { message: 'PO not found' };

      const pdf_content = await poPdfHelper.generateContent(po_id, po_result, po_result.PurchaseOrderDetails);
      let po_no = '00000' + po_id;
      let po_file_name = 'po_' + po_no.slice(-6);

      await poPdfHelper.createFile(po_file_name, pdf_content);

      const { OAuth2Client } = require('google-auth-library');
      const client = new OAuth2Client(config.EMAIL_CLIENT_ID);
      const ticket = await client.verifyIdToken({
        idToken: id_token,
        audience: config.EMAIL_CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
      });
      const payload = ticket.getPayload();
      let sender = payload.email;

      let po_attachment = `${__dirname}../pdfs/${po_file_name}.pdf`
      let attachment_type = 'application/pdf'
      await emailSenderHelper.oauthMethod(access_token, sender, receiver, topic, message, cc, po_file_name, po_attachment, attachment_type);
      return res.status(200).json(true);
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'purchaseorder/EmailController',
        action_name: 'sendEmail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}