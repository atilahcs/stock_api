const {
  OrderPrestashop,
  OrderPrestashopDetail,
  OrderScan,
  OrderScanDetail,
  OrderScanMovement,
  OrderScanVersion,
  OrderScanVersionDetail, ErrorLog
} = require("../models");
const sequelize = require("sequelize");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];

const filterHelper = require('../../helpers/filterHelper');
const model = require('../../models');

module.exports = {
  index: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    if (page || page > 1) offset = limit * (page - 1);
    let transaction = await model.sequelize.transaction();
    try {
      let obj = {
        meh: false,
        created_time: new Date(),
        updated_time: new Date()
      }
      let filter_clause = await filterHelper(filter);
      let product_filter_clause = await filterHelper(product_filter);
      let obj_count = {
        where: filter_clause
      }
      if (product_filter) {
        obj_count.include = [{
          model: Product,
          where: product_filter_clause
        }]
      }
      const count = await Supplier.count(obj_count);
      let obj_search = {
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      };
      if (product_filter) {
        obj_search.include = [{
          model: Product,
          where: product_filter_clause
        }]
      }
      const supplier_list = await Supplier.findAll(obj_search);
      const count = await BarOrder.count({ where: filter_clause });
      const bar_order_list = await BarOrder.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 20; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return res.status(200).json({ message: 'success' })
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'adjustment/AdjustmentController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}