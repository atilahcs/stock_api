const sequelize = require("sequelize");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];

const SupplierProductController = require("./SupplierProductController");
const {
  Supplier,
  SupplierProduct,
  Product,
  ErrorLog
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
const model = require('../../models');
const filterHelper = require('../../helpers/filterHelper');
const PurchaseOrderController = require('./PurchaseOrderController');
const ReceiveOrderController = require('./ReceiveOrderController');

module.exports = {
  getDetail: async (req, res) => {
    let { supplier_id } = req.params;
    try {
      const supplier_result = await Supplier.findOne({
        where: { id: supplier_id }
      });
      if (!supplier_result) throw { message: "Supplier does not exist." };
      return res.status(200).json({ supplier: supplier_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let {
      page,
      column = "supplier_name",
      ordering = "ASC",
      limit = 50
    } = req.query;
    let { filter = "" } = req.query;
    let { product_filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      let product_filter_clause = await filterHelper(product_filter);
      let obj_count = {
        where: filter_clause
      };
      if (product_filter) {
        obj_count.include = [
          {
            model: Product,
            where: product_filter_clause
          }
        ];
      }
      const count = await Supplier.count(obj_count);
      let obj_search = {
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      };
      if (product_filter) {
        obj_search.include = [
          {
            model: Product,
            where: product_filter_clause
          }
        ];
      }
      const supplier_list = await Supplier.findAll(obj_search);
      let supplier_list_with_count = await Promise.all(supplier_list.map(async (detail) => {
        const count_product = await SupplierProduct.count({ where: { sid: detail.id } });
        return {
          id: detail.id,
          supplier_name: detail.supplier_name,
          address: detail.address,
          city: detail.city,
          zipcode: detail.zipcode,
          country: detail.country,
          street_address: detail.street_address,
          street_city: detail.street_city,
          street_zipcode: detail.street_zipcode,
          street_country: detail.street_country,
          sale_setting: detail.sale_setting,
          purchase_setting: detail.purchase_setting,
          bill_duedate: detail.bill_duedate,
          invoice_duedate: detail.invoice_duedate,
          email: detail.email,
          tax_id: detail.tax_id,
          contact: detail.contact,
          contact_no: detail.contact,
          xero_supplier_id: detail.xero_supplier_id,
          prestashop_id: detail.prestashop_id,
          status: detail.status,
          created_time: detail.created_time,
          updated_time: detail.updated_time,
          count_product: count_product
        };
      }));
      return res.status(200).json({ count, supplier_list: supplier_list_with_count });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },

  create: async (req, res) => {
    let { supplier_name, address, city, state_id, zipcode, tax_id, contact_email, contact_name, contact_no } = req.body;
    if (!supplier_name || !address || !city || !state_id || !zipcode)
      throw { message: 'Information missing.' };
    let transaction = await model.sequelize.transaction();
    if (!contact_no)
      contact_no = '-';
    try {
      const created_supplier = await Supplier.create({
        supplier_name,
        address,
        city,
        zipcode,
        tax_id,
        email: contact_email,
        contact: contact_name,
        contact_no,
        country: 'Thailand',
        created_time: new Date(),
        updated_time: new Date()
      }, { transaction });
      let data = {
        supplier_info: {
          name: supplier_name,
          address1: address,
          state: city,
          id_state: state_id,
          postcode: zipcode,
          id_country: 206,
          phone: contact_no
        }
      };
      let header = {
        'api_key': config.ADMIN_API_KEY
      };
      const ps_result = await HttpService.request(data, config.wbUrl + "", 'post', header);
      if (ps_result.error)
        throw { message: ps_result.message };
      await Supplier.update({
        prestashop_id: Number(ps_result.supplier.id),
        updated_time: new Date()
      }, {
          where: { id: created_supplier.id },
          transaction
        });
      await transaction.commit();
      return res.status(200).json({ success: true, ps_result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'supplier/SupplierController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { supplier_id } = req.params;
    let { supplier_name, address, city, state_id, zipcode, tax_id, contact_email, contact_name, contact_no } = req.body;
    let transaction = await model.sequelize.transaction();
    if (!contact_no)
      contact_no = '-';
    try {
      if (!supplier_name || !address || !city || !state_id || !zipcode)
        throw { message: 'Information missing.' };
      const supplier_result = await Supplier.findOne({ where: { id: supplier_id }});
      if (!supplier_result)
        throw { message: 'Supplier does not exist.' };
      await Supplier.update({
        supplier_name,
        address,
        city,
        zipcode,
        tax_id,
        email: contact_email,
        contact: contact_name,
        contact_no,
        country: 'Thailand',
        updated_time: new Date()
      }, { where: { id: supplier_id }, transaction });
      await transaction.commit();
      const supplier = await Supplier.findOne({ where: { id: supplier_id }});
      if (!supplier.prestashop_id)
        return res.status(200).json({ supplier });
      let data = {
        supplier_id: Number(supplier.prestashop_id),
        supplier_info: {
          name: supplier_name,
          address1: address,
          state: city,
          id_state: state_id,
          postcode: zipcode,
          id_country: 206,
          phone: contact_no
        }
      };
      let header = {
        'api_key': config.ADMIN_API_KEY
      };
      const ps_result = await HttpService.request(data, config.wbUrl + "", 'put', header);
      if (ps_result.error)
        throw { message: ps_result.message };
      return res.status(200).json({ supplier });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    try {
    } catch (err) {
      console.error("=============");
      console.error("Controller: supplier/SupplierController");
      console.error("Action: delete");
      console.error("Params (params):", req.params);
      console.error("Path:", req.originalUrl);
      console.error("Method:", req.method);
      console.error("Error:", err);
      console.error("=============");
      return res.status(406).json(err);
    }
  },

  product: SupplierProductController,
  po: PurchaseOrderController,
  ro: ReceiveOrderController
};
