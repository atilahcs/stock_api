const sequelize = require("sequelize");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];

const {
  Supplier,
  SupplierProduct,
  Product,
  ErrorLog
} = require("../../models");
const filterHelper = require("../../helpers/filterHelper");

const model = require("../../models");
module.exports = {
  getList: async (req, res) => {
    let { supplier_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = "" } = req.query;
    let { product_filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      if (supplier_id !== "-1") {
        filter += `sid:${supplier_id};`;
      }
      let filter_clause = await filterHelper(filter);
      let product_filter_clause = await filterHelper(product_filter);
      const count = await SupplierProduct.count({ where: filter_clause });
      let obj_search = {
        offset: offset,
        limit: limit < 0 ? null : limit,
        order: [[column, ordering]],
        where: filter_clause,
        include: [
          {
            model: Product,
            where: product_filter ? product_filter_clause : {}
          }
        ]
      };
      const product_list = await SupplierProduct.findAll(obj_search);
      return res.status(200).json({ count, product_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },

  get: async (req, res) => {
    const pageLimit = 50;
    const { page = 0, id, Name } = req.query;
    try {
      if (id) {
        const ws_supplier = await Supplier.findOne({
          logging: true,

          where: {
            id
          },
          include: [
            {
              model: Product
            }
          ]
        });
        return res.status(200).json(ws_supplier);
      } else {
        const ws_supplier = await Supplier.findAll({
          logging: false,
          include: [Product],
          offset: 0,
          limit: pageLimit,
          order: [["supplier_name", "ASC"]]
        });
        const result = {
          ws_supplier
        };
        return res.status(200).json(result);
      }
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "get",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getProductFromPs: async (req, res) => {
    console.log(`Supplier - getfromps`);
    let { supplier_id } = req.query;
    supplier_id = Number(supplier_id);

    try {
      if (!supplier_id) throw { message: "Invalid parameter" };

      let data = {
        supplier_id: supplier_id,
        token: ""
      };

      // console.log(data);
      const sync_result = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );

      //   console.log(sync_result);
      return res.status(200).json(sync_result);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "getProductFromPs",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },

  update: async (req, res) => {
    let { supplier_id } = req.params;
    supplier_id = Number(supplier_id);
    const { add_list, remove_list } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      const supplier_result = await Supplier.findOne({ where: { id: supplier_id }});
      if (!supplier_result)
        throw { message: 'Supplier does not exist.' };
      let add_ps_list = [];
      for (let detail of add_list) {
        const sup_prod = await SupplierProduct.findOne({ where: { pid: detail.pid, sid: supplier_id }});
        if (sup_prod)
          continue;
        await SupplierProduct.create({
          pid: detail.pid,
          product_id: detail.product_prestashop_id,
          sid: supplier_id,
          supplier_id: supplier_result.prestashop_id
        }, { transaction });
        add_ps_list.push(detail.product_prestashop_id);
      }
      let remove_ps_list = [];
      for (let detail of remove_list) {
        const sup_prod = await SupplierProduct.findOne({ where: { pid: detail.pid, sid: supplier_id }});
        if (!sup_prod)
          continue;
        await SupplierProduct.destroy({ where: { pid: detail.pid, sid: supplier_id }, transaction });
        remove_ps_list.push(detail.product_prestashop_id);
      }
      let data = {
        supplier_id: supplier_result.prestashop_id,
        add_product_list: add_ps_list,
        remove_product_list: remove_ps_list
      }
      let header = {
        'api_key': config.ADMIN_API_KEY
      }
      const ps_result = await HttpService.request(data, config.wbUrl + "", 'put', header);
      if (ps_result.error)
        throw { message: ps_result.message };
      await transaction.commit();
      return res.status(200).json({ success: true });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },

  updateProductIdAndSupplierId: async (req, res) => {
    console.log(`UpdateProductIdAndSuppleirId`);
    let transaction = await model.sequelize.transaction();
    try {
      const supplier_product = await SupplierProduct.findAll({
        logging: false,
        raw: true,
        attributes: ["id", "product_id", "supplier_id"],
        order: [["id", "ASC"]]
      });
      const supplier = await Supplier.findAll({
        logging: false,
        raw: true,
        attributes: ["id", "supplier_name", "prestashop_id"],
        order: [["id", "ASC"]]
      });

      const product = await Product.findAll({
        logging: false,
        raw: true,
        attributes: ["id", "name", "product_prestashop_id"],
        order: [["id", "ASC"]]
      });

      //   console.log(supplier_product);
      //   console.log(`=========`);
      //   console.log(supplier);
      //   console.log(product[0].dataValues);

      // การทำงานของ reduce คือการ เอา array มารวมกันเป็นก้อนเดียว ก่อนที่จะ return ออกมา ?
      const filtersp = supplier_product.reduce(
        (acc, { id, product_id, supplier_id }) => {
          acc.push({
            sid: supplier
              .filter(y => y.prestashop_id == supplier_id)
              .map(x => x.id)
              .join(),
            pid: product
              .filter(y => y.product_prestashop_id == product_id)
              .map(x => x.id)
              .join(),
            // supplier_id: supplier_id,
            id: id,
            product_id: product_id,
            supplier_id: supplier_id,
            updated_time: new Date()
          });
          return acc;
        },
        []
      );
      //   console.log(filtersp[1]);

      const supplier_updated = await SupplierProduct.bulkCreate(filtersp, {
        updateOnDuplicate: ["sid", "pid", "updated_time"],
        transaction
      });

      // .then(() => {
      //   return SupplierProduct.update(
      //     { pid, sid },
      //     { where: { id: { id } } }
      //   );
      // })
      // .spread((affectedCount, affectedRows) => {
      //   return console.log(`affectedCount : ${affectedCount}`);
      // });

      await transaction.commit();
      return res.json(supplier_updated);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "updateProductIdAndSupplierId",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  updateProductIdAndSupplierIdV1: async (req, res) => {
    console.log(`UpdateProductIdAndSuppleirId`);
    let transaction = await model.sequelize.transaction();
    try {
      const supplier_product = await SupplierProduct.findAll({
        logging: false,
        attributes: ["id", "product_id", "supplier_id"],
        order: [["id", "ASC"]]
      });

      //   console.log(supplier_product.length);
      //   console.log(supplier_product[0]);

      //   console.log(supplier_product[0].id);
      //   console.log(supplier_product[0].supplier_id);
      let count = 1;
      for (let item of supplier_product) {
        // console.log(item.product_id);
        // console.log(item.supplier_id);

        //get supplier id
        const supplier = await Supplier.findOne({
          logging: false,
          where: { prestashop_id: item.supplier_id },
          // include: [Product],
          attributes: ["id", "supplier_name", "prestashop_id"]
        });

        // get product id
        const product = await Product.findOne({
          logging: false,
          where: { product_prestashop_id: item.product_id },
          // include: [Product],
          attributes: ["id", "name", "product_prestashop_id"]
        });

        //   console.log(supplier.dataValues);
        //   console.log(product.dataValues);
        let supplier_id = item.id,
          pid = product.id,
          sid = supplier.dataValues.id,
          updated_time = new Date();

        //   console.log(supplier_id, sid, pid, updated_time);

        const ws_supplier = await SupplierProduct.update(
          { pid, sid, updated_time },
          {
            where: {
              id: supplier_id
            },
            transaction
          }
        );
        count++;
        console.log(count);
      }

      await transaction.commit();
      return res.json(`Done`);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "updateProductIdAndSupplierIdV1",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    try {
    } catch (err) {
      console.error("=============");
      console.error("Controller: supplier/SupplierController");
      console.error("Action: delete");
      console.error("Params (params):", req.params);
      console.error("Path:", req.originalUrl);
      console.error("Method:", req.method);
      console.error("Error:", err);
      console.error("=============");
      return res.status(406).json(err);
    }
  },

  createAndUpdate: async (req, res) => {
    const { supplier_id, prestashop_id, xero_detail, product_list } = req.body;

    console.log(product_list);

    let product_in_supplier = [];

    for (item of product_list) {
      const obj = {
        WSProductId: item.id_product,
        WSSupplierId: supplier_id
      };
      product_in_supplier.push(obj);
    }
    console.log(product_in_supplier);
    let transaction = await model.sequelize.transaction();
    try {
      const update_supplier = await Supplier.update(
        {
          xero_supplier_id: xero_detail.xero_supplier_id,
          email: xero_detail.email,
          tax_id: xero_detail.tax_id,
          contact: xero_detail.contact
        },
        {
          where: {
            id: supplier_id
          },
          transaction
        }
      );

      const create_product_supplier = await SupplierProduct.bulkCreate(
        product_in_supplier,
        { transaction }
      );

      await transaction.commit();
      return res.json({
        data: {
          // ws_supplier: update_supplier,
          ws_supplier_product: create_product_supplier
        }
      });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "supplier/SupplierProductController",
        action_name: "createAndUpdate",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
};
