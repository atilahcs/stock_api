const { Homebar, BarOrder, BarOrderDetail, ErrorLog } = require("../../models");
const filterHelper = require('../../helpers/filterHelper');
const model = require('../../models');
module.exports = {
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await BarOrder.count({ where: filter_clause });
      const bar_order_list = await BarOrder.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, bar_order_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'homebar/BarOrderController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getDetail: async (req, res) => {
    let { homebar_id, barorder_id } = req.params;
    try {
      const homebar_detail = await Homebar.findOne({ where: { id: homebar_id }});
      if (!homebar_detail)
        throw { message: 'Homebar not found' };

      const bar_order = await BarOrder.findOne({
        where: { id: barorder_id, bar_id: homebar_id },
        include: [{ model: BarOrderDetail }]
      });
      return res.status(200).json({ bar_order });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'homebar/BarOrderController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getListByBarId: async (req, res) => {
    let { homebar_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.body;
    let { filter = '' } = req.body;
    let offset = 0;
    if (page || page > 1) offset = limit * (page - 1);
    try {
      filter += `bar_id:${homebar_id};`;
      let filter_clause = await filterHelper(filter);
      const count = await BarOrder.count({ where: filter_clause });
      const bar_order_list = await BarOrder.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, bar_order_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
        let message = (err && err.message) ? err.message : 'Something went wrong.';
        let param = {
          params: req.params
        }
        await ErrorLog.create({
          controller_name: 'homebar/HomeBarController',
          action_name: 'getListByBarId',
          path: req.originalUrl,
          method: req.method,
          message: message,
          params: JSON.stringify(param),
          created_time: new Date()
        });
        return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let { homebar_id } = req.params;
    let { barorder_info, barorder_detail } = req.body
    let transaction = await model.sequelize.transaction();
    try {
      const homebar_detail = await Homebar.findOne({ where: { id: homebar_id }});
      if (!homebar_detail)
        throw { message: 'Homebar not found.' };
      if (!barorder_info || !barorder_detail)
        throw { message: 'Information missing.' };
      
      const barorder_result = await BarOrder.create({
        bar_id: barorder_info.bar_id,
        bar_name: barorder_info.bar_name,
        address: barorder_info.address,
        tax_id: barorder_info.tax_id,
        contact_name: barorder_info.contact_name,
        contact_no: barorder_info.contact_no,
        order_date: barorder_info.order_date || new Date(),
        payment_type: barorder_info.payment_type,
        due_date: barorder_info.due_date,
        credit_date: barorder_info.credit_date,
        expect_delivery_date: barorder_info.expect_delivery_date,
        total_price: barorder_info.total_price,
        inc_tax: barorder_info.inc_tax,
        final_price: barorder_info.final_price,
        internal_remark: barorder_info.internal_remark,
        remark: barorder_info.remark,
        status: 'Pending',
        created_time: new Date(),
        updated_time: new Date()
      }, { transaction });
      barorder_detail = barorder_detail.map((detail) => {
        let value = {
          bar_order_id: barorder_result.id,
          bar_id: homebar_id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_unit: detail.product_unit,
          product_unit_price: detail.product_unit_price,
          product_unit_discount: detail.product_unit_discount,
          product_amount: detail.product_amount,
          product_amount_after_discount: detail.product_amount_after_discount,
          created_time: new Date(),
          updated_time: new Date()
        }
        return value;
      });
      await BarOrderDetail.bulkCreate(barorder_detail, { transaction });
      await transaction.commit();

      const bar_order = await BarOrder.findOne({
        where: { id: barorder_result.id, bar_id: homebar_id },
        include: [{ model: BarOrderDetail }]
      });
      return res.status(200).json({ bar_order })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: '้homebar/BarOrderController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { homebar_id, barorder_id } = req.params;
    let { barorder_info, barorder_detail } = req.body
    let transaction = await model.sequelize.transaction();
    try {
      const homebar_detail = await Homebar.findOne({ where: { id: homebar_id }});
      if (!homebar_detail)
        throw { message: 'Homebar not found.' };
      let order_detail = await BarOrder.findOne({ where: { id: barorder_id, bar_id: homebar_id }});
      if (!order_detail)
        throw { message: 'Bar Order not found.' };
      if (!barorder_info || !barorder_detail)
        throw  { message: 'Information missing.' };
      
      await BarOrder.update({
        bar_id: barorder_info.bar_id,
        bar_name: barorder_info.bar_name,
        address: barorder_info.address,
        tax_id: barorder_info.tax_id,
        contact_name: barorder_info.contact_name,
        contact_no: barorder_info.contact_no,
        order_date: barorder_info.order_date || new Date(),
        payment_type: barorder_info.payment_type,
        due_date: barorder_info.due_date,
        credit_date: barorder_info.credit_date,
        expect_delivery_date: barorder_info.expect_delivery_date,
        total_price: barorder_info.total_price,
        inc_tax: barorder_info.inc_tax,
        final_price: barorder_info.final_price,
        internal_remark: barorder_info.internal_remark,
        remark: barorder_info.remark,
        status: barorder_info.status,
        updated_time: new Date()
      }, { where: { id: barorder_id, bar_id: homebar_id }, transaction })
      await BarOrderDetail.destroy({ where: { bar_order_id: barorder_id }, transaction });
      for (let detail of barorder_detail) {
        detail.bar_order_id = barorder_id;
      }
      await BarOrderDetail.bulkCreate({
        bar_order_id: barorder_detail.bar_order_id,
        bar_id: barorder_detail.bar_id,
        product_name: barorder_detail.product_name,
        product_id: barorder_detail.product_id,
        pid: barorder_detail.pid,
        product_reference: barorder_detail.product_reference,
        product_unit: barorder_detail.product_unit,
        product_unit_price: barorder_detail.product_unit_price,
        product_unit_discount: barorder_detail.product_unit_discount,
        product_amount: barorder_detail.product_amount,
        product_amount_after_discount: barorder_detail.product_amount_after_discount,
        created_time: new Date(),
        updated_time: new Date()
      }, { transaction });
      await transaction.commit();

      let bar_order = await BarOrder.findOne({
        where: { id: barorder_id, bar_id: homebar_id },
        include: [{ model: BarOrderDetail }]
      });
      return res.status(200).json({ bar_order })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'homebar/BarOrderController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let { homebar_id, barorder_id } = req.params;
    let transaction = await model.sequelize.transaction();
    try {
      const destroy_result = await BarOrder.destroy({ where: { id: barorder_id, bar_id: homebar_id }, transaction })
      await BarOrderDetail.destroy({ where: { bar_order_id: barorder_id, bar_id: homebar_id }, transaction })
      await transaction.commit();
      return res.status(200).json({deleted: destroy_result })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'homebar/BarOrderController',
        action_name: 'delete',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  updateOrderStatus: async (req, res) => {
    let { homebar_id, barorder_id } = req.params;
    let { order_status } = req.body
    let transaction = await model.sequelize.transaction();
    try {
      const homebar_detail = await Homebar.findOne({ where: { id: homebar_id }});
      if (!homebar_detail)
        throw { message: 'Homebar not found.' };
      let order_detail = await BarOrder.findOne({ where: { id: barorder_id, bar_id: homebar_id }});
      if (!order_detail)
        throw { message: 'Bar Order not found.' };
      if (!barorder_info || !barorder_detail)
        throw { message: 'Information missing.' };
      if (!['Pending', 'Processing', 'Complete'].includes(order_status))
        throw { message: 'Incorrect status.' };
      
      await BarOrder.update({ status: order_status, updated_time: new Date() }, { where: { id: barorder_id, bar_id: homebar_id }, transaction })
      
      await transaction.commit();

      let bar_order = await BarOrder.findOne({
        where: { id: barorder_id, bar_id: homebar_id },
        include: [{ model: BarOrderDetail }]
      });
      return res.status(200).json({ bar_order })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'homebar/Controller',
        action_name: 'updateOrderStatus',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}