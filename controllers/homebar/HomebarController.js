const { Homebar, ErrorLog } = require("../../models");
const BarOrderController = require('./BarOrderController');
const filterHelper = require('../../helpers/filterHelper');
const model = require('../../models');
module.exports = {
  getDetail: async (req, res) => {
    let { homebar_id } = req.params;
    try {
      const homebar_result = await Homebar.findOne({ where: { id: homebar_id }});
      return res.status(200).json({ homebar: homebar_result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'homebar/HomnebarController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await Homebar.count({ where: filter_clause });
      const homebar_list = await Homebar.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, homebar_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'homebar/HomebarController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let { company_name, homebar, address, tax_id, contact_name, contact_no } = req.body
    let transaction = await model.sequelize.transaction();
    try {
      if (!company_name)
        throw { message: 'Company name cannot be empty' }
      
      let company_info = {
        company_name: company_name,
        homebar: homebar,
        address: address,
        tax_id: tax_id,
        contact_name: contact_name,
        contact_no: contact_no,
        created_time: new Date(),
        updated_time: new Date()
      }
      const homebar_result = await Homebar.create(company_info, { transaction })
      await transaction.commit();
      return res.status(200).json({ homebar: homebar_result })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'homebar/HomebarController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    let { homebar_id } = req.params;
    let { company_name, homebar, address, tax_id, contact_name, contact_no } = req.body
    try {
      if (!company_name)
        throw { message: 'Company name cannot be empty' }

      const company_check_result = await Homebar.findOne({ where: { id: homebar_id }})
      if (!company_check_result)
        throw { message: 'Company does not exist' }
      
      let company_info = {
        company_name: company_name,
        homebar: homebar,
        address: address,
        tax_id: tax_id,
        contact_name: contact_name,
        contact_no: contact_no,
        updated_time: new Date()
      }
      await Homebar.update(company_info, { where: { id: homebar_id }, transaction })
      await transaction.commit()
      const homebar_result = await Homebar.findOne({ where: { id: homebar_id }});
      return res.status(200).json({ homebar: homebar_result })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'homebar/HomebarController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let { homebar_id } = req.params;
    let transaction = await model.sequelize.transaction();
    try {
      const destroy_result = await Homebar.destroy({ where: { id: homebar_id }, transaction })
      await transaction.commit();
      return res.status(200).json({ deleted: destroy_result })
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'homebar/HomebarController',
        action_name: 'delete',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  barorder: BarOrderController
}