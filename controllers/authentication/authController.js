var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const { AdminHistory, AdminToken, ErrorLog } = require('../../models');
const model = require('../../models');
const HttpService = require("../../services/HttpService");
const jwt = require('jsonwebtoken');

module.exports = {
    login: async (req, res) => {
        const { _email, _password } = req.body;
        let transaction = await model.sequelize.transaction();
        try {
            if (!_email || !_password)
                throw { message: 'Email or password cannot be empty.' }

            let login_info = {
                adminLoginData: {
                    email: _email,
                    password: _password
                },
                token: ''
            };
            const ps_result = await HttpService.request(login_info, config.wbUrl + "", 'post');
            if (ps_result.status == 'error')
                throw { message: ps_result.message }
            
            let admin_email = ps_result.employee.email,
            admin_firstname = ps_result.employee.firstname,
            admin_lastname = ps_result.employee.lastname,
            admin_ps_id = Number(ps_result.employee.id),
            admin_level = Number(ps_result.employee.id_profile);

            const token = await jwt.sign({
                eml: admin_email,
                fn: admin_firstname,
                ln: admin_lastname,
                psid: admin_ps_id
            }, config.JWT_SECRET, { expiresIn: '12h' });
            await AdminToken.create({
                admin_ps_id,
                admin_email,
                admin_firstname,
                admin_lastname,
                token,
                agent: req.header('user-agent'),
                created_time: new Date(),
                updated_time: new Date()
            }, { transaction });
            await AdminHistory.create({
                admin_ps_id,
                admin_email,
                admin_firstname,
                admin_lastname,
                agent: req.header('user-agent'),
                admin_level,
                action: 'Login ' + req.method + ' ' + req.originalUrl,
                created_time: new Date(),
                updated_time: new Date()
            }, { transaction });
            await transaction.commit();
            return res.status(200).json({
                admin_info: {
                    _admin_id: admin_ps_id,
                    _admin_firstname: admin_firstname,
                    _admin_lastname: admin_lastname,
                    _admin_email: admin_email,
                    _admin_level: admin_level,
                    _token: token
                }});
        } catch (err) {
            if (transaction.finished != 'commit')
                await transaction.rollback();
            let status = (err && err.status) ? err.status : 500;
            let message = (err && err.message) ? err.message : 'Something went wrong.';
            let param = {
                body: req.body
            }
            await ErrorLog.create({
                controller_name: 'authentication/authAdminController',
                action_name: 'login',
                path: req.originalUrl,
                method: req.method,
                message: message,
                params: JSON.stringify(param),
                created_time: new Date()
            });
            return res.status(status).json({ error: message });
        }
    },
    logout: async (req, res) => {
        let headers = req.headers;
        let token = headers['admin-token'];
        let admin_id = headers['admin-id'];
        let admin_firstname = headers['admin-firstname'];
        let admin_lastname = headers['admin-lastname'];
        let admin_email = headers['admin-email'];
        let admin_level = headers['admin-level'];
        try {
            await AdminToken.destroy({ where: { token: token }});
            await AdminHistory.create({
                admin_ps_id: admin_id,
                admin_email: admin_email,
                admin_firstname: admin_firstname,
                admin_lastname: admin_lastname,
                agent: req.header('user-agent'),
                admin_level: admin_level,
                action: 'Logout ' + req.method + ' ' + req.originalUrl,
                created_time: new Date(),
                updated_time: new Date()
            });
            return res.status(200).json({ logout: true });
        } catch (err) {
            let status = (err && err.status) ? err.status : 500;
            let message = (err && err.message) ? err.message : 'Something went wrong.';
            let param = {
                header: req.headers
            }
            await ErrorLog.create({
                controller_name: 'authentication/authAdminController',
                action_name: 'logout',
                path: req.originalUrl,
                method: req.method,
                message: message,
                params: JSON.stringify(param),
                created_time: new Date()
            });
            return res.status(status).json({ error: message });
        }
    }
}
