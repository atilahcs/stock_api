const sequelize = require("sequelize");
const {
  WS_Product,
  ProductQuantity,
  WS_Movement_Logs_Flat
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
module.exports = {
  search: async (req, res) => {
    const {
      page = 0,
      pageSize = 50,
      id,
      Name,
      start = 0,
      search,
      filtered = [],
      attributes
    } = req.body;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageSize)
    );
    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };
    console.log(req.body);
    try {
      let orderby = [];
      const ws_movement_logs_flat = await WS_Movement_Logs_Flat.findAndCountAll(
        {
          distinct: true,
          required: true,
          where: filtered.map((props, key) => {
            // if (!props.value) {
            //   orderby = [["id", "DESC"]];
            // } else {
            //   orderby = [[`${props.id}`, "ASC"], ["id", "ASC"]];
            // }
            return {
              [props.id]: sequelize.where(
                sequelize.fn("lower", sequelize.col(props.id)),
                "LIKE",
                "" + props.value + "%"
              )
            };
          }),
          include: [
            {
              model: WS_Product
            }
          ],
          attributes,
          offset: pageNumber,
          limit: parseInt(pageSize),
          order: [["id", "DESC"]]
        }
      );
      dataArray.totalPage = Math.ceil(ws_movement_logs_flat.count / pageSize);
      dataArray.log = ws_movement_logs_flat.rows;
      dataArray.totalData = ws_movement_logs_flat.count;

      return res.json(dataArray);
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  get: async (req, res) => {
    const pageLimit = 100;
    const { page = 0, product_id, Name } = req.query;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageLimit)
    );
    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };
    try {
      if (product_id) {
        const ws_movement_logs_flat = await WS_Movement_Logs_Flat.findAll({
          offset: 0,
          limit: pageLimit,
          order: [["id", "DESC"]],
          where: {
            product_id
          }
        });
        return res.json(ws_movement_logs_flat);
      } else {
        const ws_movement_logs_flat = await WS_Movement_Logs_Flat.findAll({
          offset: 0,
          limit: pageLimit,
          order: [["id", "DESC"]]
        });
        return res.json({ log: ws_movement_logs_flat });
      }
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  create: async (req, res) => {
    const {
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;
    console.log(req.body);
    try {
      const _ws_movement_logs_flat = await WS_Movement_Logs_Flat.create({
        name,
        idprestashop,
        reference,
        status,
        barcode,
        product_img,
        shelf_location,
        product_type,
        base_price,
        bar_price,
        purchasing_price,
        final_price
      });
      return res.json({
        data: {
          ws_movement_logs_flat: _ws_movement_logs_flat
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  update: async (req, res) => {
    const {
      id,
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;

    try {
      const _ws_movement_logs_flat = await WS_Movement_Logs_Flat.update(
        {
          name,
          idprestashop,
          reference,
          status,
          barcode,
          product_img,
          shelf_location,
          product_type,
          base_price,
          bar_price,
          purchasing_price,
          final_price
        },
        {
          where: {
            id: id
          }
        }
      );
      return res.json({
        data: {
          ws_movement_logs_flat: _ws_movement_logs_flat
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;

    try {
      const _ws_movement_logs_flat = await WS_Movement_Logs_Flat.destroy({
        where: {
          id
        }
      });
      return res.json({
        data: {
          ws_movement_logs_flat: _ws_movement_logs_flat
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  }
};
