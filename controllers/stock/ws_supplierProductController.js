const sequelize = require("sequelize");
// const ws_supplierMovementController = require('./ws_supplierMovementController')
const {
  WS_Supplier,
  WS_Supplier_Product,
  WS_Product
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
module.exports = {
  search: async (req, res) => {
    const {
      page = 0,
      pageSize = 500,
      id,
      Name,
      start = 0,
      search,
      filtered = [],
      attributes
    } = req.body;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageSize)
    );
    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };
    console.log(req.body);
    try {
      const ws_supplier_product = await WS_Supplier_Product.findAndCountAll({
        distinct: true,
        required: true,
        include: [
          {
            model: ProductQuantity,
            where: filtered.map((props, key) => {
              return {
                [props.id]: sequelize.where(
                  sequelize.fn("lower", sequelize.col(props.id)),
                  "LIKE",
                  "%" + props.value + "%"
                )
              };
            })
          },
          {
            model: WS_Movement_Logs_Flat
          }
        ],
        attributes,
        offset: pageNumber,
        limit: parseInt(pageSize),
        order: [["id", "DESC"]]
      });
      dataArray.totalPage = Math.ceil(ws_supplier_product.count / pageSize);
      dataArray.data = ws_supplier_product.rows;
      dataArray.totalData = ws_supplier_product.count;

      return res.json(dataArray);
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  get: async (req, res) => {
    const pageLimit = 100;
    const { page = 0, id, Name } = req.query;
    try {
      if (id) {
        const ws_supplier_product = await WS_Supplier_Product.findOne({
          where: {
            id
          },
          include: [
            {
              model: WS_Product //ใช้จัวนี้เชื่อมได้เลยนะ
            }
          ]
        });
        // const result = {
        //     ws_supplier_product
        // }
        return res.json(ws_supplier_product);
        // } else if (Name) {
        //     const ws_supplier_product = await WS_Supplier_Product.findAll({
        //         offset: 0,
        //         limit: pageLimit
        //     });
        //     return res.json(ws_supplier_product);
      } else {
        const ws_supplier_product = await WS_Supplier_Product.findAll({
          include: [WS_Product],
          offset: 0,
          limit: pageLimit,
          order: [["supplier_name", "ASC"]]
        });
        const result = {
          ws_supplier_product
        };
        return res.json(result);
      }
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  create: async (req, res) => {
    const {
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;
    console.log(req.body);

    try {
      const _ws_supplier_product = await WS_Supplier_Product.create({
        name,
        idprestashop,
        reference,
        status,
        barcode,
        product_img,
        shelf_location,
        product_type,
        base_price,
        bar_price,
        purchasing_price,
        final_price
      });
      return res.json({
        data: {
          ws_supplier_product: _ws_supplier_product
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  update: async (req, res) => {
    const {
      supplier_name,
      address,
      city,
      zipcode,
      country,

      street_address,
      street_city,
      street_zipcode,
      street_country,

      sale_setting,
      purchase_setting,

      bill_duedate,
      invoice_duedate,
      email,
      tax_id,

      contact,
      contact_no,
      xero_supplier_id,
      prestashop_id,
      status
    } = req.body;

    try {
      const _ws_supplier_product = await WS_Supplier_Product.update(
        {
          supplier_name,
          address,
          city,
          zipcode,
          country,

          street_address,
          street_city,
          street_zipcode,
          street_country,

          sale_setting,
          purchase_setting,

          bill_duedate,
          invoice_duedate,
          email,
          tax_id,

          contact,
          contact_no,
          xero_supplier_id,
          prestashop_id,
          status
        },
        {
          where: {
            id: id
          }
        }
      );
      return res.json({
        data: {
          ws_supplier_product: _ws_supplier_product
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;

    try {
      const _ws_supplier_product = await WS_Supplier_Product.destroy({
        where: {
          id
        }
      });
      return res.json({
        data: {
          ws_supplier_product: _ws_supplier_product
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  }
};
