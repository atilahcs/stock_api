const sequelize = require('sequelize');
const stockWares = require('../../middlewares/stockWares');
const {
    WS_Product,
    ProductQuantity,
    WS_Movement_Logs_Flat
} = require('../../models');


module.exports = {
    run: async (req, res) => {
        const {
            _date
        } = req.query;
        try {
            const product = await WS_Product.findAll({
                subQuery: false,
                distinct: true,
                include: [{
                        model: ProductQuantity,
                        where: {
                            location_id: 1
                        }
                    },
                    {
                        model: WS_Movement_Logs_Flat,
                        limit: 1,
                        required: false,
                        where: {
                            createdAt: {
                                [sequelize.Op.lt]: "2017-08-13"
                            }
                        },
                        order: [
                            ["id", "DESC"]
                        ]
                    }
                ],

                offset: 0,
                limit: 1000
                // order: [[ 'id', 'DESC']]
            });
            const Products = {
                products: product
            };
            return res.json(Products);
        } catch (err) {
            console.error(err);
            return res.status(500).json(err);
        }
    },

    getMovementReport: async (req, res) => {
        const {
            _sdate,
            _edate
        } = req.query;
        console.log(req.query);
        console.log(_sdate);
        console.log(_edate);


        try {
            const movementReport = await WS_Movement_Logs_Flat.findAll({
                required: false,
                where: {
                    createdAt: {
                        [sequelize.Op.between]: [_sdate, _edate]
                    }
                },
            });
            return res.json(movementReport);
        } catch (err) {
            console.error(err);
            return res.status(500).json(err);
        }
    },

    getReportByDate: async (req, res) => {
        console.log("query: ", req.query);
        // console.log("body", req.body);
        // console.log("params", req.params);
        let pageNumber = 0;

        const {
            _date,
            pageSize = 3000
        } = req.query;

        let dataArray = {
            page: 0,
            totalPage: 0,
            products: [],
        }

        try {
            const product = await WS_Product.findAndCountAll({
                subQuery: false,
                distinct: true,
                logging: false,
                include: [{
                        model: ProductQuantity,
                        where: {
                            location_id: 1
                        }
                    },
                    {
                        model: WS_Movement_Logs_Flat,
                        limit: 1,
                        required: false,
                        where: {
                            createdAt: {
                                [sequelize.Op.lt]: _date
                            }
                        },
                        order: [
                            ["id", "DESC"]
                        ]
                    }
                ],

                offset: 0,
                limit: Number(pageSize)
            });
            dataArray.products.push(product);
            const totalPage = Math.ceil(product.count / pageSize);
            dataArray.totalPage = totalPage;

            for (let i = 1; i < totalPage; i++) {
                pageNumber = i * pageSize;
                const product = await WS_Product.findAndCountAll({
                    subQuery: false,
                    distinct: true,
                    logging: false,
                    include: [{
                            model: ProductQuantity,
                            where: {
                                location_id: 1
                            }
                        },
                        {
                            model: WS_Movement_Logs_Flat,
                            limit: 1,
                            required: false,
                            where: {
                                createdAt: {
                                    [sequelize.Op.lt]: _date
                                }
                            },
                            order: [
                                ["id", "DESC"]
                            ]
                        }
                    ],

                    offset: pageNumber,
                    limit: Number(pageSize)
                });
                dataArray.products.push(product);
            }


            console.log(`--Done`);
            return res.json(dataArray);
        } catch (err) {
            console.error(err);
            return res.status(500).json(err);
        }
    }
}