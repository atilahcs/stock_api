const sequelize = require("sequelize");
// const ws_supplierMovementController = require('./ws_supplierMovementController')
const {
  WS_Supplier,
  WS_Supplier_Product,
  WS_Product
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
module.exports = {
  create: async (req, res) => {
    const {
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;
    console.log(req.body);

    try {
      const _ws_supplier = await WS_Supplier.create({
        name,
        idprestashop,
        reference,
        status,
        barcode,
        product_img,
        shelf_location,
        product_type,
        base_price,
        bar_price,
        purchasing_price,
        final_price
      });
      return res.json({
        data: {
          ws_supplier: _ws_supplier
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  update: async (req, res) => {
    const {
      supplier_name,
      address,
      city,
      zipcode,
      country,

      street_address,
      street_city,
      street_zipcode,
      street_country,

      sale_setting,
      purchase_setting,

      bill_duedate,
      invoice_duedate,
      email,
      tax_id,

      contact,
      contact_no,
      xero_supplier_id,
      prestashop_id,
      status
    } = req.body;

    try {
      const _ws_supplier = await WS_Supplier.update(
        {
          supplier_name,
          address,
          city,
          zipcode,
          country,

          street_address,
          street_city,
          street_zipcode,
          street_country,

          sale_setting,
          purchase_setting,

          bill_duedate,
          invoice_duedate,
          email,
          tax_id,

          contact,
          contact_no,
          xero_supplier_id,
          prestashop_id,
          status
        },
        {
          where: {
            id: id
          }
        }
      );
      return res.json({
        data: {
          ws_supplier: _ws_supplier
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;

    try {
      const _ws_supplier = await WS_Supplier.destroy({
        where: {
          id
        }
      });
      return res.json({
        data: {
          ws_supplier: _ws_supplier
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },

  createAndUpdate: async (req, res) => {
    const { supplier_id, prestashop_id, xero_detail, product_list } = req.body;
    // console.log(supplier_id);
    // console.log(prestashop_id);
    console.log(product_list);

    let product_in_supplier = [];

    for (item of product_list) {
      const obj = {
        WSProductId: item.id_product,
        WSSupplierId: supplier_id
      };
      product_in_supplier.push(obj);
    }
    console.log(product_in_supplier);

    try {
      const update_supplier = await WS_Supplier.update(
        {
          xero_supplier_id: xero_detail.xero_supplier_id,
          email: xero_detail.email,
          tax_id: xero_detail.tax_id,
          contact: xero_detail.contact
        },
        {
          where: {
            id: supplier_id
          }
        }
      );

      const create_product_supplier = await WS_Supplier_Product.bulkCreate(
        product_in_supplier
      );

      return res.json({
        data: {
          // ws_supplier: update_supplier,
          ws_supplier_product: create_product_supplier
        }
      });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  }
};
