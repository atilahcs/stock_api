/***
 * === Product Quantity Controller ====
 * - Description : control product that come with quantity.
 *
 * Functions :
 * 1. Update
 * - Desc. : update product quantity ( increase , decrease )
 * - Usage : for manual scanner.
 *
 * 2. Sync product quantity
 * - Desc. : Sync product to prestashop with updated quantity
 * - Usage : Sync one by one , Sync all
 *
 */
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const HttpService = require("../../services/HttpService");

const sequelize = require("sequelize");
const ws_productMovementController = require("./ws_productMovementController");
const filterHelper = require("../../helpers/filterHelper");
const MovementLogService = require("../../services/MovementLogService");
const { WS_Product, ProductQuantity, WS_Supplier } = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
module.exports = {
  search: async (req, res) => {
    const {
      page = 0,
      pageSize = 100,
      id,
      Name,
      start = 0,
      search,
      filtered = [],
      filter,
      attributes
    } = req.body;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageSize)
    );

    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };
    try {
      const filter_fields = await filterHelper.separateField(filter);

      let obj_search = {
        distinct: true,
        required: true,
        subQuery: false, // เพิมมาเพราะว่า where ใน include หาไม่เจอ
        offset: pageNumber,
        limit: parseInt(pageSize),
        order: [["id", "DESC"]]
      };
      obj_search.where = filter_fields.map(props => {
        let field_name = props[0];
        let field_value = props[1];
        if (field_name == "supplier_name") {
          console.log(`supplier`);
          obj_search.include = [
            {
              model: WS_Supplier,
              attributes: ["supplier_name"],
              subQuery: false
              // required: true
            },
            { model: ProductQuantity, where: { location_id: 1 } }
          ];
          return {
            [field_name]: sequelize.where(
              sequelize.fn("lower", sequelize.col(field_name)),
              "LIKE",
              "%" + field_value + "%"
            )
          };
        } else {
          obj_search.include = [
            { model: ProductQuantity, where: { location_id: 1 } }
          ];
          return {
            [field_name]: sequelize.where(
              sequelize.fn("lower", sequelize.col(field_name)),
              "LIKE",
              "%" + field_value + "%"
            )
          };
        }
      });

      console.log("obj_search", obj_search);

      const ws_product = await WS_Product.findAndCountAll(obj_search);
      // console.log("ws_product", ws_product);
      dataArray.totalPage = Math.ceil(ws_product.count / pageSize);
      dataArray.data = ws_product.rows;
      dataArray.totalData = ws_product.count;

      return res.json(dataArray);
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  get: async (req, res) => {
    const pageLimit = 50;
    const { id } = req.params;
    const { page = 0, Name } = req.query;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageLimit)
    );

    try {
      if (id) {
        const ws_product = await WS_Product.findOne({
          where: { id },
          include: [{ model: ProductQuantity }]
        });
        return res.json(ws_product);
      } else {
        const ws_product = await WS_Product.findAll({
          include: [
            {
              model: ProductQuantity,

              where: { location_id: 1 }
            }
            // {
            //   model: WS_Supplier,
            //   // attributes: ["supplier_name"],
            //   subQuery: false,
            //   required: true
            // }
          ],
          offset: 0,
          limit: pageLimit,
          order: [["idPrestashop", "ASC"]]
        });
        const dataArray = {
          page: 0,
          data: ws_product
        };
        return res.json(dataArray);
      }
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  create: async (req, res) => {
    const {
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;
    console.log(req.body);
    try {
      const _ws_product = await WS_Product.create({
        name,
        idprestashop,
        reference,
        status,
        barcode,
        product_img,
        shelf_location,
        product_type,
        base_price,
        bar_price,
        purchasing_price,
        final_price
      });
      return res.json({ data: { ws_product: _ws_product } });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  update: async (req, res) => {
    console.log(`--> Update Product : ProductQuantity <-- \n`);

    /* 
        Param : 
        updateData :
        [{
            product_id,
            name,
            barcode,
            quantity,
            admin_id,
            admin_name,
            orderid_prestashop,
            location_id
        }]

        Usage : Scanner Page
        

     */
    const { updateData } = req.body;

    console.log(updateData);
    try {
      const result = [];

      console.log(updateData);

      for (let item of updateData) {
        //   console.log(item);
        if (!item.orderid_prestashop) {
          //   console.log(`orderid_prestashop is NULL`);
          item.orderid_prestashop = "";
        } else {
          console.log(`order_id : ${item.orderid_prestashop}`);
        }
        const id = item.product_id;
        const ws_product = await WS_Product.findOne({
          where: { id },
          include: [
            {
              model: ProductQuantity,
              where: { location_id: 1 }
            }
          ]
        });

        let type = item.action_type;
        let currentQty;
        let currentQtyId;
        let updatedQty;

        if (type === "Add") {
          currentQty = ws_product.ProductQuantities[0].amount;
          currentQtyId = ws_product.ProductQuantities[0].id;
          updatedQty = currentQty + item.quantity;
        } else {
          currentQty = ws_product.ProductQuantities[0].amount;
          currentQtyId = ws_product.ProductQuantities[0].id;
          updatedQty = currentQty - item.quantity;
        }
        // console.log(currentQty, updatedQty);
        const _updated_quantity = await ProductQuantity.update(
          { amount: updatedQty },
          { where: { id: currentQtyId } }
        );
        // console.log(_updated_quantity);
        let movement_array = [];
        console.log(item);

        // console.log(ws_product.dataValues);

        //Follow pattern of MovementLogService;

        let admin_info = {
          id: item.admin_id,
          name: item.admin_name
        };

        let log_detail = [
          {
            product_name: ws_product.name,
            product_id: ws_product.idPrestashop,
            product_reference: ws_product.reference,
            product_barcode: ws_product.barcode,
            action_id: 1,
            action_name: `Manual ${item.action_type}`,
            action_type: item.action_type,
            location_id: item.location_id,
            location_name: "Wishbeer",
            instock_quantity: ws_product.ProductQuantities[0].amount,
            action_quantity: item.quantity,
            reference_id: item.orderid_prestashop,
            remark: item.note
          }
        ];
        // console.log(log_detail);

        let sync_quantity = {
          product_ID: ws_product.idPrestashop,
          qty: updatedQty
        };

        //sync quantity with prestashop
        let products_update = {
          products_update: [sync_quantity],
          admin_id: String(admin_info.id),
          token: ""
        };
        const sync_result = await HttpService.request(
          products_update,
          config.wbUrl + "",
          "POST"
        );

        const movement_list = await MovementLogService.create(
          log_detail,
          admin_info
        );

        // console.log(movement_list);
      }

      return res.json({ data: result });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;

    try {
      const _ws_product = await WS_Product.destroy({ where: { id } });
      return res.json({ data: { ws_product: _ws_product } });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },

  movement: ws_productMovementController
};
