const sequelize = require("sequelize");
const ws_productMovementController = require("./ws_productMovementController");
const filterHelper = require("../../helpers/filterHelper");
const {
  WS_Product,
  ProductQuantity,
  WS_Movement_Logs_Flat,
  WS_Supplier
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
module.exports = {
  search: async (req, res) => {
    const {
      page = 0,
      pageSize = 100,
      id,
      Name,
      start = 0,
      search,
      filtered = [],
      filter,
      attributes
    } = req.body;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageSize)
    );

    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };
    try {
      const filter_fields = await filterHelper.separateField(filter);

      let obj_search = {
        distinct: true,
        required: true,
        subQuery: false, // เพิมมาเพราะว่า where ใน include หาไม่เจอ
        offset: pageNumber,
        limit: parseInt(pageSize),
        order: [["id", "DESC"]]
      };
      obj_search.where = filter_fields.map(props => {
        let field_name = props[0];
        let field_value = props[1];
        if (field_name == "supplier_name") {
          console.log(`supplier`);
          obj_search.include = [
            {
              model: WS_Supplier,
              attributes: ["supplier_name"],
              subQuery: false
              // required: true
            },
            { model: ProductQuantity, where: { location_id: 1 } }
          ];
          return {
            [field_name]: sequelize.where(
              sequelize.fn("lower", sequelize.col(field_name)),
              "LIKE",
              "%" + field_value + "%"
            )
          };
        } else {
          obj_search.include = [
            { model: ProductQuantity, where: { location_id: 1 } }
          ];
          return {
            [field_name]: sequelize.where(
              sequelize.fn("lower", sequelize.col(field_name)),
              "LIKE",
              "%" + field_value + "%"
            )
          };
        }
      });

      console.log("obj_search", obj_search);

      const ws_product = await WS_Product.findAndCountAll(obj_search);
      // console.log("ws_product", ws_product);
      dataArray.totalPage = Math.ceil(ws_product.count / pageSize);
      dataArray.data = ws_product.rows;
      dataArray.totalData = ws_product.count;

      return res.json(dataArray);
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  getCount: async (req, res) => {
    try {
      const ws_product = await WS_Product.findAndCountAll({
        include: [{ model: ProductQuantity, where: { location_id: 1 } }]
      });
      // console.log(ws_product);
      const dataArray = {
        count: ws_product.count
      };
      console.log(dataArray);
      return res.json(dataArray);
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  getAll: async (req, res) => {
    try {
      let ws_productlist = [];
      const _limit = 3000;
      const ws_productCount = await WS_Product.count({
        include: [{ model: ProductQuantity, where: { location_id: 1 } }]
      });
      const productTime = ws_productCount / _limit;
      for (let i = 0; i < productTime; i++) {
        let ws_product = await WS_Product.findAll({
          include: [{ model: ProductQuantity, where: { location_id: 1 } }],
          offset: i * _limit,
          limit: _limit
        });
        ws_productlist = ws_productlist.concat(ws_product);
      }
      return res.json(ws_productlist);
    } catch (error) {
      console.error(error);
      return res.status(500).json(error);
    }
  },
  get: async (req, res) => {
    const { id } = req.params;
    const { page = 0, Name, pageLimit = 50, offset = 0 } = req.query;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageLimit)
    );

    try {
      if (id) {
        const ws_product = await WS_Product.findOne({
          where: { id },
          include: [{ model: ProductQuantity }]
        });
        return res.json(ws_product);
      } else {
        const ws_product = await WS_Product.findAll({
          include: [
            {
              model: ProductQuantity,

              where: { location_id: 1 }
            }
            // {
            //   model: WS_Supplier,
            //   // attributes: ["supplier_name"],
            //   subQuery: false,
            //   required: true
            // }
          ],
          offset: offset,
          limit: pageLimit,
          order: [["idPrestashop", "ASC"]]
        });
        const dataArray = {
          page: 0,
          data: ws_product
        };
        return res.json(dataArray);
      }
    } catch (err) {
      console.error(err);
      return res.json(err);
    }
  },
  getDashboard: async (req, res) => {
    const ws_product_count = await WS_Product.findAndCountAll();
    const Op = sequelize.Op;
    const ws_product_negative = await WS_Product.findAndCountAll({
      include: [
        {
          model: ProductQuantity,

          where: { location_id: 1, amount: { [Op.lt]: 0 } }
        }
      ]
    });
    const ws_product_barcode = await WS_Product.findAndCountAll({
      where: { barcode: { [Op.eq]: "" } }
    });
    const ws_product_without = await WS_Product.findAndCountAll({
      include: [
        {
          model: WS_Supplier,
          where: { supplier_name: { [Op.eq]: null } },
          attributes: ["supplier_name"],
          subQuery: false,
          required: true
        }
      ]
    });
    const dataArray = {
      page: 0,
      count: ws_product_count.count,
      negative: ws_product_negative.count,
      negative_list: ws_product_negative.rows,
      missing_barcode: ws_product_barcode.count,
      without_supplier: ws_product_without.count
    };
    // console.log(dataArray);
    return res.json(dataArray);
  },
  create: async (req, res) => {
    const {
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;
    console.log(req.body);
    try {
      const _ws_product = await WS_Product.create({
        name,
        idprestashop,
        reference,
        status,
        barcode,
        product_img,
        shelf_location,
        product_type,
        base_price,
        bar_price,
        purchasing_price,
        final_price
      });
      return res.json({ data: { ws_product: _ws_product } });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  update: async (req, res) => {
    const {
      id,
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      purchasing_price,
      final_price
    } = req.body;

    try {
      const _ws_product = await WS_Product.update(
        {
          name,
          idprestashop,
          reference,
          status,
          barcode,
          product_img,
          shelf_location,
          product_type,
          base_price,
          bar_price,
          purchasing_price,
          final_price
        },
        { where: { id: id } }
      );
      return res.json({ data: { ws_product: _ws_product } });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;

    try {
      const _ws_product = await WS_Product.destroy({ where: { id } });
      return res.json({ data: { ws_product: _ws_product } });
    } catch (err) {
      console.error(err);
      return res.status(500).json(err);
    }
  },

  movement: ws_productMovementController
};
