const sequelize = require("sequelize");
const { or, like } = sequelize.Op;
const {
  WS_Product,
  ProductQuantity,
  WS_Movement_Logs_Flat
} = require("../../models");
const filterHelper = require("../../helpers/filterHelper");

module.exports = {
  get: async (req, res) => {
    const pageLimit = 100;
    let {
      page = 1,
      filter,
      column = "id",
      ordering = "DESC",
      keyword
    } = req.query;
    let offset = 0;
    if (page != 1) offset = pageLimit * (page - 1);
    try {
      let obj_search = {
        offset: offset,
        limit: pageLimit,
        order: [[column, ordering]]
      };
      if (keyword) {
        keyword = keyword
          .toLowerCase()
          .split("%20")
          .join(" ");
        // keyword = keyword.toLowerCase().split('%').join('!%');
        obj_search.where = {
          [or]: [
            { product_name: { [like]: "%" + keyword + "%" } },
            { reference_id: { [like]: keyword } },
            { product_barcode: { [like]: keyword } },
            { action_name: { [like]: "%" + keyword + "%" } },
            { remark: { [like]: "%" + keyword + "%" } },
            { user_name: { [like]: "%" + keyword + "%" } }
          ]
        };
      } else if (filter) {
        const filter_fields = await filterHelper.separateField(filter);
        obj_search.where = filter_fields.map(props => {
          let operator = "=";
          let field_name = props[0];
          let field_value = props[1];
          if (props[2]) operator = props[2];
          if (["product_name", "action_name", "remark"].includes(field_name)) {
            operator = "like";
            field_value = "%" + field_value + "%";
          }
          if (["createdAt", "updatedAt"].includes(field_name))
            field_value = new Date(field_value);
          return {
            [field_name]: sequelize.where(
              sequelize.fn("lower", sequelize.col(field_name)),
              operator,
              field_value
            )
          };
        });
      }
      const logs_result = await WS_Movement_Logs_Flat.findAndCountAll(
        obj_search
      );
      let count = logs_result.count;
      let last_page = Math.floor(count / 100);
      if (count % 100 > 0) last_page++;
      return res
        .status(200)
        .json({ last_page: last_page, rows: logs_result.rows });
    } catch (err) {
      console.error("=============");
      console.error("Controller: stock/LogController");
      console.error("Action: get");
      console.error("Params:", req.query);
      console.error("Path:", req.originalUrl);
      console.error("Method:", req.method);
      console.error("Error:", err);
      console.error("=============");
      return res.status(406).json(err);
    }
  }
};
