const sequelize = require("sequelize");
const { PurchaseOrder, PurchaseOrderDetail } = require("../../models");

module.exports = {
  // run: async (req, res) => {
  //     const {
  //         _date
  //     } = req.query;
  //     try {
  //         const product = await WS_Product.findAll({
  //             subQuery: false,
  //             distinct: true,
  //             include: [{
  //                     model: WS_Quantity,
  //                     where: {
  //                         location_id: 1
  //                     }
  //                 },
  //                 {
  //                     model: WS_Movement_Logs_Flat,
  //                     limit: 1,
  //                     required: false,
  //                     where: {
  //                         createdAt: {
  //                             [sequelize.Op.lt]: "2017-08-13"
  //                         }
  //                     },
  //                     order: [
  //                         ["id", "DESC"]
  //                     ]
  //                 }
  //             ],

  //             offset: 0,
  //             limit: 1000
  //             // order: [[ 'id', 'DESC']]
  //         });
  //         const Products = {
  //             products: product
  //         };
  //         return res.json(Products);
  //     } catch (err) {
  //         console.error(err);
  //         return res.status(500).json(err);
  //     }
  // },

  getReportPurchaseOrderBetween: async (req, res) => {
    const { _sdate, _edate } = req.query;
    try {
      const po_between = await PurchaseOrder.findAll({
        required: false,
        where: {
          createdAt: {
            [sequelize.Op.between]: [_sdate, _edate]
          }
        }
      });
      return res.json(po_between);
    } catch (err) {
      return res.status(500).json(err);
    }
  },

  getReportPurchaseOrderAll: async (req, res) => {
    let pageNumber = 0;

    const { _date, pageSize = 3000 } = req.query;

    let dataArray = {
      page: 0,
      totalPage: 0,
      po_list: []
    };

    try {
      const po = await PurchaseOrder.findAndCountAll({
        subQuery: false,
        distinct: true,
        logging: false,
        include: [
          {
            model: PurchaseOrderDetail
          }
        ],
        order: [["id", "DESC"]]
      });
      dataArray.po_list.push(po);
      const totalPage = Math.ceil(po.count / pageSize);
      dataArray.totalPage = totalPage;

      for (let i = 1; i < totalPage; i++) {
        pageNumber = i * pageSize;
        const po = await PurchaseOrder.findAndCountAll({
          subQuery: false,
          distinct: true,
          logging: false,
          include: [
            {
              model: PurchaseOrderDetail
            }
          ],

          offset: pageNumber,
          limit: Number(pageSize)
        });
        dataArray.po_list.push(po);
      }

      return res.json(dataArray);
    } catch (err) {
      return res.status(500).json(err);
    }
  }
};
