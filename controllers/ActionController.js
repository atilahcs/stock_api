const { Action, ErrorLog } = require('../models');
const filterHelper = require('../helpers/filterHelper');
const model = require('../models');
module.exports = {
  getDetail: async (req, res) => {
    try {
      let { action_id } = req.params;
      const action_detail_result = await Action.findOne({ where: { id: Number(action_id) }});
      if (!action_detail_result)
        throw { message: 'Action not found.' };
      return res.status(200).json({ action: action_detail_result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'ActionController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await Action.count({ where: filter_clause });
      const action_list = await Action.findAll({
        offset: offset,
        limit: limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ action_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'ActionController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { action_name, action_type } = req.body;
      if (!action_name && !action_type)
        throw { message: 'Information missing.' };
      const action_result = await Action.create({ action_name, action_type, created_time: new Date(), updated_time: new Date() }, { transaction });
      await transaction.commit();
      return res.status(200).json({ action: action_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'ActionController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { action_id } = req.params;
      let { action_name, action_type } = req.body;
      const update_result = await Action.update({ action_name, action_type, updated_time: new Date() }, { where: { id: Number(action_id) }, transaction });
      if (!update_result)
        throw { message: 'Unable to update action or action not found.' };
      await transaction.commit();
      const action_result = await Action.findOne({ where: { id: action_id }});
      return res.status(200).json({ action: action_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'ActionController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { action_id } = req.params;
      const delete_result = await Action.destroy({ where: { id: Number(action_id) }, transaction });
      if (!delete_result)
        throw { message: 'Unable to delete action or action not found.' };
      return res.status(200).json({ deleted: delete_result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'ActionController',
        action_name: 'delete',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}