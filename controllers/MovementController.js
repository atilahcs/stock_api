const { Movement, ErrorLog } = require("../models");
const filterHelper = require("../helpers/filterHelper");

module.exports = {
  getList: async (req, res) => {
    try {
      let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
      let { filter = "" } = req.query;
      let offset = 0;

      limit = Number(limit);
      if (page || page > 1) offset = limit * (page - 1);
      try {
        let filter_clause = await filterHelper(filter);
        const count = await Movement.count({ where: filter_clause });
        const movement_list = await Movement.findAll({
          offset: offset,
          limit: (limit < 0) ? null : limit,
          order: [[column, ordering]],
          where: filter_clause
        });
        return res.status(200).json({ count, movement_list });
      } catch (err) {
        let status = err && err.status ? err.status : 500;
        let message =
          err && err.message ? err.message : "Something went wrong.";
        let param = {
          query: req.query
        }
        await ErrorLog.create({
          controller_name: "MovementController",
          action_name: "getList",
          path: req.originalUrl,
          method: req.method,
          message: message,
          params: JSON.stringify(param),
          created_time: new Date()
        });
        return res.status(status).json({ error: message });
      }
    } catch (err) {
      return res.status(500).json({ error: err });
    }
  }
};
