const {
  Adjustment,
  AdjustmentDetail,
  ProductQuantity,
  Movement,
  ErrorLog
} = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
const model = require("../../models");

const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];

module.exports = {
  getDetail: async (req, res) => {
    let { adjustment_id } = req.params;
    try {
      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_id },
        include: [{ model: AdjustmentDetail }]
      });
      if (!adjustment_result)
        throw { status: 400, message: "Adjustment not found." };
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await Adjustment.count({ where: filter_clause });
      const adjustment_list = await Adjustment.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, adjustment_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { adjustment_info, adjustment_detail, admin_info } = req.body;
      if (!adjustment_info.location_id || !adjustment_info.location_name)
        throw {
          status: 400,
          message: "Please specific location to adjust stock."
        };

      let adjustment = {
        created_by: admin_info.id,
        created_by_name: admin_info.name,

        title: adjustment_info.title,
        remark: adjustment_info.remark,
        status: "Pending",

        location_id: adjustment_info.location_id,
        location_name: adjustment_info.location_name,

        created_time: new Date(),
        updated_time: new Date()
      };
      const adjustment_create = await Adjustment.create(adjustment, {
        transaction
      });
      for (let detail of adjustment_detail) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_prestashop_id,
            location_id: adjustment_info.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              adjustment_info.location_name
            } location.`
          };

        await AdjustmentDetail.create(
          {
            adjustment_id: adjustment_create.id,
            product_id: detail.product_prestashop_id,
            pid: detail.id,
            product_name: detail.name,
            product_img: detail.product_img,
            product_barcode: detail.barcode,
            product_reference: detail.reference,
            purchasing_price: detail.purchasing_price,
            base_price: detail.base_price,
            instock_quantity: detail.ProductQuantities[0].amount,
            location_id: adjustment_info.location_id,
            location_name: adjustment_info.location_name,
            counter_quantity: 0,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
      }
      await transaction.commit();

      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_create.id },
        include: [{ model: AdjustmentDetail }]
      });
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "create",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { adjustment_id } = req.params;
      let { adjustment_info, adjustment_detail, admin_info } = req.body;
      adjustment_id = Number(adjustment_id);
      const adjustment_exist = await Adjustment.findOne({
        where: { id: adjustment_id }
      });
      if (!adjustment_exist)
        throw { status: 400, message: "Adjustment does not exist." };
      let adjustment = {
        title: adjustment_info.title,
        remark: adjustment_info.remark,
        status: adjustment_info.status,
        location_id: adjustment_info.location_id,
        location_name: adjustment_info.location_name,
        updated_time: new Date()
      };
      await Adjustment.update(adjustment, {
        where: { id: adjustment_id },
        transaction
      });
      await AdjustmentDetail.destroy({ where: { adjustment_id }, transaction });
      for (let detail of adjustment_detail) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: adjustment_info.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              adjustment_info.location_name
            } location.`
          };

        await AdjustmentDetail.create(
          {
            adjustment_id: adjustment_id,
            product_id: detail.product_id,
            pid: detail.pid,
            product_name: detail.product_name,
            product_img: detail.product_img,
            location_id: adjustment_info.location_id,
            location_name: adjustment_info.location_name,
            counter_quantity: detail.coutner_quantity || 0,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
      }
      await transaction.commit();
      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_id },
        include: [{ model: AdjustmentDetail }]
      });
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  //Note : jump created a new saveAdjust for save counted list.
  saveAdjustment: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { adjustment_id } = req.params;
      let { adjustment_info, adjustment_detail, admin_info } = req.body;
      adjustment_id = Number(adjustment_id);
      const adjustment_exist = await Adjustment.findOne({
        where: { id: adjustment_id }
      });

      if (!adjustment_exist)
        throw { status: 400, message: "Adjustment does not exist." };
      let adjustment = {
        title: adjustment_info.title,
        remark: adjustment_info.remark,
        status: adjustment_info.status,
        location_id: adjustment_info.location_id,
        location_name: adjustment_info.location_name,
        updated_time: new Date(),
        counted_by: admin_info.id,
        counted_by_name: admin_info.name
      };
      await Adjustment.update(adjustment, {
        where: { id: adjustment_id },
        transaction
      });

      for (let detail of adjustment_detail) {
        const detail_exist = await AdjustmentDetail.findOne({
          where: { id: detail.id }
        });
        if (detail_exist) {
          await AdjustmentDetail.update(detail, {
            where: { id: detail.id },
            transaction
          });
        } else {
          await AdjustmentDetail.create(detail, {
            transaction
          });
        }
      }
      await transaction.commit();
      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_id },
        include: [{ model: AdjustmentDetail }]
      });
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { adjustment_id } = req.params;
      const adjustment = await Adjustment.findOne({
        where: { id: adjustment_id }
      });
      if (!adjustment)
        throw { status: 400, message: "Adjustment does not exist." };
      if (adjustment.status == "Complete")
        throw {
          status: 400,
          message:
            "This adjustment is already completed. Cannot delete this adjustment."
        };
      await Adjustment.destroy({ where: { id: adjustment_id }, transaction });
      await AdjustmentDetail.destroy({
        where: { adjustment_id: adjustment_id },
        transaction
      });
      await transaction.commit();
      return res.status(200).json({ deleted: true });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "delete",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  adjust: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { adjustment_id } = req.params;
      let {
        adjustment_info,
        adjustment_detail,
        admin_info,
        action_info
      } = req.body;
      let sync_list = [];

      adjustment_id = Number(adjustment_id);
      const adjustment_exist = await Adjustment.findOne({
        where: { id: adjustment_id }
      });
      if (!adjustment_exist)
        throw { status: 400, message: "Adjustment does not exist." };
      if (adjustment_exist.status == "Complete")
        throw { status: 400, message: "Adjustment is already adjusted stock." };
      let adjustment = {
        title: adjustment_info.title,
        remark: adjustment_info.remark,
        status: "Complete",
        location_id: adjustment_info.location_id,
        location_name: adjustment_info.location_name,
        updated_time: new Date(),
        counted_by: admin_info.id,
        counted_by_name: admin_info.name
      };
      await Adjustment.update(adjustment, {
        where: { id: adjustment_id },
        transaction
      });

      for (let detail of adjustment_detail) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: adjustment_info.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              adjustment_info.location_name
            } location.`
          };
        let temp = {
          adjustment_id: adjustment_id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_barcode: detail.product_barcode,
          product_reference: detail.product_reference,
          location_id: adjustment_info.location_id,
          location_name: adjustment_info.location_name,
          counter_quantity: detail.counter_quantity || 0,
          instock_quantity: product.amount,
          difference_quantity: Math.abs(
            Number(detail.instock_quantity) - Number(detail.counter_quantity)
          ),
          purchasing_price: detail.purchasing_price,
          base_price: detail.base_price,
          updated_time: detail.id ? new Date() : null
        };

        await AdjustmentDetail.update(temp, {
          where: { id: detail.id },
          transaction
        });

        if (temp.difference_quantity == 0) continue;

        await ProductQuantity.update(
          { amount: temp.counter_quantity },
          {
            where: {
              product_id: temp.product_id,
              location_id: temp.location_id
            },
            transaction
          }
        );

        sync_list.push({
          product_ID: temp.product_id,
          qty: temp.counter_quantity
        });

        let movement = {
          user_id: admin_info.id,
          user_name: admin_info.name,
          product_id: Number(detail.product_id),
          pid: detail.pid,
          product_name: detail.product_name,
          product_barcode: detail.product_barcode,
          product_reference: detail.product_reference,
          action_id:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].id
              : action_info[1].id,
          action_name:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].action_name
              : action_info[1].action_name,
          action_type:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].action_type
              : action_info[1].action_type,
          location_id: adjustment_info.location_id,
          location_name: adjustment_info.location_name,
          instock_quantity: temp.instock_quantity,
          action_quantity: Math.abs(temp.difference_quantity),
          after_action_quantity: temp.counter_quantity,
          reference_id: `adjustment ${adjustment_id}`
        };
        const movement_result = await Movement.create(movement, {
          transaction
        });
        await AdjustmentDetail.update(
          { movement_id: movement_result.id },
          { where: { id: detail.id }, transaction }
        );
      }
      let products_update = {
        products_update: sync_list,
        admin_id: String(admin_info.id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_id },
        include: [{ model: AdjustmentDetail }]
      });
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "adjust",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
};
