const {
  StockCount,
  StockCountDetail,
  Adjustment,
  AdjustmentDetail,
  ProductQuantity,
  Movement,
  ErrorLog
} = require("../../models");
const model = require("../../models");
module.exports = {
  adjust: async (req, res) => {
    console.log(`adjust stock count`);
    let transaction = await model.sequelize.transaction();
    try {
      let { stock_count_id } = req.params;
      let { admin_info, action_info, stockcount_info } = req.body;
      const stockcount = await StockCount.findOne({
        where: { id: stock_count_id },
        include: [{ model: StockCountDetail }]
      });
      if (!stockcount)
        throw { status: 400, message: "StockCount does not exist." };
      if (stockcount.status == "Adjusted")
        throw { status: 400, message: "This StockCount is already adjusted." };

      const adjustment_create = await Adjustment.create(
        {
          scid: stock_count_id,
          created_by: admin_info.id,
          created_by_name: admin_info.name,
          title: stockcount.title,
          remark: stockcount.remark,
          status: "Complete",
          location_id: stockcount.location_id,
          location_name: stockcount.location_name,
          counted_by: stockcount.created_by,
          counted_by_name: stockcount.created_by_name,
          updated_time: new Date()
        },
        { transaction }
      );

      // console.log(adjustment_create);
      await StockCount.update(
        { status: "Adjusted", updated_time: new Date() },
        { where: { id: stock_count_id }, transaction }
      );

      for (let detail of stockcount.StockCountDetails) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: stockcount.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              stockcount.location_name
            } location.`
          };

        let temp = {
          adjustment_id: adjustment_create.id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_barcode: detail.product_barcode,
          product_reference: detail.product_reference,
          purchasing_price: detail.purchasing_price,
          base_price: detail.base_price,
          location_id: stockcount.location_id,
          location_name: stockcount.location_name,
          instock_quantity: product.amount,
          counter_quantity: detail.counter_quantity,
          difference_quantity: Math.abs(
            product.amount - detail.counter_quantity
          ),
          movement_id: null,
          created_time: new Date(),
          updated_time: new Date()
        };

        const adjustmentDetail = await AdjustmentDetail.create(temp, {
          transaction
        });

        await StockCountDetail.update(
          { instock_quantity: temp.instock_quantity },
          { where: { id: detail.id }, transaction }
        );

        if (temp.difference_quantity == 0) continue;

        await ProductQuantity.update(
          { amount: temp.counter_quantity, updated_time: new Date() },
          {
            where: {
              product_id: temp.product_id,
              location_id: temp.location_id
            },
            transaction
          }
        );

        let movement = {
          user_id: admin_info.id,
          user_name: admin_info.name,
          product_id: Number(detail.product_id),
          pid: detail.pid,
          product_name: detail.product_name,
          product_barcode: detail.product_barcode,
          product_reference: detail.product_reference,
          action_id:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].id
              : action_info[1].id,
          action_name:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].action_name
              : action_info[1].action_name,
          action_type:
            temp.counter_quantity > temp.instock_quantity
              ? action_info[0].action_type
              : action_info[1].action_type,
          location_id: stockcount_info.location_id,
          location_name: stockcount_info.location_name,
          instock_quantity: temp.instock_quantity,
          action_quantity: Math.abs(temp.difference_quantity),
          after_action_quantity: temp.counter_quantity,
          reference_id: `adjustment ${adjustment_create.id}`
        };
        const movementResult = await Movement.create(movement, { transaction });

        await AdjustmentDetail.update(
          { movement_id: movementResult.id },
          { where: { id: adjustmentDetail.id }, transaction }
        );
      }

      await transaction.commit();
      const stockcount_result = await StockCount.findOne({
        where: { id: stock_count_id },
        include: [
          { model: StockCountDetail },
          { model: Adjustment, include: [{ model: AdjustmentDetail }] }
        ]
      });
      return res.status(200).json({ stockcount: stockcount_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "adjustment/AdjustmentController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { stock_count_id } = req.params;
      let { admin_info } = req.body;
      const stockcount = await StockCount.findOne({
        where: { id: stock_count_id },
        include: [{ model: StockCountDetail }]
      });
      if (!stockcount)
        throw { status: 400, message: "StockCount does not exist." };

      const adjustment_create = await Adjustment.create(
        {
          created_by: admin_info.id,
          created_by_name: admin_info.name,
          title: stockcount.title,
          remark: stockcount.remark,
          status: "Pending",
          location_id: stockcount.location_id,
          location_name: stockcount.location_name,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );

      for (let detail of stockcount.StockCountDetails) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: stockcount.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              stockcount.location_name
            } location.`
          };

        let temp = {
          adjustment_id: adjustment_create.id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_barcode: detail.product_barcode,
          product_reference: detail.product_reference,
          location_id: stockcount.location_id,
          location_name: stockcount.location_name,
          created_time: new Date(),
          updated_time: new Date()
        };
        await AdjustmentDetail.create(temp, { transaction });
      }

      await transaction.commit();
      const adjustment_result = await Adjustment.findOne({
        where: { id: adjustment_create.id },
        include: [{ model: AdjustmentDetail }]
      });
      return res.status(200).json({ adjustment: adjustment_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "stockcount/AdjustmentController",
        action_name: "create",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
};
