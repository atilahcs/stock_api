const {
  StockCount,
  StockCountDetail,
  ProductQuantity,
  ErrorLog
} = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
const model = require("../../models");
const AdjustmentController = require("./AdjustmentController");
module.exports = {
  getDetail: async (req, res) => {
    let { stock_count_id } = req.params;
    try {
      const stockCountResult = await StockCount.findOne({
        where: { id: stock_count_id },
        include: [{ model: StockCountDetail }]
      });
      if (!stockCountResult)
        throw { status: 400, message: "StockCount not found." };
      return res.status(200).json({ stockcount: stockCountResult });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "stockcount/StockCountController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await StockCount.count({ where: filter_clause });
      const stockcount_list = await StockCount.findAll({
        offset: offset,
        limit: limit < 0 ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, stockcount_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "stockcount/StockCountController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { stockcount_info, stockcount_detail, admin_info } = req.body;
      if (!stockcount_info.location_id || !stockcount_info.location_name)
        throw {
          status: 400,
          message: "Please specify location to count stock."
        };
      let stockcount = {
        created_by: admin_info.id,
        created_by_name: admin_info.name,
        title: stockcount_info.title,
        remark: stockcount_info.remark,
        status: "Complete",
        location_id: stockcount_info.location_id,
        location_name: stockcount_info.location_name,
        created_time: new Date(),
        updated_time: new Date()
      };
      const stockcount_create = await StockCount.create(stockcount, {
        transaction
      });
      for (let detail of stockcount_detail) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: stockcount_info.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              stockcount_info.location_name
            } location.`
          };

        await StockCountDetail.create(
          {
            stock_count_id: stockcount_create.id,
            product_id: detail.product_id,
            pid: detail.pid,
            product_name: detail.product_name,
            product_barcode: detail.product_barcode,
            product_reference: detail.product_reference,
            location_id: stockcount_info.location_id,
            location_name: stockcount_info.location_name,
            instock_quantity: detail.instock_quantity,
            counter_quantity: detail.counter_quantity,
            purchasing_price: detail.purchasing_price,
            base_price: detail.base_price,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
      }
      await transaction.commit();
      const stockcount_result = await StockCount.findOne({
        where: { id: stockcount_create.id },
        include: [{ model: StockCountDetail }]
      });
      return res.status(200).json({ stockcount: stockcount_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "stockcount/StockCountController",
        action_name: "create",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { stock_count_id } = req.params;
      let { stockcount_info, stockcount_detail } = req.body;
      stock_count_id = Number(stock_count_id);
      const stockcount_exist = await StockCount.findOne({
        where: { id: stock_count_id }
      });
      if (!stockcount_exist)
        throw { status: 400, message: "StockCount does not exist." };
      let stockcount = {
        title: stockcount_info.title,
        remark: stockcount_info.remark,
        status: stockcount_info.status,
        location_id: stockcount_info.location_id,
        location_name: stockcount_info.location_name,
        updated_time: new Date()
      };
      await StockCount.update(stockcount, {
        where: { id: stock_count_id },
        transaction
      });
      await StockCountDetail.destroy({ where: { stock_count_id } });
      for (let detail of stockcount_detail) {
        let product = await ProductQuantity.findOne({
          where: {
            product_id: detail.product_id,
            location_id: stockcount_info.location_id
          }
        });
        if (!product)
          throw {
            status: 400,
            message: `Product: ${detail.product_name} does not exist in ${
              stockcount_info.location_name
            } location.`
          };

        await StockCountDetail.create(
          {
            stock_count_id: stock_count_id,
            product_id: detail.product_id,
            pid: detail.pid,
            product_name: detail.product_name,
            product_barcode: detail.product_barcode,
            product_reference: detail.product_reference,
            location_id: stockcount_info.location_id,
            location_name: stockcount_info.location_name,
            instock_quantity: detail.instock_quantity,
            counter_quantity: detail.counter_quantity,
            purchasing_price: detail.purchasing_price,
            base_price: detail.base_price,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
      }
      await transaction.commit();
      const stockcount_result = await StockCount.findOne({
        where: { id: stock_count_id },
        include: [{ model: StockCountDetail }]
      });
      return res.status(200).json({ stockcount: stockcount_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "stockcount/StockCountController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { stock_count_id } = req.params;
      const stockcount = await StockCount.findOne({
        where: { id: stock_count_id }
      });
      if (!stockcount)
        throw { status: 400, message: "StockCount does not exist." };
      if (stockcount.status == "Adjusted")
        throw {
          status: 400,
          message:
            "This stockcount is already adjusted. Cannot delete this stockcount."
        };
      await StockCount.destroy(
        { where: { id: stock_count_id } },
        { transaction }
      );
      await StockCountDetail.destroy(
        { where: { id: stock_count_id } },
        { transaction }
      );
      await transaction.commit();
      return res.status(200).json({ deleted: true });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "stockcount/StockCountController",
        action_name: "delete",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  adjustment: AdjustmentController
};
