const { Supplier, ErrorLog } = require('../../models');
const model = require("../../models");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
module.exports = {
  importAll: async (req, res) => {
    let data = {
      getSupplierList: "",
      token: ""
    };
    let transaction = await model.sequelize.transaction();
    try {
      const supplier_list = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );
      const created_time = new Date();
      for (item of supplier_list.supplierList) {
        await Supplier.findOrCreate({
          where: { prestashop_id: item.id_supplier },
          defaults: {
            supplier_name: item.name,
            address: "",
            city: "",
            zipcode: "",
            country: "",

            street_address: "",
            street_city: "",
            street_zipcode: "",
            street_country: "",

            sale_setting: "",
            purchase_setting: "",

            bill_duedate: "",
            invoice_duedate: "",
            email: "",
            tax_id: "",

            contact: "",
            contact_no: "",
            xero_supplier_id: "",
            prestashop_id: item.id_supplier,
            created_time: created_time,
            updated_time: created_time
          },
          transaction
        });
      }
      await transaction.commit();

      return res.status(200).json({ success: true, message: 'Import supplier successful.' });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: 'cron/SupplierController',
        action_name: 'importAll',
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  importLatest: async (req, res) => {
    let supplier_model_array = [];

    let transaction = await model.sequelize.transaction();
    try {
      const last_supplier_id = await Supplier.findOne({
        logging: false,
        order: [["prestashop_id", "DESC"]]
      });

      const check_prestashop = {
        getNewSupplierList: true,
        supplier_last_id: last_supplier_id.prestashop_id,
        token: ""
      };

      const supplier_new_list = await HttpService.request(
        check_prestashop,
        config.wbUrl + "",
        "POST"
      );

      if (supplier_new_list.supplierList.length === 0) {
        return res.status(200).json({ success: true, message: `Supplier already up-to-date` });
      } else {
        let created_time = new Date();
        for (let item of supplier_new_list.supplierList) {
          const supplier_model = {
            supplier_name: item.name,
            address: "",
            city: "",
            zipcode: "",
            country: "",

            street_address: "",
            street_city: "",
            street_zipcode: "",
            street_country: "",

            sale_setting: "",
            purchase_setting: "",

            bill_duedate: "",
            invoice_duedate: "",
            email: "",
            tax_id: "",

            contact: "",
            contact_no: "",
            xero_supplier_id: "",
            prestashop_id: item.id_supplier,
            created_time: created_time,
            updated_time: created_time
          };
          supplier_model_array.push(supplier_model);
        }
        await Supplier.bulkCreate(
          supplier_model_array,
          { transaction }
        );
        await transaction.commit();
        return res
          .status(200)
          .json({ success: true, message: 'Import supplier successful.' });
      }
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "cron/SupplierController",
        action_name: "importLatest",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
}