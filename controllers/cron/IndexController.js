const ProductController = require('./ProductController');
const SupplierController = require('./SupplierController');
const SupplierProductController = require('./SupplierProductController');
module.exports = {
  product: ProductController,
  supplier: SupplierController,
  supplierProduct: SupplierProductController
}