const { Product, SupplierProduct, ProductQuantity, ErrorLog } = require('../../models');
const model = require("../../models");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
module.exports = {
  importAll: async (req, res) => {
    var data = {
      stock: {
        getList: ""
      },
      token: ""
    };
    let transaction = await model.sequelize.transaction();

    try {
      const product_ps = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );
      if (!product_ps) {
        throw { message: 'Unable to get product from prestashop.' };
      }

      for (let item of product_ps.products) {
        const product_result = await Product.findOne({ where: { product_prestashop_id: item.idprestashop }});
        if (product_result)
          continue;
        const product = await Product.create({
          product_prestashop_id: item.idprestashop,
          name: item.name,
          barcode: item.barcode,
          reference: item.reference,
          status: 1,
          product_img: item.product_img,
          created_time: new Date(),
          updated_time: new Date()
        }, { transaction });
        await ProductQuantity.create({
          amount: 0,
          location_id: 1,
          location_name: "Wishbeer",
          product_id: item.idprestashop,
          pid: product.id,
          created_time: new Date(),
          updated_time: new Date()
        }, { transaction });
      }
      await transaction.commit();
      return res.status(200).json({ message: "Import successful", success: true });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "cron/ProductController",
        action_name: "createOneTime",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  importLatest: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      const last_product = await Product.findOne({
        limit: 1,
        attributes: ["idprestashop"],
        order: [["idprestashop", "DESC"]]
      });

      const check_prestashop = {
        getNewProductAfterLastID: last_product.dataValues.idprestashop,
        token: ""
      };

      const product_with_supplier = await HttpService.request(
        check_prestashop,
        config.wbUrl + "",
        "POST"
      );
      let product_to_email = [];
      if (product_with_supplier.new_products.length === 0) {
        return res.status(200).json({ message: `There is no new product created in ps` });
      } else {
        product_with_supplier.new_products.map(product => {
          const data = {
            product_prestashop_id: product.id_product,
            name: product.name,
            reference: product.reference,
            status: 1,
            barcode: product.ean13,
            product_img: product.img_url,
            default_supplier_id: product.id_default_supplier,
            default_supplier_name: product.default_supplier_name
          };
          product_to_email.push(data);
        });
      }
      const create_product = await Product.bulkCreate(product_to_email, { transaction });

      for (let product of create_product) {
        const data_create_quantity = {
          amount: 0,
          location_id: 1,
          location_name: "Wishbeer",
          product_id: product.dataValues.product_prestashop_id,
          pid: product.dataValues.id,
          created_time: new Date(),
          updated_time: new Date()
        };
        const create_quantity = await ProductQuantity.findOrCreate({
          where: { product_id: product.dataValues.product_prestashop_id },
          defaults: data_create_quantity,
          transaction
        });

        const check_supplier = {
          getProductSupplier: {
            product_id: product.dataValues.product_prestashop_id
          },
          token: ""
        };
        const product_supplier = await HttpService.request(
          check_supplier,
          config.wbUrl + "",
          "POST"
        );

        if (product_supplier.product_supplier_info.length) {
          const create_supplier = await SupplierProduct.create({
            idprestashop: product.id_product,
            id_supplier: product_supplier.product_supplier_info[0].id_supplier,
            created_time: new Date(),
            updated_time: new Date()
          }, { transaction });
        }
      }
      await transaction.commit();
      const sent = await sendMailService.send(product_to_email);
      if (sent.message === "sent")
        return res.status(200).json({ sucess: true, message: "Import latest product(s) successful." });
      else
        return res.status(200).json({ success: true, message: 'Import latest product(s) successful with no email sent.' });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      await ErrorLog.create({
        controller_name: 'cron/ProductController',
        action_name: 'importLatest',
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  updateInfoFromPs: async (req, res) => {
    const data = {
      getAllProductsInfo: "",
      token: ""
    };
    let transaction = await model.sequelize.transaction();
    try {
      const product_ps = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );
      for (let update_item of product_ps.products_info) {
        await Product.update(
          {
            name: update_item.product_name,
            barcode: update_item.barcode,
            reference: update_item.reference,
            product_img: update_item.product_image,
            base_price: update_item.base_price,
            wholesale_price: update_item.wholesale_price,
            purchasing_price: update_item.purchasing_price,
            final_price: update_item.final_price,
            default_supplier_id: update_item.id_default_supplier,
            default_supplier_name: update_item.default_supplier_name,
            updated_time: new Date()
          },
          { where: { product_prestashop_id: update_item.prestashop_id }, transaction }
        );
      }

      await transaction.commit();
      return res
        .status(200)
        .json({ success: true, message: 'Products have been updated.' });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "cron/ProductController",
        action_name: "updateInfoFromPs",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
}