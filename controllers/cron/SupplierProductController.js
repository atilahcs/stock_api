const { Product, Supplier, SupplierProduct, ProductQuantity, ErrorLog } = require('../../models');
const model = require("../../models");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
module.exports = {
  //not finished
  importAllSupplierProduct: async (req, res) => {
    let supplier_product_list = [];
    let transaction = await model.sequelize.transaction();
    try {
      const supplier_list = await Supplier.findAll({
        logging: false,
        attributes: ['id', "supplier_name", "prestashop_id"],
        order: [["prestashop_id", "DESC"]]
      });
      
      for (let supplier of supplier_list) {
        let data = {
          supplier_id: supplier.prestashop_id,
          token: ""
        };
        
        const supplier_detail = await HttpService.request(
          data,
          config.wbUrl + "",
          "post"
        );

        for (let item of supplier_detail.supplierDetail) {
          const product = await Product.findOne({ where: { product_prestashop_id: item.id_product }});
          if (!product)
            continue;
          let data_in_array = {
            product_id: Number(item.id_product),
            pid: product.id,
            supplier_id: Number(item.id_supplier),
            sid: supplier.id,
            created_time: new Date(),
            updated_time: new Date()
          };
          supplier_product_list.push(data_in_array);
        }
      }
      // console.log(supplier_product_list);
      await SupplierProduct.bulkCreate(
        supplier_product_list,
        { transaction }
      );
      await transaction.commit();
      return res.status(200).json({ success: true, message: 'Supplier product have been imported.' });
    } catch (err) {
      console.log(err);
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "cron/SupplierProductController",
        action_name: "importAllSupplierProduct",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
}