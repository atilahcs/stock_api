const { BranchLocation } = require('./../models');
const model = require('./../models');
const filterHelper = require('./../helpers/filterHelper');
module.exports = {
  getDetail: async (req, res) => {
    try {
      let { location_id } = req.params;
      const locationResult = await BranchLocation.findOne({ where: { id: location_id } });
      if (!locationResult)
        throw { message: 'Location not found.' };
      return res.status(200).json({ location: locationResult });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'BranchLocationController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await Adjustment.count({ where: filter_clause });
      const location_list = await BranchLocation.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, location_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.query
      }
      await ErrorLog.create({
        controller_name: 'BranchLocationController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { location_name } = req.body;
      const location_result = await BranchLocation.create({ location_name }, { transaction });
      await transaction.commit();
      return res.status(200).json({ location: location_result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'BranchLocationController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { location_id } = req.params;
      let { location_name } = req.body;
      const result = await BranchLocation.update({ location_name }, { where: { id: location_id }, transaction });
      if (!result)
        throw { message: 'Unable to update location.' };

      await transaction.commit();
      const branch_location = await BranchLocation.findOne({ where: { id: location_id }});
      return res.status(200).json({ branchLocation: branch_location });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        bost: req.body
      }
      await ErrorLog.create({
        controller_name: 'BranchLocationController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { location_id } = req.params;
      const result = await BranchLocation.destroy({ where: { id: location_id }, transaction });
      await transaction.commit();
      return res.status(200).json({ deleted: result });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'BranchLocationController',
        action_name: 'delete',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
}