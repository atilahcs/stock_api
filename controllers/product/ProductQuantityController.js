/***
 * === Product Quantity Controller ====
 * - Description : control product that come with quantity.
 *
 * Functions :
 * 1. Update
 * - Desc. : update product quantity ( increase , decrease )
 * - Usage : for manual scanner.
 *
 * 2. Sync product quantity
 * - Desc. : Sync product to prestashop with updated quantity
 * - Usage : Sync one by one , Sync all
 *
 */

var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const HttpService = require("../../services/HttpService");

const sequelize = require("sequelize");
const filterHelper = require("../../helpers/filterHelper");
const {
  Product,
  ProductQuantity,
  Supplier,
  Movement,
  BranchLocation,
  ErrorLog
} = require("../../models");
const paginationHelper = require("../../helpers/paginationHelper");
const model = require("../../models");

module.exports = {
  addQuantity: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { product_id, location_id } = req.params;
      let { amount, remark, admin_info, action_info } = req.body;
      const productResult = await Product.findOne({
        where: { product_prestashop_id: product_id }
      });
      if (!productResult) throw { message: "Product does not exist." };
      const locationResult = await BranchLocation.findOne({
        where: { id: location_id }
      });
      if (!locationResult) throw { message: "Location does not exist." };
      const productQty = await ProductQuantity.findOne({
        where: { product_id, location_id }
      });
      if (!productQty)
        throw { message: "Product does not regist to this location." };

      let instock_quantity = productQty.amount;
      let after_action_quantity = instock_quantity + amount;
      await ProductQuantity.update(
        { amount: after_action_quantity, updated_time: new Date() },
        { where: { product_id, location_id }, transaction }
      );
      await Movement.create(
        {
          user_id: admin_info.id,
          user_name: admin_info.name,
          product_id: product_id,
          pid: productResult.id,
          product_name: productResult.product_name,
          product_reference: productResult.reference,
          product_barcode: productResult.barcode,
          action_id: action_info.id,
          action_name: action_info.action_name,
          action_type: action_info.action_type,
          location_id: location_id,
          location_name: locationResult.location_name,
          instock_quantity,
          action_quantity: amount,
          after_action_quantity,
          remark: remark,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );
      let products_update = {
        products_update: [
          {
            product_ID: product_id,
            qty: after_action_quantity > 0 ? after_action_quantity : 0
          }
        ],
        admin_id: String(admin_info.id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      return res
        .status(200)
        .json({ product: productResult, amount: after_action_quantity });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "addQuantity",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  addQuantityList: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { location_id } = req.params;
      let { product_list, remark, admin_info, action_info } = req.body;
      let sync_list = [];

      for (let detail of product_list) {
        const productResult = await Product.findOne({
          where: { product_prestashop_id: detail.product_id }
        });
        if (!productResult) throw { message: "Product does not exist." };
        const locationResult = await BranchLocation.findOne({
          where: { id: location_id }
        });
        if (!locationResult) throw { message: "Location does not exist." };
        const productQty = await ProductQuantity.findOne({
          where: { product_id: detail.product_id, location_id }
        });
        if (!productQty)
          throw { message: "Product does not regist to this location." };

        let instock_quantity = productQty.amount;
        let after_action_quantity = instock_quantity + detail.quantity;

        await ProductQuantity.update(
          { amount: after_action_quantity, updated_time: new Date() },
          { where: { product_id: detail.product_id, location_id }, transaction }
        );

        await Movement.create(
          {
            user_id: admin_info.admin_id,
            user_name: admin_info.admin_name,
            product_id: detail.product_id,
            pid: productResult.id,
            product_name: productResult.name,
            product_reference: productResult.reference,
            product_barcode: productResult.barcode,
            action_id: action_info.id,
            action_name: action_info.action_name,
            action_type: action_info.action_type,
            location_id: location_id,
            location_name: locationResult.location_name,
            reference_id: detail.reference_id,
            instock_quantity,
            action_quantity: detail.quantity,
            after_action_quantity,
            remark: remark,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
        sync_list.push({
          product_ID: detail.product_id,
          qty: detail.amount
        });
        detail.amount = after_action_quantity;
      }
      let products_update = {
        products_update: sync_list,
        admin_id: String(admin_info.admin_id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      return res.status(200).json({ product_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "addQuantityList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  removeQuantity: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { product_id, location_id } = req.params;
      let { amount, remark, admin_info, action_info } = req.body;
      const productResult = await Product.findOne({
        where: { product_prestashop_id: product_id }
      });
      if (!productResult) throw { message: "Product does not exist." };
      const locationResult = await BranchLocation.findOne({
        where: { id: location_id }
      });
      if (!locationResult) throw { message: "Location does not exist." };
      const productQty = await ProductQuantity.findOne({
        where: { product_id, location_id }
      });
      if (!productQty)
        throw { message: "Product does not regist to this location." };

      let instock_quantity = productQty.amount;
      let after_action_quantity = instock_quantity - amount;
      await ProductQuantity.update(
        { amount: after_action_quantity, updated_time: new Date() },
        { where: { product_id, location_id }, transaction }
      );
      await Movement.create(
        {
          user_id: admin_info.id,
          user_name: admin_info.name,
          product_id: product_id,
          pid: productResult.id,
          product_name: productResult.product_name,
          product_reference: productResult.reference,
          product_barcode: productResult.barcode,
          action_id: action_info.id,
          action_name: action_info.action_name,
          action_type: action_info.action_type,
          location_id: location_id,
          location_name: locationResult.location_name,
          instock_quantity,
          action_quantity: amount,
          after_action_quantity,
          remark: remark,
          created_time: new Date(),
          updated_time: new Date()
        },
        { transaction }
      );
      let products_update = {
        products_update: [
          {
            product_ID: product_id,
            qty: after_action_quantity > 0 ? after_action_quantity : 0
          }
        ],
        admin_id: String(admin_info.id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      return res
        .status(200)
        .json({ product: productResult, amount: after_action_quantity });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "removeQuantity",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  removeQuantityList: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { location_id } = req.params;
      let { product_list, remark, admin_info, action_info } = req.body;

      let sync_list = [];
      for (let detail of product_list) {
        const productResult = await Product.findOne({
          where: { product_prestashop_id: detail.product_id }
        });
        if (!productResult) throw { message: "Product does not exist." };
        const locationResult = await BranchLocation.findOne({
          where: { id: location_id }
        });
        if (!locationResult) throw { message: "Location does not exist." };
        const productQty = await ProductQuantity.findOne({
          where: { product_id: detail.product_id, location_id }
        });
        if (!productQty)
          throw { message: "Product does not regist to this location." };

        let instock_quantity = productQty.amount;
        let after_action_quantity = instock_quantity - detail.quantity;
        await ProductQuantity.update(
          { amount: after_action_quantity, updated_time: new Date() },
          { where: { product_id: detail.product_id, location_id }, transaction }
        );
        await Movement.create(
          {
            user_id: admin_info.admin_id,
            user_name: admin_info.admin_name,
            product_id: detail.product_id,
            pid: productResult.id,
            product_name: productResult.name,
            product_reference: productResult.reference,
            product_barcode: productResult.barcode,
            action_id: action_info.id,
            action_name: action_info.action_name,
            action_type: action_info.action_type,
            location_id: location_id,
            location_name: locationResult.location_name,
            reference_id: detail.reference_id,
            instock_quantity,
            action_quantity: detail.quantity,
            after_action_quantity,
            remark: remark,
            created_time: new Date(),
            updated_time: new Date()
          },
          { transaction }
        );
        sync_list.push({
          product_ID: detail.product_id,
          qty: detail.amount
        });
        detail.amount = after_action_quantity;
      }
      let products_update = {
        products_update: sync_list,
        admin_id: String(admin_info.id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      return res.status(200).json({ product_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "removeQuantityList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  //Update function is not use. It's the same as addQuantity and removeQuantity
  update: async (req, res) => {
    console.log(`--> update product quantity : ProductQuantity <--`);
    const {
      id,
      idprestashop,
      pid,
      amount,
      location_id,
      location_name,
      link_product
    } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      // console.log(req.body);
      if (link_product === "pid") {
        const product = await Product.findAll({
          raw: true,
          attributes: ["idprestashop", "id"]
        });
        let count = 0;
        for (let item of product) {
          // console.log(item);
          const _update_quantity = await ProductQuantity.update(
            { pid: item.id, updated_time: new Date() },
            { where: { idprestashop: item.idprestashop }, transaction }
          );
          count++;
          // console.log(count);
        }
        return res.json({ data: product, count: count });
      } else {
      }

      // console.log(product);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  syncQuantityToPrestashop: async (req, res) => {
    console.log("===> Syncing product quantity to prestashop <===");
    try {
      const ws_product = await ProductQuantity.findAll({
        where: { location_id: 1 },
        raw: true,
        attributes: ["idprestashop", "amount"],
        order: [["id", "ASC"]]
      });
      let arr_product = [];
      const v = ws_product.map(x => {
        const data = {
          product_ID: x.idprestashop,
          qty: x.amount
        };
        arr_product.push(data);
      });
      const update_data = {
        products_update: arr_product,
        admin_id: String(31),
        token: ""
      };

      const sync_result = await HttpService.request(
        update_data,
        config.wbUrl + "",
        "POST"
      );

      const total_changed_products = sync_result.total_change;
      const total_unchanged_products = sync_result.total_unchange;
      const total_error = sync_result.total_error;
      console.log("Total touched Products is: " + total_changed_products);
      console.log("Total untouched product is: " + total_unchanged_products);
      console.log("Total errors is: " + total_error);

      return res.status(200).json(sync_result);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "syncQuantityToPrestashop",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },

  resetAllQuantityAtLocation: async (req, res) => {
    //this is provided for a case like adjust stock for a whole warehouse
    let transaction = await model.sequelize.transaction();
    try {
      let { location_id } = req.params;
      await ProductQuantity.update(
        { amount: 0, updated_time: new Date() },
        { where: { location_id }, transaction }
      );
      await transaction.commit();
      return res.status(200).json(true);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "resetAllQuantityAtLocation",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  resetAllQuantity: async (req, res) => {
    //reset all quantity at all location
    //DO NOT USE THIS IF U DON'T KNOW WHAT U R DOING!!!
    let transaction = await model.sequelize.transaction();
    try {
      await ProductQuantity.update(
        { amount: 0, updated_time: new Date() },
        { where: {}, transaction }
      );
      await transaction.commit();
      return res.status(200).json(true);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "resetAllQuantity",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  adjustQuantity: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { product_id, location_id } = req.params;
      let { amount, remark, admin_info, action_info } = req.body;
      const productResult = await Product.findOne({
        where: { product_prestashop_id: product_id }
      });
      if (!productResult) throw { message: "Product does not exist." };
      const locationResult = await BranchLocation.findOne({
        where: { id: location_id }
      });
      if (!locationResult) throw { message: "Location does not exist." };
      const productQty = await ProductQuantity.findOne({
        where: { product_id, location_id }
      });
      if (!productQty)
        throw { message: "Product does not regist to this location." };

      let instock_quantity = productQty.amount;
      let action_quantity = Math.abs(instock_quantity - amount);
      let after_action_quantity = amount;
      if (instock_quantity == after_action_quantity)
        return res
          .status(200)
          .json({ product: productResult, amount: after_action_quantity });

      await ProductQuantity.update(
        { amount: after_action_quantity, updated_time: new Date() },
        { where: { product_id, location_id }, transaction }
      );
      await Movement.create(
        {
          user_id: admin_info.id,
          user_name: admin_info.name,
          product_id: product_id,
          pid: productResult.id,
          product_name: productResult.product_name,
          product_reference: productResult.reference,
          product_barcode: productResult.barcode,
          action_id:
            action_quantity > instock_quantity
              ? action_info[0].id
              : action_info[1].id,
          action_name:
            action_quantity > instock_quantity
              ? action_info[0].action_name
              : action_info[1].action_name,
          action_type:
            action_quantity > instock_quantity
              ? action_info[0].action_type
              : action_info[1].action_type,
          location_id: location_id,
          location_name: locationResult.location_name,
          instock_quantity,
          action_quantity,
          after_action_quantity,
          remark: remark
        },
        { transaction }
      );
      let products_update = {
        products_update: [
          {
            product_ID: product_id,
            qty: after_action_quantity > 0 ? after_action_quantity : 0
          }
        ],
        admin_id: String(admin_info.id),
        token: ""
      };
      await HttpService.request(
        products_update,
        config.wbUrl + "",
        "POST"
      );
      await transaction.commit();
      return res
        .status(200)
        .json({ product: productResult, amount: after_action_quantity });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductQuantityController",
        action_name: "adjustQuantity",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
};
