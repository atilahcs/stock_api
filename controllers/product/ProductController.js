const sequelize = require("sequelize"); // for op
const {
  Product,
  ProductQuantity,
  SupplierProduct,
  Supplier,
  ErrorLog
} = require("./../../models");
const ProductLocationController = require("./ProductLocationController");
const ProductPurchaseController = require("./ProductPurchaseController");
const ProductQuantityController = require("./ProductQuantityController");
const ProductReceiveController = require("./ProductReceiveController");
const ProductSupplierController = require("./ProductSupplierController");

const paginationHelper = require("../../helpers/paginationHelper");
const filterHelper = require("../../helpers/filterHelper"); // for search

const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const model = require("../../models");

module.exports = {
  search: async (req, res) => {
    const { page = 0, pageSize = 100, filter, supplier_filter } = req.body;
    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageSize)
    );

    let dataArray = {
      page: parseInt(page),
      totalPage: 50,
      totalData: 0,
      data: []
    };

    try {
      const filter_fields = await filterHelper(filter);
      const supplier_field = await filterHelper(supplier_filter);

      let obj_search = {
        // raw: true,
        distinct: true,
        required: true,
        subQuery: false, // เพิมมาเพราะว่า where ใน include หาไม่เจอ
        offset: pageNumber,
        limit: parseInt(pageSize),
        order: [["id", "DESC"]]
      };

      if (supplier_filter !== "") {
        obj_search.include = [
          {
            model: Supplier,
            attributes: ["supplier_name"],
            subQuery: false,
            where: supplier_field
          },
          {
            model: ProductQuantity
          }
        ];
      }
      if (filter !== "") {
        obj_search.include = [
          {
            model: ProductQuantity,
            attributes: ["amount"],
            where: { location_id: 1 }
          }
        ];
        obj_search.where = filter_fields;
      }

      const ws_product = await Product.findAndCountAll(obj_search);
      dataArray.totalPage = Math.ceil(ws_product.count / pageSize);
      dataArray.data = ws_product.rows;
      dataArray.totalData = ws_product.count;

      return res.status(200).json(dataArray);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "search",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  // get information on top of product
  getDashboard: async (req, res) => {
    try {
      const ws_product_count = await Product.findAndCountAll();
      const Op = sequelize.Op;
      const ws_product_negative = await Product.findAndCountAll({
        include: [
          {
            model: ProductQuantity,
            where: { location_id: 1, amount: { [Op.lt]: 0 } }
          }
        ]
      });
      const ws_product_barcode = await Product.findAndCountAll({
        where: { barcode: { [Op.eq]: "" } }
      });
      const ws_product_without = await Product.findAndCountAll({
        include: [
          {
            model: Supplier,
            where: { supplier_name: { [Op.eq]: null } },
            attributes: ["supplier_name"],
            subQuery: false,
            required: true
          }
        ]
      });
      const dataArray = {
        page: 0,
        count: ws_product_count.count,
        negative: ws_product_negative.count,
        negative_list: ws_product_negative.rows,
        missing_barcode: ws_product_barcode.count,
        without_supplier: ws_product_without.count
      };
      // console.log(dataArray);
      return res.status(200).json(dataArray);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "getDashboard",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getDetail: async (req, res) => {
    let { product_id } = req.params;
    try {
      const product_result = await Product.findOne({
        where: { id: product_id },
        include: [{ model: ProductQuantity }]
      });
      return res.status(200).json({ product: product_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getDetailByBarcode: async (req, res) => {
    let { product_barcode } = req.params;
    try {
      const product_result = await Product.findOne({
        where: { barcode: product_barcode },
        include: [{ model: ProductQuantity }]
      });
      return res.status(200).json({ product: product_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "getDetailByBarcode",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    const {
      page = 0,
      pageLimit = 50,
      offset = 0,
      column = "product_prestashop_id",
      ordering = "ASC"
    } = req.query;
    let { filter = "" } = req.query;

    const pageNumber = await paginationHelper.pageNumber(
      parseInt(page),
      parseInt(pageLimit)
    );

    let filter_clause = await filterHelper(filter);

    try {
      const product = await Product.findAndCountAll({
        include: [
          {
            model: ProductQuantity,
            where: { location_id: 1 }
          }
        ],
        // required: true,
        // subQuery: false,
        where: filter_clause,
        offset: pageNumber,
        limit: pageLimit,
        order: [[column, ordering]]
      });
      const dataArray = {
        totalPage: Math.ceil(product.count / pageLimit),
        count: product.count,
        data: product.rows
      };
      return res.status(200).json({ message: "Product is found", dataArray });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    try {
      //we don't have crate product in ws yet. I will implement it late.
      // So, this is not CRUD. but it's RUD.
    } catch (err) {
      console.error("=============");
      console.error("Controller: product/ProductController");
      console.error("Action: getList");
      console.error("Params (query):", req.query);
      console.error("Path:", req.originalUrl);
      console.error("Method:", req.method);
      console.error("Error:", err);
      console.error("=============");
      return res.status(406).json(err);
    }
  },
  update: async (req, res) => {
    console.log(`update product`);
    const {
      id,
      name,
      idprestashop,
      reference,
      status,
      barcode,
      product_img,
      shelf_location,
      product_type,
      base_price,
      bar_price,
      wholesale_price,
      purchasing_price,
      default_supplier_id,
      default_supplier_name,

      final_price
    } = req.body;
    console.log(req.body);
    const updated_time = new Date();
    let transaction = await model.sequelize.transaction();
    try {
      if (!barcode) {
        const _ws_product = await Product.update(
          {
            name,
            idprestashop,
            reference,
            status,
            barcode,
            product_img,
            shelf_location,
            product_type,
            base_price,
            bar_price,
            wholesale_price,
            purchasing_price,
            default_supplier_id,
            default_supplier_name,
            final_price,
            updated_time
          },
          { where: { id: id }, transaction }
        );
        await transaction.commit();
        return res.status(200).json({ data: { Product: _ws_product } });
      } else {
        console.log(` Barcode -> update to prestashop `);
        const find_barcode = await Product.findOne({
          where: { barcode }
        });
        console.log(find_barcode);

        if (find_barcode) {
          return res.status(406).json(`Barcode already exist`);
        } else {
          const update_product = await Product.update(
            {
              barcode
            },
            { where: { id: id }, transaction }
          );
          console.log(update_product);
          const data = {
            product_barcodeUpdate: {
              product_id: idprestashop,
              product_barcode: barcode
            },
            token: ""
          };

          console.log(data);
          const update_barcode_ps = await HttpService.request(
            data,
            config.wbUrl + "",
            "POST"
          );
          console.log(update_barcode_ps);
          if (update_barcode_ps.status === "ok") {
            console.log("Successfully, add product barcode in prestashop.");
            await transaction.commit();
            return res.status(200).json({
              status: "success"
            });
          } else {
            if (transaction.finished != 'commit')
              await transaction.rollback();
            return res.status(406).json(`Error`);
          }
        }
      }
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "update",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {
    const { id } = req.query;
    let transaction = await model.sequelize.transaction();
    try {
      const product = await Product.destroy({ where: { id }, transaction });
      const quantity = await ProductQuantity.destroy({
        where: { product_id: id },
        transaction
      });
      await transaction.commit();
      return res
        .status(200)
        .json({ message: "Success deleted product_id: " + product_id });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "delete",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  getRemovedFromPs: async (req, res) => {
    // Note : Only check and display a detail.
    console.log("=> Check removed product from prestashop");
    try {
      const product = await Product.findAll({
        raw: true,
        attributes: ["product_prestashop_id"]
      });
      let product_arr = [];
      const product_to_ps = product.map(x => {
        const data = {
          idprestashop: x.product_prestashop_id
        };
        return product_arr.push(data);
      });
      const data = {
        comparePrestashopidList: product_arr,
        token: ""
      };
      const request_to_ps = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );
      let products_list = [];
      if (request_to_ps.removed_products.length > 0) {
        for (let removed_item of request_to_ps.removed_products) {
          const product = await Product.findOne({
            where: { product_prestashop_id: removed_item.idprestashop }
          });
          products_list.push(product);
        }
      } else {
        return res
          .status(200)
          .json({ message: `There is no removed product in prestashop. ` });
      }

      return res.status(200).json(products_list);
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "getRemovedFromPs",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  updateProductBarcode: async (req, res) => {
    let { product_id } = req.params;
    let { barcode } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      const check_duplicate_barcode = await Product.findOne({
        where: { barcode }
      });

      if (check_duplicate_barcode && check_duplicate_barcode.id != product_id)
        throw { message: "This barcode is already in used." };
      const update_product = await Product.update(
        { barcode },
        { where: { id: Number(product_id) }, transaction }
      );

      await transaction.commit();
      const product_result = await Product.findOne({
        where: { id: product_id }
      });

      const data = {
        product_barcodeUpdate: {
          product_id: product_id,
          product_barcode: barcode
        },
        token: ""
      };
      const barcode_to_ps = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );

      console.log(barcode_to_ps);
      return res.status(200).json({ product: product_result });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      await ErrorLog.create({
        controller_name: "product/ProductController",
        action_name: "updateProductBarcode",
        path: req.originalUrl,
        method: req.method,
        message: message,
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  location: ProductLocationController,
  po: ProductPurchaseController,
  quantity: ProductQuantityController,
  ro: ProductReceiveController,
  productsupplier: ProductSupplierController
};
