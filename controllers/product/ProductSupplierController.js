const { Product, ProductQuantity, ErrorLog } = require("./../../models");

const HttpService = require("../../services/HttpService");
const sendMailService = require("../../services/SendMailService");

var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const model = require('../../models');

module.exports = {
  getProductSupplierFromPs: async (req, res) => {
    console.log(`check product supplier from ps`);
    try {
      let { product_id } = req.query;
      let data;
      if (product_id) {
        data = {
          getProductSupplier: { product_id: product_id },
          token: ""
        };
      } else {
        data = {
          getProductSupplier: "",
          token: ""
        };
      }
      const product_supplier = await HttpService.request(
        data,
        config.wbUrl + "",
        "POST"
      );
      return res.status(200).json({ product: product_supplier });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'product/ProductSupplierController',
        action_name: 'getProductSupplierFromPs',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
};
