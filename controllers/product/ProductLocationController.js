const { Product, ProductQuantity, BranchLocation, Movement, ErrorLog } = require('./../../models');
const filterHelper = require('../../helpers/filterHelper');
const model = require('../../models');
module.exports = {
  getLocation: async (req, res) => {
    let { product_id, location_id } = req.params;
    try {
      const productResult = await Product.findOne({
        where: { product_prestashop_id: product_id },
        include: [{
          model: BranchLocation, where: { id: location_id } }]});
      if (!productResult)
        throw { message: 'Product does not regist in this location.' };

      return res.status(200).json({ product: productResult });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'getLocation',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getLocationList: async (req, res) => {
    let { product_id } = req.params;
    try {
      const productResult = await Product.findOne({ where: { product_prestashop_id: product_id }, include: [{ model: BranchLocation }]});
      if (!productResult)
        throw { message: 'Product not found' };

      let total = 0;
      for (let qty of productResult.BranchLocations) {
        total += qty.ProductQuantity.amount;
      }
      let countLocation = productResult.BranchLocations.length;
      return res.status(200).json({ product: productResult, count: countLocation, total });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'getLocationList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getLocationMovement: async (req, res) => {
    let { product_id, location_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    if (page || page > 1) offset = limit * (page - 1);
    try {
      const productResult = await Product.findOne({ where: { product_prestashop_id: product_id }});
      if (!productResult)
        throw { message: 'Product not found' };

      filter += `product_id:${product_id};location_id:${location_id};`
      let filter_clause = await filterHelper(filter);
      const count = await Movement.count({ where: filter_clause });
      const movement_list = await Movement.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, movement_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'getLocationMovement',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  addProductLocation: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { product_id, location_id } = req.params;
      const productResult = await Product.findOne({ where: { product_prestashop_id: product_id }});
      if (!productResult)
        throw { message: 'Product not found' };

      const locationResult = await BranchLocation.findOne({ where: { id: location_id }});
      if (!locationResult)
        throw { message: 'Location does not exist' };

      let location_name = locationResult.location_name;
      const checkProductLocation = await ProductQuantity.findOne({ where: { product_id, location_id }});
      if (checkProductLocation)
        throw { message: 'This product already regist to this location.' };
      const product_location = await ProductQuantity.create({ product_id, pid: productResult.id, location_id, location_name, amount: 0 }, { transaction });
      await transaction.commit();
      return res.status(200).json({ product_location });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'addProductLocation',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  removeProductLocation: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { product_id, location_id } = req.params;
      const productResult = await Product.findOne({ where: { product_prestashop_id: product_id }});
      if (!productResult)
        throw { message: 'Product not found' };

      const locationResult = await BranchLocation.findOne({ where: { id: location_id }});
      if (!locationResult)
        throw { message: 'Location does not exist' };

      const checkProductLocation = await ProductQuantity.findOne({ where: { product_id, location_id }});
      if (!checkProductLocation)
        throw { message: 'This product already regist to this location.' };
      if (checkProductLocation.amount > 0)
        throw { message: 'Remove quantity from location first before continue.' };
      await ProductQuantity.destroy({ where: { product_id, location_id }, transaction });
      // const productLocation = await ProductQuantity.create({ product_id, pid: productResult.id, location_id, location_name, amount: 0 });
      await transaction.commit();
      return res.status(200).json(true);
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'removeProductLocation',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
}