const { PurchaseOrderDetail, ErrorLog } = require('./../../models');
const filterHelper = require('../../helpers/filterHelper');
module.exports = {
  getPoList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.body;
    let { filter = '' } = req.body;
    let offset = 0;
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let { product_id } = req.params;
      filter += `product_id:${product_id};`;
      let filter_clause = await filterHelper(filter);
      const count = await PurchaseOrderDetail.count({ where: filter_clause });
      const purchase_order_list = await PurchaseOrderDetail.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, purchase_order_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'product/ProductLocationController',
        action_name: 'getPoList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}