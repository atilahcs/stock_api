const { ReceiveOrderDetail, ErrorLog } = require('./../../models');
const filterHelper = require('../../helpers/filterHelper');
module.exports = {
  getRoList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.body;
    let { filter = '' } = req.body;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let { product_id } = req.params;
      filter += `product_id:${product_id};`;
      let filter_clause = await filterHelper(filter);
      const count = await ReceiveOrderDetail.count({ where: filter_clause });
      const receive_order_list = await ReceiveOrderDetail.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, receive_order_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        param: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'product/ProductReceiveController',
        action_name: 'getRoList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
}