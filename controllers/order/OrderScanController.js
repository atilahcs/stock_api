const { OrderScan, OrderScanDetail, ErrorLog } = require("../../models");
const filterHelper = require('../../helpers/filterHelper');
module.exports = {
    getList: async (req, res) => {
        let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
        let { filter = '' } = req.query;
        let offset = 0;
        limit = Number(limit);
        if (page || page > 1) offset = limit * (page - 1);
        try {
            let filter_clause = await filterHelper(filter);
            const count = await OrderScan.count({ where: filter_clause });
            let order_scan_list = await OrderScan.findOne({
                offset: offset,
                limit: (limit < 0) ? null : limit,
                order: [[column, ordering]],
                where: filter_clause
            });
            return res.status(200).json({ count, order_scan_list });
        } catch (err) {
            let status = (err && err.status) ? err.status : 500;
            let message = (err && err.message) ? err.message : 'Something went wrong.';
            let param = {
                query: req.query
            }
            await ErrorLog.create({
                controller_name: 'order/OrderScanController',
                action_name: 'getList',
                path: req.originalUrl,
                method: req.method,
                message: message,
                params: JSON.stringify(param),
                created_time: new Date()
            });
            return res.status(status).json({ error: message });
        }
    },
    getDetail: async (req, res) => {
        const { order_ps_id } = req.params;
        try {
            const order_scan = await OrderScan.findOne({
                where: { order_prestashop_id: order_ps_id },
                include: [{ model: OrderScanDetail }]
            });
            if (!order_scan)
                throw { message: 'OrderScan not found.' }
            return res.status(200).json({ order_scan });
        } catch (err) {
            let status = (err && err.status) ? err.status : 500;
            let message = (err && err.message) ? err.message : 'Something went wrong.';
            let param = {
                params: req.params
            }
            await ErrorLog.create({
                controller_name: 'order/OrderScanController',
                action_name: 'getDetail',
                path: req.originalUrl,
                method: req.method,
                message: message,
                params: JSON.stringify(param),
                created_time: new Date()
            });
            return res.status(status).json({ error: message });
        }
    },
}