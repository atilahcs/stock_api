const {
  Order,
  OrderPrestashop,
  OrderPrestashopDetail,
  ProductQuantity,
  OrderScan,
  OrderScanDetail,
  OrderScanMovement,
  OrderScanVersion,
  OrderScanVersionDetail,
  Movement,
  Product,
  ErrorLog
} = require("../../models");
const model = require('../../models');
const filterHelper = require("../../helpers/filterHelper");
const HttpService = require("../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
const OrderScanController = require("./OrderScanController");
const OrderScanMovementController = require("./OrderScanMovementController");
const OrderScanVersionController = require("./OrderScanVersionController");
const MovementLogController = require("./MovementLogController");
const normalizePsOrderHelper = require('../../helpers/normailzePsOrderHelper');
module.exports = {
  getDetail: async (req, res) => {
    let { order_ps_id } = req.params;
    try {
      let order_prestashop = await OrderPrestashop.findOne({
        where: { order_prestashop_id: order_ps_id },
        include: [
          { model: OrderPrestashopDetail },
        ]
      });
      if (!order_prestashop)
        throw { message: 'Order not found.' };
      return res.json({ order_prestashop });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'getDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = '' } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await OrderPrestashop.count({ where: filter_clause });
      const order_prestashop_list = await OrderPrestashop.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, order_prestashop_list });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        query: req.query
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'getList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getPsOrderDetail: async (req, res) => {
    let { order_ps_id } = req.params;
    try {
      let request_data = {
        orderID: order_ps_id,
        token: ""
      };
      const ps_result = await HttpService.request(
        request_data,
        config.wbUrl + "",
        "post"
      );
      if (typeof ps_result == "string") throw { message: "Something went wrong" };
      if (ps_result.status != "ok") throw { message: ps_result.message };
      let order_info = {
        order_prestashop_id: order_ps_id,
        order_reference: ps_result.orderInfo[0].reference,
        order_type: ps_result.orderInfo[0].order_type ? ps_result.orderInfo[0].order_type : 'retail'
      };

      let customer_info = {
        firstname: ps_result.customerInfo[0].firstname,
        lastname: ps_result.customerInfo[0].lastname,
        email: ps_result.customerInfo[0].email,
        customer_id: ps_result.customerInfo[0].id_customer
      };
      let billing_address = {
        address: ps_result.billingAddress[0].address1,
        city: ps_result.billingAddress[0].city,
        postcode: ps_result.billingAddress[0].postcode,
        phone: ps_result.billingAddress[0].phone,
        phone2: ps_result.billingAddress[0].phone_mobile
      };
      let shipping_address = {
        address: ps_result.deliveryAddress[0].address1,
        city: ps_result.deliveryAddress[0].city,
        postcode: ps_result.deliveryAddress[0].postcode,
        phone: ps_result.deliveryAddress[0].phone,
        phone2: ps_result.deliveryAddress[0].phone_mobile
      };
      let normalizes_order_detail = await normalizePsOrderHelper(
        ps_result.orderDetail
      );
      for (let detail of normalizes_order_detail) {
        let product = await Product.findOne({ where: { product_prestashop_id: detail.product_id }});
        detail.pid = product.id;
        if (!detail.products_in_set)
          continue;
        for (let product_in_set of detail.products_in_set) {
          let product_is = await Product.findOne({ where: { product_prestashop_id: detail.product_id }});
          product_in_set.pid = product_is.id;
        }
      }
      return res
        .status(200)
        .json({
          order_info: order_info,
          customer_info: customer_info,
          billing_address: billing_address,
          shipping_address: shipping_address,
          order_detail: normalizes_order_detail
        });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'getPsOrderDetail',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let { order_info, order_detail, admin_info } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      let order_prestashop_id = order_info.order_prestashop_id;
      const search_order = await OrderPrestashop.findOne({ where: { order_prestashop_id: order_prestashop_id } });
      if (search_order)
        throw { message: 'This order is already scanned' };

      let order_prestashop = {
        order_prestashop_id: order_info.order_prestashop_id,
        version_number: 1,
        customer_id: order_info.customer_id,
        customer_firstname: order_info.customer_firstname,
        customer_lastname: order_info.customer_lastname,
        customer_email: order_info.customer_email,
        billing_address: order_info.billing_address,
        shipping_address: order_info.shipping_address,
        status: order_info.status,
        total_price: order_info.total_price,
        order_type: order_info.order_type,
        oid: null,
        osid: null,
        osvid: null,
        scan_by_id: admin_info.id,
        scan_by_name: admin_info.name,
        order_status: order_info.status,
        created_time: new Date(),
        updated_time: new Date()
      };

      const order_prestashop_result = await OrderPrestashop.create(order_prestashop, { transaction });
      order_prestashop.oid = order_prestashop_result.id;

      let order_prestashop_detail = order_detail.map((detail) => {
        let result = {
          oid: order_prestashop.oid,
          order_prestashop_id: order_prestashop_id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_reference: detail.product_reference,
          product_barcode: detail.product_barcode,
          product_unit_price: detail.product_unit_price,
          product_total_price: detail.product_total_price,
          product_quantity: detail.product_quantity,
          product_in_set_quantity: detail.product_in_set_quantity,
          product_image: detail.product_image,
          product_quantity_instock: detail.product_quantity_instock,
          product_parent_id: detail.product_parent_id,
          product_parent_name: detail.product_parent_name,
          is_set: detail.is_set,
          in_box_quantity: 0,
          scan_quantity: detail.scan_quantity,
          scan_result: detail.scan_result,
          scan_by_id: (detail.scan_quantity) ? admin_info.id : null,
          scan_by_name: (detail.scan_quantity) ? admin_info.name : null,
          user_id: Number(admin_info.id),
          user_name: admin_info.name,
          reference_id: `${order_prestashop_id}`,
          order_prestashop_id: order_prestashop_id,
          version_number: 1,
          odid: null,
          product_quantity_instock: 0,
          instock_quantity: 0,
          action_quantity: detail.scan_quantity,
          after_action_quantity: 0,
          osid: null,
          osdid: null,
          osvid: null,
          osvdid: null,
          location_id: detail.location_id,
          location_name: detail.location_name,
          action_id: detail.action_id,
          action_type: detail.action_type,
          action_name: detail.action_name,
          movement_id: null,
          created_time: new Date(),
          updated_time: new Date()
        }
        return result;
      });

      let sync_product_list = [];

      for (let detail of order_prestashop_detail) {
        detail.oid = order_prestashop.oid;
        const product_result = await Product.findOne({ where: { product_prestashop_id: detail.product_id } });
        detail.pid = product_result.id;
        const order_prestashop_detail_result = await OrderPrestashopDetail.create(detail, { transaction });
        detail.odid = order_prestashop_detail_result.id;
        if (detail.action_quantity <= 0)
          continue;
        const product_quantity_result = await ProductQuantity.findOne({ where: { product_id: detail.product_id, location_id: detail.location_id } });
        if (!product_result)
          continue;
        detail.product_quantity_instock = product_quantity_result.amount;
        detail.instock_quantity = product_quantity_result.amount;
        let product_amount = (detail.action_type == 'decrease') ? detail.instock_quantity - detail.action_quantity : detail.instock_quantity + detail.action_quantity;
        detail.after_action_quantity = product_amount;
        const update_quantity_result = await ProductQuantity.update({ amount: product_amount }, { where: { product_id: detail.product_id }, transaction });
        if (!update_quantity_result)
          continue;
        let sync_quantity = {
          product_ID: detail.product_id,
          qty: product_amount
        }
        sync_product_list.push(sync_quantity);
        const movement_result = await Movement.create(detail, { transaction });
        detail.movement_id = movement_result.id;
        await OrderPrestashopDetail.update({ product_quantity_instock: detail.product_quantity_instock }, { where: { id: detail.odid }, transaction });
      }

      const order_scan_result = await OrderScan.create(order_prestashop, { transaction });
      order_prestashop.osid = order_scan_result.id;
      for (let detail of order_prestashop_detail) {
        detail.osid = order_prestashop.osid;
        const order_scan_detail_result = await OrderScanDetail.create(detail, { transaction });
        detail.osdid = order_scan_detail_result.id;
      }

      const order_scan_version_result = await OrderScanVersion.create(order_prestashop, { transaction });
      for (let detail of order_prestashop_detail) {
        detail.osvid = order_scan_version_result.id;
        const order_scan_version_detail_result = await OrderScanVersionDetail.create(detail, { transaction });
        detail.osvdid = order_scan_version_detail_result.id;
        if (detail.action_quantity <= 0)
          continue;
        await OrderScanMovement.create(detail, { transaction });
      }

      await transaction.commit();
      let products_update = {
        products_update: sync_product_list,
        admin_id: String(admin_info.id),
        token: ''
      };
      await HttpService.request(products_update, config.wbUrl + '', 'POST');

      const order_result = await OrderPrestashop.findOne({ where: { order_prestashop_id }, include: [{ model: OrderPrestashopDetail }] });
      return res.status(200).json({
        order: order_result
      });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { order_ps_id } = req.params;
    let { order_info, order_detail, admin_info } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      const search_order = await OrderPrestashop.findOne({ where: { order_prestashop_id: order_ps_id }});
      if (!search_order)
        throw { message: 'Order does not exist.' };
      if (order_info.version_number <= search_order.version_number)
        throw { message: 'This version of order is already updated.' };

      let order_prestashop = {
        order_prestashop_id: order_info.order_prestashop_id,
        version_number: order_info.version_number,
        status: order_info.status,
        total_price: order_info.total_price,
        order_type: order_info.order_type,
        oid: search_order.id,
        osid: null,
        osvid: null,
        scan_by_id: admin_info.id,
        scan_by_name: admin_info.name,
        order_status: order_info.status,
        updated_time: new Date()
      };
      await OrderPrestashop.update(order_prestashop, { where: { order_prestashop_id: order_ps_id }, transaction });
      await OrderScan.update(order_prestashop, { where: { order_prestashop_id: order_ps_id }, transaction });
      const order_scan = await OrderScan.findOne({ where: { order_prestashop_id: order_ps_id }});
      order_prestashop.osid = order_scan.id;
      const order_scan_version = await OrderScanVersion.create(order_prestashop, { transaction });
      order_prestashop.osvid = order_scan_version.id;

      let order_prestashop_detail = order_detail.map((detail) => {
        let result = {
          oid: order_prestashop.oid,
          order_prestashop_id: order_info.order_prestashop_id,
          product_id: detail.product_id,
          pid: detail.pid,
          product_name: detail.product_name,
          product_reference: detail.product_reference,
          product_barcode: detail.product_barcode,
          product_unit_price: detail.product_unit_price,
          product_total_price: detail.product_total_price,
          product_quantity: detail.product_quantity,
          product_in_set_quantity: detail.product_in_set_quantity,
          product_image: detail.product_image,
          product_quantity_instock: detail.product_quantity_instock,
          product_parent_id: detail.product_parent_id,
          product_parent_name: detail.product_parent_name,
          is_set: detail.is_set,
          in_box_quantity: detail.in_box_quantity,
          scan_quantity: detail.scan_quantity,
          scan_result: detail.scan_result,
          scan_by_id: admin_info.id,
          scan_by_name: admin_info.name,
          user_id: admin_info.id,
          user_name: admin_info.name,
          reference_id: order_info.order_prestashop_id,
          order_prestashop_id: order_info.order_prestashop_id,
          version_number: order_prestashop.version_number,
          odid: detail.id,
          product_quantity_instock: 0,
          instock_quantity: 0,
          action_quantity: detail.action_quantity,
          after_action_quantity: 0,
          osid: order_prestashop.osid,
          osdid: null,
          osvid: order_prestashop.osvid,
          osvdid: null,
          location_id: detail.location_id,
          action_id: detail.action_id,
          action_type: detail.action_type,
          action_name: detail.action_name,
          movement_id: null,
          updated_time: null,
          deleted: detail.deleted
        }
        if (detail.action_quantity) {
          detail.scan_by_id = admin_info.id;
          detail.scan_by_name = admin_info.name;
        }
        return result;
      });

      let sync_product_list = [];
      for (let detail of order_prestashop_detail) {
        if (!detail.odid) {
          detail.created_time = new Date();
          detail.updated_time = new Date();
          let order_prestashop_detail_result = await OrderPrestashopDetail.create(detail, { transaction });
          detail.odid = order_prestashop_detail_result.id;
        } else if (detail.deleted && (detail.in_box_quantity == 0 || detail.in_box_quantity == detail.action_quantity)) {
          await OrderPrestashopDetail.destroy({ where: { id: detail.odid }, transaction });
        } else {
          detail.updated_time = new Date();
          await OrderPrestashopDetail.update(detail, { where: { id: detail.odid }, transaction });
        }
        if (detail.action_quantity <= 0)
          continue;
        const product_quantity_result = await ProductQuantity.findOne({ where: { product_id: detail.product_id, location_id: detail.location_id } });
        if (!product_quantity_result)
          continue;
        detail.product_quantity_instock = product_quantity_result.amount;
        detail.instock_quantity = product_quantity_result.amount;
        let product_amount = (detail.action_type == 'decrease') ? detail.instock_quantity - detail.action_quantity : detail.instock_quantity + detail.action_quantity;
        detail.after_action_quantity = product_amount;
        const update_quantity_result = await ProductQuantity.update({ amount: product_amount, updated_time: new Date() }, { where: { product_id: detail.product_id }, transaction });
        if (!update_quantity_result)
          continue;
        let sync_quantity = {
          product_ID: detail.product_id,
          qty: product_amount
        }
        sync_product_list.push(sync_quantity);
        detail.created_time = new Date();
        detail.updated_time = new Date();
        const movement_result = await Movement.create(detail, { transaction });
        detail.movement_id = movement_result.id;
        await OrderPrestashopDetail.update({ product_quantity_instock: detail.product_quantity_instock, updated_time: new Date() }, { where: { id: detail.odid }, transaction });
      }

      for (let detail of order_prestashop_detail) {
        const search_order_scan_result = await OrderScanDetail.findOne({ where: { odid: detail.odid }});
        if (search_order_scan_result)
          detail.osdid = search_order_scan_result.id;
        
        if (!detail.osdid) {
          detail.created_time = new Date();
          detail.updated_time = new Date();
          const order_scan_detail = await OrderScanDetail.create(detail, { transaction });
          detail.osdid = order_scan_detail.id;
        } else if (detail.deleted && (detail.in_box_quantity == 0 || detail.in_box_quantity == detail.action_quantity)) {
          await OrderScanDetail.destroy({ where: { id: detail.osdid }, transaction });
        } else {
          detail.updated_time = new Date();
          await OrderScanDetail.update(detail, { where: { id: detail.osdid }, transaction });
        }

        if (!detail.deleted || !(detail.in_box_quantity == 0 || detail.in_box_quantity == detail.action_quantity)) {
          detail.created_time = new Date();
          detail.updated_time = new Date();
          const order_scan_detail = await OrderScanVersionDetail.create(detail, { transaction });
          detail.osvdid = order_scan_detail.id;
        }

        if (detail.action_quantity <= 0)
          continue;
        detail.created_time = new Date();
        detail.updated_time = new Date();
        await OrderScanMovement.create(detail, { transaction });
      }

      await transaction.commit();
      let products_update = {
        products_update: sync_product_list,
        admin_id: String(admin_info.id),
        token: ''
      };
      await HttpService.request(products_update, config.wbUrl + '', 'POST');

      const order_result = await OrderPrestashop.findOne({ where: { order_prestashop_id: order_ps_id }, include: [{ model: OrderPrestashopDetail }] });
      return res.status(200).json({
        order_prestashop: order_result
      });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  checkCreated: async (req, res) => {
    let { order_ps_id } = req.params;
    try {
      const order_old_system = await Order.findOne({ where: { order_id: order_ps_id }});
      if (order_old_system)
        throw { message: 'This order is already scanned by old system.' };
      const order_new_system = await OrderPrestashop.findOne({ where: { order_prestashop_id: order_ps_id }});
      if (order_new_system)
        throw { message: 'This order is already scanned.' };
      return res.status(200).json({ created: false });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'order/OrderController',
        action_name: 'checkCreated',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  order_scan: OrderScanController,
  order_scan_movement: OrderScanMovementController,
  order_scan_version: OrderScanVersionController,
  movement: MovementLogController
};
