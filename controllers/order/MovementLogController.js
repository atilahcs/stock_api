const { OrderPrestashop, Movement, ErrorLog } = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
module.exports = {
  getListByOrderId: async (req, res) => {
    const { order_ps_id } = req.params;
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.body;
    let { filter = "" } = req.body;
    let offset = 0;
    if (page || page > 1) offset = limit * (page - 1);
    try {
      const order_prestashop = await OrderPrestashop.findOne({ where: { order_prestashop_id: order_ps_id }});
      filter += `oid:${order_prestashop.id};`;
      let filter_clause = await filterHelper(filter);
      const count = await Movement.count({ where: filter_clause });
      // const kia = "limit"
      let movement_list = await Movement.findAll({
        offset: offset,
        limit: (limit < 0) ? null : limit,
        // [kia]: limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.json({ count, movement_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "order/MovementLogController",
        action_name: "getListByOrderId",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  }
};
