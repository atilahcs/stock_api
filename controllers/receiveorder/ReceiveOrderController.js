const {
  ReceiveOrder,
  ReceiveOrderDetail,
  PurchaseOrder,
  PurchaseOrderDetail,
  ProductQuantity,
  Movement,
  ErrorLog
} = require("./../../models");
const HttpService = require("./../../services/HttpService");
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];

const ReceiveOrderFileController = require("./ReceiveOrderFileController");
const model = require("../../models");
const filterHelper = require("../../helpers/filterHelper");
module.exports = {
  getDetail: async (req, res) => {
    let { ro_id } = req.params;
    try {
      let receive_order = await ReceiveOrder.findOne({
        where: { id: ro_id },
        include: [{ model: ReceiveOrderDetail }]
      });

      if (!receive_order) throw { message: "Receive Order not found." };

      return res.status(200).json({ receive_order });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params
      };
      await ErrorLog.create({
        controller_name: "receiveorder/ReceiveOrderController",
        action_name: "getDetail",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getList: async (req, res) => {
    let { page, column = "id", ordering = "DESC", limit = 50 } = req.query;
    let { filter = "" } = req.query;
    let offset = 0;
    limit = Number(limit);
    if (page || page > 1) offset = limit * (page - 1);
    try {
      let filter_clause = await filterHelper(filter);
      const count = await ReceiveOrder.count({ where: filter_clause });
      const receive_order_list = await ReceiveOrder.findAll({
        offset: offset,
        limit: limit < 0 ? null : limit,
        order: [[column, ordering]],
        where: filter_clause
      });
      return res.status(200).json({ count, receive_order_list });
    } catch (err) {
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        query: req.query
      };
      await ErrorLog.create({
        controller_name: "receiveorder/ReceiveOrderController",
        action_name: "getList",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  create: async (req, res) => {
    let { receive_order, receive_list, action_info } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      let ro_status = "Complete";
      let po_status = "Received";
      let po_order_status = true;

      let ro = {
        po_id: receive_order.po_id,
        receive_status: ro_status,
        reference_invoice: receive_order.reference_invoice,
        receive_remark: receive_order.receive_remark,
        scan_by_id: receive_order.scan_by_id,
        scan_by_name: receive_order.scan_by_name,
        supplier_name: receive_order.supplier_name,
        supplier_id: receive_order.supplier_id,
        sid: receive_order.sid,
        due_date: receive_order.due_date || new Date(),
        order_date: receive_order.order_date || null,
        received_date: receive_order.receive_date || new Date(),
        expected_delivery_date: receive_order.expect_delivery_date || null,
        total_tax_excl: receive_order.total_tax_excl,
        total_tax: receive_order.total_tax,
        total_tax_inc: receive_order.total_tax_inc,
        created_time: new Date(),
        updated_time: new Date()
      };

      // create ro
      const ro_result = await ReceiveOrder.create(ro, { transaction });
      let sync_products = {
        products_update: [],
        admin_id: String(receive_order.scan_by_id),
        token: ""
      };
      for (let product of receive_list) {
        const product_to_create = {
          po_id: ro_result.po_id,
          po_reference: ro_result.po_reference,
          ro_id: ro_result.id,
          order_date: ro_result.order_date,
          expected_delivery_date: ro_result.expected_delivery_date,
          received_date: ro_result.received_date,
          reference_invoice: ro_result.reference_invoice,
          product_id: product.product_id,
          pid: product.pid,
          product_name: product.product_name,
          order_quantity: product.product_unit,
          scan_quantity: product.scan_quantity,
          product_unit_price: product.product_unit_price,
          product_unit_discount: product.product_unit_discount,
          product_amount: product.product_amount,
          product_amount_after_discount: product.product_amount_after_discount,
          created_time: new Date(),
          updated_time: new Date()
        };
        let qty_in = product.received_quantity + product.scan_quantity;

        //ro list
        await ReceiveOrderDetail.create(product_to_create, { transaction });

        //update po list, received quantity
        await PurchaseOrderDetail.update(
          { received_quantity: qty_in, updated_time: new Date() },
          {
            where: { po_id: ro_result.po_id, product_id: product.product_id },
            transaction
          }
        );

        //retrieve product quantity
        const product_result = await ProductQuantity.findOne({
          where: { product_id: product.product_id, location_id: 1 }
        });
        let quantity = product_result.amount + product.scan_quantity;

        //update product quantity
        await ProductQuantity.update(
          { amount: quantity, updated_time: new Date() },
          {
            where: { product_id: product.product_id, location_id: 1 },
            transaction
          }
        );

        //update log
        if (product.scan_quantity > 0) {
          let log_info = {
            user_id: receive_order.scan_by_id,
            user_name: receive_order.scan_by_name,
            product_id: product.product_id,
            product_name: product.product_name,
            product_barcode: product.barcode,
            action_id: action_info.id,
            action_name: action_info.action_name,
            action_type: action_info.action_type,
            location_id: receive_order.location_id,
            location_name: receive_order.location_name,
            instock_quantity: product_result.amount,
            action_quantity: product.scan_quantity,
            after_action_quantity: quantity,
            reference_id: ro_result.reference_invoice,
            remark: "Receive Order - " + ro_result.reference_invoice,
            sync_quantity: quantity,
            created_time: new Date(),
            updated_time: new Date()
          };
          Movement.create(log_info, { transaction });
        }

        //push to array to sync
        let sync_quantity = {
          product_ID: product.product_id,
          qty: quantity
        };
        sync_products.products_update.push(sync_quantity);

        //check each products if po has complete scan
        if (qty_in < product.product_unit) po_order_status = false;
      }
      //update po status
      if (!po_order_status) po_status = "Partial " + po_status;

      await PurchaseOrder.update(
        { status: po_status },
        { where: { id: ro.po_id }, transaction }
      );

      await transaction.commit();

      // sync products
      const ps_result = HttpService.request(
        sync_products,
        config.wbUrl + "",
        "post"
      );

      const receive_order_result = await ReceiveOrder.findOne({
        where: { id: ro_result.id },
        include: [{ model: ReceiveOrderDetail }]
      });

      return res.status(200).json({ receive_order: receive_order_result });
    } catch (err) {
      if (transaction.finished != 'commit')
        await transaction.rollback();
      let status = err && err.status ? err.status : 500;
      let message = err && err.message ? err.message : "Something went wrong.";
      let param = {
        params: req.params,
        body: req.body
      };
      await ErrorLog.create({
        controller_name: "receiveorder/ReceiveOrderController",
        action_name: "create",
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {},
  delete: async (req, res) => {},
  file: ReceiveOrderFileController
};
