const { ReceiveOrder, ReceiveOrderFile, ErrorLog } = require('../../models');
const roFileHelper = require('../../helpers/roFileHelper');
const model = require('../../models');
module.exports = {
  uploadFile: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    try {
      let { ro_id } = req.params;
      let { admin_id, admin_name } = req.body;
      const ro_result = await ReceiveOrder.findOne({ where: { id: ro_id }});
      if (!ro_result)
        throw { message: 'Receive Order Not Found' };

      if (!req.file)
        throw { message: 'No file upload.' };
      let file_name = req.file.filename;
      await ReceiveOrder.update({ default_image: file_name, updated_time: new Date() }, { where: { id: ro_id }, transaction });
      await ReceiveOrderFile.create({
        ro_id: Number(ro_id),
        file_name: file_name,
        admin_id: Number(admin_id),
        admin_name: admin_name,
        created_time: new Date(),
        updated_time: new Date()
      });
      await roFileHelper.moveFileLocation(file_name);
      await roFileHelper.removeLocalFile(file_name);

      return res.status(200).json({ file_name: file_name });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body,
        file_name: req.file ? req.file.filename : null
      }
      await ErrorLog.create({
        controller_name: 'receiveorder/ReceiveOrderController',
        action_name: 'uploadFile',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
  getFileList: async (req, res) => {
    try {
      let { ro_id } = req.params;
      const receiver_order_result = await ReceiveOrder.findOne({ where: { id: ro_id }});
      if (!receiver_order_result)
        throw { message: 'Receive order not found' };
      
      const receiver_order_file_list = await ReceiveOrderFile.findAll({ where: { ro_id: ro_id }});
      return res.status(200).json(receiver_order_file_list);
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params
      }
      await ErrorLog.create({
        controller_name: 'receiveorder/ReceiveOrderFileController',
        action_name: 'getFileList',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      return res.status(status).json({ error: message });
    }
  },
}