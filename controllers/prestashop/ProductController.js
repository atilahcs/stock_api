const { Product, Supplier, SupplierProduct, ErrorLog } = require('../../models');
const model = require('../../models');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
module.exports = {
  create: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    let { product_prestashop_id, name, reference, barcode,
      product_type, purchasing_price, homebar_price,
      wholesale_price, base_price, final_price } = req.body;
    try {
      if (!product_prestashop_id || !name)
        throw { message: 'Required information is missing.' };

      await Product.create({
        product_prestashop_id,
        name,
        reference,
        barcode,
        product_type,
        purchasing_price,
        bar_price: homebar_price,
        wholesale_price,
        base_price,
        final_price,
        status: 1,
        created_time: new Date(),
        updated_time: new Date()
      }, { transaction });
      await transaction.commit();
      return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/ProductController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { product_id } = req.params;
    let transaction = await model.sequelize.transaction();
    let { name, reference, barcode,
      product_type, purchasing_price, homebar_price,
      wholesale_price, base_price, final_price } = req.body;
    try {
      if (!product_id || !name)
      throw { message: 'Required information is missing.' };

    await Product.update({
      name,
      reference,
      barcode,
      product_type,
      purchasing_price,
      bar_price: homebar_price,
      wholesale_price,
      base_price,
      final_price,
      updated_time: new Date()
    }, { where: { product_prestashop_id: product_id }, transaction });
    await transaction.commit();
    return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/ProductController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  updateSupplier: async (req, res) => {
    let transaction = await model.sequelize.transaction();
    let { product_id } = req.params;
    let { new_supplier_list, remove_supplier_list, default_supplier_id, default_supplier_name } = req.body;
    product_id = Number(product_id);
    try {
      if (!new_supplier_list || !remove_supplier_list)
        throw { message: 'Supplier list is missing.' };
      if (!default_supplier_id || !default_supplier_name)
        throw { message: 'Default supplier is missing is missing.' };
        
      const product = await Product.findOne({ where: { product_prestashop_id: product_id }});
      if (!product)
        throw { message: 'Product does not exist.' };

      await Promise.all(new_supplier_list.map(async (supplier_id) => {
        const supplier = await Supplier.findOne({ where: { supplier_id }});
        if (!supplier_id)
          return;

        await SupplierProduct.create({
          product_id,
          pid: product.id,
          supplier_id,
          sid: supplier.id
        }, { transaction });
      }));
      await Promise.all(remove_supplier_list.map(async (supplier_id) => {
        await SupplierProduct.destroy({
          where: { product_id, supplier_id },
          transaction
        });
      }));
      await Product.update({
        default_supplier_id,
        default_supplier_name
      }, { where: { product_prestashop_id: product_id }, transaction });
      await transaction.commit();
      return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/ProductController',
        action_name: 'updateSupplier',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  updateProductImage: async (req, res) => {
    let { product_id } = req.params;
    let transaction = await model.sequelize.transaction();
    let { img_url } = req.body;
    try {
      if (!product_id || !img_url)
      throw { message: 'Required information is missing.' };

    await Product.update({
      product_img: img_url,
      updated_time: new Date()
    }, { where: { product_prestashop_id: product_id }, transaction });
    await transaction.commit();
    return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/ProductController',
        action_name: 'updateProductImage',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  }
}