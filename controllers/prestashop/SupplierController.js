const { ErrorLog, Supplier } = require('../../models');
const model = require('../../models');
var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../../config/config.json")[env];
module.exports = {
  create: async (req, res) => {
    let { supplier_id, supplier_name, address, city, zipcode, country } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      await Supplier.create({
        supplier_name: supplier_name,
        address: address,
        city: city,
        zipcode: zipcode,
        country: country['1'],
        street_address: "",
        street_city: "",
        street_zipcode: "",
        street_country: "",
        sale_setting: "",
        purchase_setting: "",
        bill_duedate: "",
        invoice_duedate: "",
        email: "",
        tax_id: '',
        contact: "",
        contact_no: "",
        xero_supplier_id: "",
        prestashop_id: Number(supplier_id),
        created_time: new Date(),
        updated_time: new Date()
      }, { transaction });

      await transaction.commit();
      return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/SupplierController',
        action_name: 'create',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  update: async (req, res) => {
    let { supplier_id } = req.params;
    let { supplier_name, phone, address, city, zipcode, country } = req.body;
    let transaction = await model.sequelize.transaction();
    try {
      await Supplier.update({
        supplier_name: supplier_name,
        address: address,
        city: city,
        zipcode: zipcode,
        country: country,
        street_address: "",
        street_city: "",
        street_zipcode: "",
        street_country: "",
        sale_setting: "",
        purchase_setting: "",
        bill_duedate: "",
        invoice_duedate: "",
        email: "",
        tax_id: '',
        contact: "",
        contact_no: "",
        xero_supplier_id: "",
        updated_time: new Date()
      }, { where: { prestashop_id: Number(supplier_id) }, transaction });

      await transaction.commit();
      return res.status(200).json({ success: true });
    } catch (err) {
      let status = (err && err.status) ? err.status : 500;
      let message = (err && err.message) ? err.message : 'Something went wrong.';
      let param = {
        params: req.params,
        body: req.body
      }
      await ErrorLog.create({
        controller_name: 'prestashop/SupplierController',
        action_name: 'update',
        path: req.originalUrl,
        method: req.method,
        message: message,
        params: JSON.stringify(param),
        created_time: new Date()
      });
      if (transaction.finished != 'commit')
        await transaction.rollback();
      return res.status(status).json({ error: message });
    }
  },
  delete: async (req, res) => {

  }
}