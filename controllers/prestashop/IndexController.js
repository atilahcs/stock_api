const SupplierController = require('./SupplierController');
const ProductController = require('./ProductController');
module.exports = {
  supplier: SupplierController,
  product: ProductController
}