var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
module.exports = {
    removeLocalFile: (file_name) => {
        return new  Promise((resolve, reject)=>{
            let fs = require('fs');
            fs.unlink(__dirname + '/../assets/img/' + file_name, function(err) {
                resolve(true);
            });
        });
    },
    moveFileLocation: (file_name) => {
        return new Promise((resolve, reject) => {
            if (!file_name) {
                reject('There\'s no file to upload.');
            } else {
                let client = require('scp2');
                if (env == 'production') {
                    client.scp(__dirname + '/../assets/img/' + file_name, {
                        host: config.ro_scp_host,
                        username: config.ro_scp_username,
                        password: config.ro_scp_key,
                        path: '/var/www/html/assets/ro_history'
                    }, (err)=>{
                        if (err) {
                            reject({ message: 'Unable to upload file ' + file_name + ' to server.' });
                        } else {
                            resolve(true);
                        }
                    });
                } else {
                    resolve(true);
                }
            }
        });
    }
}