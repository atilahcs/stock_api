const sequelize = require("sequelize");
const Op = sequelize.Op;
module.exports = filtered => {
  return new Promise((resolve, reject) => {
    if (!filtered) resolve({});
    let and_clause = [];
    let fields = filtered.split(";");
    for (let field of fields) {
      let values = field.split(":");
      if (values.length > 1) {
        let val = values[1];
        let field;
        if (
          values.length > 2 &&
          [">", ">=", "<", "<=", "=", "<>", "like"].includes(values[2])
        ) {
          switch (values[2]) {
            case ">":
              field = {
                [Op.gt]: val
              };
              break;
            case ">=":
              field = {
                [Op.gte]: val
              };
              break;
            case "<":
              field = {
                [Op.lt]: val
              };
              break;
            case "<=":
              field = {
                [Op.lte]: val
              };
              break;
            case "=":
              field = {
                [Op.eq]: val
              };
              break;
            case "<>":
              field = {
                [Op.ne]: val
              };
              break;
            case "like":
              field = {
                [Op.like]: "%" + val + "%"
              };
              break;
            default:
              field = {
                [Op.eq]: val
              };
              break;
          }
        } else {
          field = {
            [Op.eq]: values[1]
          };
        }
        let temp = {};
        temp[`${values[0]}`] = field;
        and_clause.push(temp);
      }
    }
    resolve({ [Op.and]: and_clause });
  });
};
