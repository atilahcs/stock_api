module.exports = {
    pageNumber: (page, pageLimit, startAtZero = true) => {
        let pageNumber = 0;
        if(startAtZero){
            pageNumber = page * pageLimit;
            return pageNumber;
        } else {
            if(page == 1){
                pageNumber = 0;
            } else {
                pageNumber = page * pageLimit;
            }
            return pageNumber;
        }
        
    }
}