module.exports = {
  generateContent: async (po_id, po_info, po_detail) => {
    let img_path = `${__dirname}../../assets/img/.jpg`;
    let po_no = '00000' + po_id;
    po_no = po_no.slice(-6);
    const po_number = '#PO' + po_no;

    let supplier_info = 'Purchase to:\n' + po_info.supplier_name + '\nTax ID:' + po_info.tax_id + '\n' + po_info.address;
    supplier_info += '\nAtten to:\n' + po_info.contact_name;
    supplier_info += '\n' + po_info.contact_email + ', ' + po_info.contact_no;

    let billing_info = 'Billing to:\n';

    let delivery_info = 'Delivery to:\n';
    if (po_info.delivery_address)
      delivery_info += po_info.delivery_address;

    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let detail_info = po_info.order_date.getDate() + ' ' + months[po_info.order_date.getMonth()] + ', ' + po_info.order_date.getFullYear() + '\n';
    if (po_info.expect_delivery_date)
      detail_info += po_info.expect_delivery_date.getDate() + ' ' + months[po_info.expect_delivery_date.getMonth()] + ', ' + po_info.expect_delivery_date.getFullYear() + '\n';
    else
      detail_info += '\n';
    detail_info += 'Purchaser name';

    let header = [
      {
        text: '',
        margin: 15
      },
      {
        columns: [{
          width: 40,
          text: ''
        }, {
          width: 120,
          image: img_path
        }, {
          width: '*',
          text: 'Purchase Order ' + po_number,
          alignment: 'right',
          fontSize: 22
        }, {
          width: 40,
          text: ''
        }],
      }, {
        columns: [{
          width: 40,
          text: ''
        }, {
          width: '45%',
          text: supplier_info,
          fontSize: 14
        }, {
          width: '35%',
          text: billing_info,
          fontSize: 14
        }, {
          width: 40,
          text: ''
        }]
      }, {
        columns: [{
          width: 40,
          text: ''
        }, {
          width: '45%',
          text: delivery_info,
          fontSize: 14
        }, {
          width: '20%',
          text: 'Purchase Order date:\nExpect Delivery date:\nPurchaser:\nTelephone:',
          fontSize: 14
        }, {
          width: '*',
          text: detail_info,
          alignment: 'right',
          fontSize: 14
        }, {
          width: 40,
          text: ''
        }],
        margin: [0, 5, 0, 0]
      }
    ];

    let product_table_header = [{
      text: 'Item',
      alignment: 'center'
    }, {
      text: 'Qty',
      alignment: 'center'
    }, {
      text: 'Unit Price',
      alignment: 'center'
    }, {
      text: 'Discount',
      alignment: 'center'
    }, {
      text: 'Amount',
      width: '*',
      alignment: 'center'
    }];
    let product_table = {
      style: 'product_table',
      table: {
        widths: ['60%', '*', '*', '*', '*'],
        body: [product_table_header]
      }
    };
    for (let product of po_detail) {
      let temp = product.product_unit_price.toFixed(2).split('.');
      temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      let unit_price = temp.join('.');
      temp = product.product_amount.toFixed(2).split('.');
      temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      let prouduct_price = temp.join('.');
      let product_line = [
        product.product_name,
        { text: product.product_unit, alignment: 'center' },
        { text: unit_price, alignment: 'right' },
        { text: product.product_unit_discount, alignment: 'right' },
        { text: prouduct_price, alignment: 'right' }];
      product_table.table.body.push(product_line);
    }
    let temp = po_info.total_price.toFixed(2).split('.');
    temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let total_product_excl = temp.join('.');
    let order_summary_line_1 = {
      columns: [{
        width: '75%',
        text: 'Total Product (excl. Vat)',
      }, {
        width: '*',
        text: total_product_excl,
      }],
      style: 'summary_info',
      margin: [0, 15, 0, 0]
    };
    let vat_text = 'VAT';
    if (po_info.inc_tax)
      vat_text += ' (7%)';
    temp = po_info.inc_tax.toFixed(2).split('.');
    temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let total_vat = temp.join('.');
    let order_summary_line_2 = {
      columns: [{
        width: '75%',
        text: vat_text,
      }, {
        width: '*',
        text: total_vat,
      }],
      style: 'summary_info',
      margin: [0, 5, 0, 0]
    };
    temp = po_info.final_price.toFixed(2).split('.');
    temp[0] = temp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let total_product_incl = temp.join('.');
    let order_summary_line_3 = {
      columns: [{
        width: '75%',
        text: 'Total (incl. Vat)'
      }, {
        width: '*',
        text: total_product_incl
      }],
      margin: [0, 5, 0, 0],
      style: 'summary_info_total'
    };

    let footer = {
      columns: [{
        width: '33.33%',
        text: '______________________\nPurchase Department\n(Date:______________)',
        alignment: 'center',
      }, {
        width: '33.33%',
        text: '______________________\nApproved by\n(Date:______________)',
        alignment: 'center'
      }, {
        width: '33.33%',
        text: '______________________\nSupplier\n(Date:______________)',
        alignment: 'center'
      }],
      style: 'product_table',
      margin: [0, 0, 0, 40]
    };

    let pdf_content = {
      pageMargins: [40, 300, 40, 90],
      header: header,
      content: [product_table, order_summary_line_1, order_summary_line_2, order_summary_line_3],
      footer: footer,
      styles: {
        product_table: {
          fontSize: 14
        },
        summary_info: {
          fontSize: 16,
          alignment: 'right'
        },
        summary_info_total: {
          fontSize: 20,
          alignment: 'right'
        }
      }
    };

    return pdf_content;
  },
  createFile: async (pdf_filename, pdf_content) => {
    const PdfPrinter = require('pdfmake');
    const FONT_PATH = `${__dirname}../../assets/fonts/`
    const fonts = {
      Roboto: {
        normal: FONT_PATH + 'THSarabunNew.ttf',
        bold: FONT_PATH + 'THSarabunNew-Bold.ttf',
        italics: FONT_PATH + 'THSarabunNew-Italic.ttf',
        bolditalics: FONT_PATH + 'THSarabunNew-BoldItalic.ttf'
      }
    };
    const printer = new PdfPrinter(fonts);
    var pdfDoc = printer.createPdfKitDocument(pdf_content);
    const fs = require('fs');
    pdfDoc.pipe(fs.createWriteStream(`pdfs/${pdf_filename}.pdf`));
    pdfDoc.end();
    return;
  },
  sendFile: async (pdf_filename, pdf_content, res) => {
    const PdfPrinter = require('pdfmake');
    const FONT_PATH = `${__dirname}../../assets/fonts/`
    const fonts = {
      Roboto: {
        normal: FONT_PATH + 'THSarabunNew.ttf',
        bold: FONT_PATH + 'THSarabunNew-Bold.ttf',
        italics: FONT_PATH + 'THSarabunNew-Italic.ttf',
        bolditalics: FONT_PATH + 'THSarabunNew-BoldItalic.ttf'
      }
    };
    const printer = new PdfPrinter(fonts);
    var pdfDoc = printer.createPdfKitDocument(pdf_content);
    res.setHeader('Content-disposition', `filename=${pdf_filename}.pdf`);
    pdfDoc.pipe(res);
    return pdfDoc.end();
    // const fs = require('fs');
    // pdfDoc.pipe(fs.createWriteStream(`pdfs/${pdf_filename}.pdf`));
    // pdfDoc.end();
    // return;
  }
}