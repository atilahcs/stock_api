module.exports = {
  validateProduct: async (req, res, next) => {
    try {
      let { product_id } = req.params;
      const productResult = await Product.findOne({ where: { idprestashop: product_id }});
      if (!productResult)
        throw { status: 400, message: 'Product not found' };

      next();
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}