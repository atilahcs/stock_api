module.exports = (order_detail) => {
  return new Promise((resolve, reject) => {
    let id_list = [];
    let product_list = [];
    for (let detail of order_detail) {
      let product = {
        product_id: Number(detail.product_id),
        name: detail.product_name,
        reference: detail.product_reference,
        quantity: Number(detail.product_quantity),
        scan_quantity: 0,
        img_url: detail.img_url,
        is_set: false,
        unit_price: Number(detail.unit_price_tax_excl),
        total_price: Number(detail.total_price_tax_excl)
      }
      let index = id_list.indexOf(product.product_id);
      if (index > -1) {
        product_list[index].quantity += product.quantity;
        product_list[index].total_price = product_list[index].quantity * product_list[index].unit_price;
        if (detail.is_set)
          for (product_in_set of product_list[index].products_in_set) {
            product_in_set.quantity = product_in_set.set_quantity * product_list[index].quantity;
          }
      } else {
        if (detail.is_set) {
          product.is_set = true;
          let product_set_list = [];
          for (product_in_set of detail.products_set) {
            let product_set = {
              product_id: Number(product_in_set.product_id),
              name: product_in_set.product_name,
              reference: product_in_set.product_reference,
              set_quantity: Number(product_in_set.set_item_quantity),
              quantity: Number(product_in_set.set_item_quantity * product.quantity),
              scan_quantity: 0,
              img_url: product_in_set.img,
              is_set: false,
              product_parent_id: product.product_id,
              unit_price: 0,
              total_price: 0
            }
            product_set_list.push(product_set);
          }
          product.products_in_set = product_set_list;
        }
        product_list.push(product);
        id_list.push(product.product_id);
      }
    }
    resolve(product_list);
  });
}