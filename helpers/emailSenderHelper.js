var env = process.env.NODE_ENV || "development";
var config = require(__dirname + "/../config/config.json")[env];
module.exports = {
  oauthMethod: (access_token, sender, receiver, topic, message, cc = null, attachment_file_name = null, attachment_location = null, attachment_type = null) => {
    return new Promise((resolve, reject) => {
      const nodemailer = require('nodemailer');

      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          type: 'OAuth2',
          user: sender,
          clientId: config.EMAIL_CLIENT_ID,
          clientSecret: config.EMAIL_CLIENT_SECRET,
          // refreshToken: '',
          accessToken: access_token
        }
      });

      let mailOptions = {
        from: sender,
        to: receiver,
        subject: topic,
        html: message,
        cc: cc || null,
        attachments: (attachment_file_name && attachment_type && attachment_location) ? [{
          filename: attachment_file_name,
          contentType: attachment_type,
          path: attachment_location
        }] : null
      }

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          let message = 'Something went wrong from email sender.';
          if (typeof error == 'string')
            message = error;
          if (error && error.message)
            message = error.message;
          reject({ message });
        } else {
          const fs = require('fs');
          fs.unlink(attachment_location, function (err) {
            resolve(true);
          });
        }
      });
    });
  }
}