module.exports = ()=>{
    switch(process.env.NODE_ENV){
        case 'development':
            return require('./dev');
            break;
        case 'production':
            return require('./prod');
            break;
        default:
            return require('./dev');
            break;
    }
}