"use strict";
module.exports = (sequelize, DataTypes) => {
  var SupplierProduct = sequelize.define(
    "SupplierProduct",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: "idprestashop"
      },
      pid: {
        type: DataTypes.INTEGER,
        field: "pid"
      },
      supplier_id: {
        type: DataTypes.INTEGER,
        field: "id_supplier"
      },
      sid: {
        type: DataTypes.INTEGER,
        field: "sid"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_supplier_product"
    }
  );
  SupplierProduct.associate = ({ Product, Supplier }) => {
    SupplierProduct.belongsTo(Product, {
      foreignKey: "pid"
    });

    SupplierProduct.belongsTo(Supplier, {
      foreignKey: "sid"
    });
  };
  return SupplierProduct;
};
