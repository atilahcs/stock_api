'use strict';
module.exports = (sequelize, DataTypes) => {
  var BarOrderDetail = sequelize.define('BarOrderDetail', {
    bar_order_id: {
      type: DataTypes.INTEGER,
      field: 'bar_order_id'
    },
    bar_id: {
      type: DataTypes.INTEGER,
      field: 'bar_id'
    },
    product_name: {
      type: DataTypes.STRING,
      field: 'prodduct_name'
    },
    product_id: {
      type: DataTypes.INTEGER,
      field: 'product_id'
    },
    pid: {
      type: DataTypes.INTEGER,
      field: 'pid'
    },
    product_reference: {
      type: DataTypes.STRING,
      field: 'product_reference'
    },
    product_unit: {
      type: DataTypes.INTEGER,
      field: 'product_unit'
    },
    product_unit_price: {
      type: DataTypes.FLOAT,
      field: 'product_unit_price'
    },
    product_unit_discount: {
      type: DataTypes.STRING,
      field: 'product_unit_discount'
    },
    product_amount: {
      type: DataTypes.FLOAT,
      field: 'product_amount'
    },
    product_amount_after_discount: {
      type: DataTypes.FLOAT,
      field: 'product_amount_after_discount'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  }, {
      freezeTableName: true,
      tableName: 'ws_list_bar_order'
    });
  BarOrderDetail.associate = ({
    BarOrder
  }) => {
    BarOrderDetail.belongsTo(BarOrder, {
      foreignKey: 'bar_order_id'
    })
  }
  return BarOrderDetail;
};