'use strict';
module.exports = (sequelize, DataTypes) => {
  var AdminToken = sequelize.define('AdminToken', {
    admin_ps_id: {
      type: DataTypes.INTEGER,
      field: 'admin_ps_id'
    },
    admin_email: {
      type: DataTypes.STRING,
      field: 'admin_email'
    },
    admin_firstname: {
      type: DataTypes.STRING,
      field: 'admin_firstname'
    },
    admin_lastname: {
      type: DataTypes.STRING,
      field: 'admin_lastname'
    },
    token: {
      type: DataTypes.STRING,
      field: 'token'
    },
    agent: {
      type: DataTypes.STRING,
      field: 'agent'
    },
    created_time: {
      type: DataTypes.DATE,
      defaultValue: new Date(),
      field: 'created_time'
    },
    updated_time: {
      type: DataTypes.DATE,
      defaultValue: new Date(),
      field: 'updated_time'
    }
  }, {
      freezeTableName: true,
      tableName: 'ws_admin_token'
    });
  return AdminToken;
};