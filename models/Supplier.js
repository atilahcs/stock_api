"use strict";
module.exports = (sequelize, DataTypes) => {
  var Supplier = sequelize.define(
    "Supplier",
    {
      supplier_name: {
        type: DataTypes.STRING,
        field: "supplier_name"
      },
      address: {
        type: DataTypes.STRING,
        field: "address"
      },
      city: {
        type: DataTypes.STRING,
        field: "city"
      },
      zipcode: {
        type: DataTypes.STRING,
        field: "zipcode"
      },
      country: {
        type: DataTypes.STRING,
        field: "country"
      },
      street_address: {
        type: DataTypes.STRING,
        field: "street_address"
      },
      street_city: {
        type: DataTypes.STRING,
        field: "street_city"
      },
      street_zipcode: {
        type: DataTypes.STRING,
        field: "street_zipcode"
      },
      street_country: {
        type: DataTypes.STRING,
        field: "street_country"
      },

      sale_setting: {
        type: DataTypes.STRING,
        field: "sale_setting"
      },
      purchase_setting: {
        type: DataTypes.STRING,
        field: "purchase_setting"
      },

      bill_duedate: {
        type: DataTypes.STRING,
        field: "bill_duedate"
      },
      invoice_duedate: {
        type: DataTypes.STRING,
        field: "invoice_duedate"
      },
      email: {
        type: DataTypes.STRING,
        field: "email"
      },
      tax_id: {
        type: DataTypes.STRING,
        field: "tax_id"
      },

      contact: {
        type: DataTypes.STRING,
        field: "contact"
      },
      contact_no: {
        type: DataTypes.STRING,
        field: "contact_no"
      },
      xero_supplier_id: {
        type: DataTypes.STRING,
        field: "xero_supplier_id"
      },
      prestashop_id: {
        type: DataTypes.INTEGER,
        field: "prestashop_id"
      },
      status: {
        type: DataTypes.STRING,
        field: "status"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_supplier_new"
    }
  );
  Supplier.associate = ({ Product, SupplierProduct }) => {
    Supplier.hasMany(SupplierProduct, {
      foreignKey: "supplier_id",
      sourceKey: "prestashop_id"
    });
    Supplier.belongsToMany(Product, {
      through: {
        model: SupplierProduct,
        unique: false
      },
      foreignKey: "sid",
      constraints: true
    });
  };

  return Supplier;
};
