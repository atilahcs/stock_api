'use strict'
module.exports = (sequelize, DataTypes) => {
  var StockCountDetail = sequelize.define('StockCountDetail', {
    stock_count_id: {
      type: DataTypes.INTEGER,
      field: 'stock_count_id'
    },
    product_id: {
      type: DataTypes.INTEGER,
      field: 'product_id'
    },
    pid: {
      type: DataTypes.INTEGER,
      field: 'pid'
    },
    product_name: {
      type: DataTypes.STRING,
      field: 'product_name'
    },
    product_barcode: {
      type: DataTypes.STRING,
      field: 'product_barcode'
    },
    product_reference: {
      type: DataTypes.STRING,
      field: 'product_reference'
    },
    instock_quantity: {
      type: DataTypes.INTEGER,
      field: 'instock_quantity'
    },
    counter_quantity: {
      type: DataTypes.INTEGER,
      field: 'counter_quantity'
    },
    purchasing_price: {
      type: DataTypes.FLOAT,
      field: 'purchasing_price'
    },
    base_price: {
      type: DataTypes.FLOAT,
      field: 'base_price'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  },{
    freezeTableName: true,
    tableName: "ws_list_stock_count"
  });
  StockCountDetail.associate = ({
    StockCount,
    Product
  }) => {
    StockCountDetail.belongsTo(StockCount, {
      foreignKey: 'stock_count_id'
    });
    StockCountDetail.belongsTo(Product, {
      foreignKey: 'pid'
    });
  }
  return StockCountDetail;
}