'use strict';
module.exports = (sequelize, DataTypes) => {
    var OrderScanVersion = sequelize.define('OrderScanVersion', {
        oid: {
            type: DataTypes.INTEGER,
            field: 'oid'
        },
        osid: {
            type: DataTypes.INTEGER,
            field: 'osid'
        },
        order_prestashop_id: {
            type: DataTypes.INTEGER,
            field: 'order_prestashop_id'
        },
        order_status: {
            type: DataTypes.STRING,
            field: 'order_status'
        },
        version_number: {
            type: DataTypes.INTEGER,
            field: 'version_number'
        },
        scan_by_id: {
            type: DataTypes.INTEGER,
            field: 'scan_by_id'
        },
        scan_by_name: {
            type: DataTypes.STRING,
            field: 'scan_by_name'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_order_scan_version'
    });
    OrderScanVersion.associate = ({
        OrderScanMovement,
        OrderScanVersionDetail,
        OrderPrestashop,
        OrderScan
    }) => {
        OrderScanVersion.hasMany(OrderScanMovement, {
            foreignKey: 'osvid',
        });
        OrderScanVersion.hasMany(OrderScanVersionDetail, {
            foreignKey: 'osvid',
        });
        OrderScanVersion.belongsTo(OrderPrestashop, {
            foreignKey: 'oid',
            targetKey: 'id'
        });
        OrderScanVersion.belongsTo(OrderScan, {
            foreignKey: 'osid',
            targetKey: 'id'
        });
    };
    return OrderScanVersion;
};