"use strict";
module.exports = (sequelize, DataTypes) => {
  var ErrorLog = sequelize.define(
    "ErrorLog",
    {
      controller_name: {
        type: DataTypes.STRING,
        field: "controller_name"
      },
      action_name: {
        type: DataTypes.STRING,
        field: "action_name"
      },
      path: {
        type: DataTypes.STRING,
        field: "path"
      },
      method: {
        type: DataTypes.STRING,
        field: "method"
      },
      message: {
        type: DataTypes.TEXT,
        field: "message"
      },
      params: {
        type: DataTypes.TEXT,
        field: "params"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_error_log"
    }
  );
  return ErrorLog;
};
