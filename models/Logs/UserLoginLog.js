const mongoose = require('mongoose');
const { Schema } = mongoose;

const userLoginLogSchema = new Schema({
    user: String,
    email: String,
    message: String,
});

mongoose.model('userLoginLogs', userLoginLogSchema);