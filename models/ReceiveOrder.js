'use strict'
module.exports = (sequelize, DataTypes) => {
  var ReceiveOrder = sequelize.define("ReceiveOrder", {
    po_id: {
      type: DataTypes.INTEGER,
      field: 'po_id'
    },
    po_reference: {
      type: DataTypes.INTEGER,
      field: 'po_reference'
    },
    receive_status: {
      type: DataTypes.INTEGER,
      field: 'receive_status'
    },
    reference_invoice: {
      type: DataTypes.INTEGER,
      field: 'reference_invoice'
    },
    supplier_id: {
      type: DataTypes.INTEGER,
      field: 'supplier_id'
    },
    sid: {
      type: DataTypes.INTEGER,
      field: 'sid'
    },
    supplier_name: {
      type: DataTypes.INTEGER,
      field: 'supplier_name'
    },
    receive_remark: {
      type: DataTypes.INTEGER,
      field: 'receive_remark'
    },
    due_date: {
      type: DataTypes.DATE,
      field: 'due_date'
    },
    order_date: {
      type: DataTypes.DATE,
      field: 'order_date'
    },
    expected_delivery_date: {
      type: DataTypes.DATE,
      field: 'expected_delivery_date'
    },
    received_date: {
      type: DataTypes.DATE,
      field: 'received_date'
    },
    scan_by_id: {
      type: DataTypes.INTEGER,
      field: 'scan_by_id'
    },
    scan_by_name: {
      type: DataTypes.INTEGER,
      field: 'scan_by_name'
    },
    default_image: {
      type: DataTypes.INTEGER,
      field: 'default_image'
    },
    total_tax_excl: {
      type: DataTypes.FLOAT,
      field: 'total_tax_excl'
    },
    total_tax: {
      type: DataTypes.FLOAT,
      field: 'total_tax'
    },
    total_tax_incl: {
      type: DataTypes.FLOAT,
      field: 'total_tax_incl'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  },
    {
      freezeTableName: true,
      tableName: "ws_receiving"
    }
  );
  ReceiveOrder.associate = ({
    ReceiveOrderDetail,
    ReceiveOrderFile,
    PurchaseOrder
  }) => {
    // associations can be defined here
    ReceiveOrder.hasMany(ReceiveOrderDetail, {
      foreignKey: 'ro_id'
    })
    ReceiveOrder.hasMany(ReceiveOrderFile, {
      foreignKey: 'ro_id'
    })
    ReceiveOrder.belongsTo(PurchaseOrder, {
      foreignKey: 'po_id'
    })
  };
  return ReceiveOrder;
}