"use strict";
module.exports = (sequelize, DataTypes) => {
  var OrderScanDetail = sequelize.define(
    "OrderScanDetail",
    {
      osid: {
        type: DataTypes.INTEGER,
        field: 'osid'
      },
      odid: {
        type: DataTypes.INTEGER,
        field: 'odid'
      },
      order_prestashop_id: {
        type: DataTypes.INTEGER,
        field: 'order_prestashop_id'
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: 'product_id'
      },
      pid: {
        type: DataTypes.INTEGER,
        field: 'pid'
      },
      product_name: {
        type: DataTypes.STRING,
        field: 'product_name'
      },
      product_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_quantity'
      },
      product_in_set_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_in_set_quantity'
      },
      product_barcode: {
        type: DataTypes.STRING,
        field: 'product_barcode'
      },
      product_reference: {
        type: DataTypes.STRING,
        field: 'product_reference'
      },
      is_set: {
        type: DataTypes.BOOLEAN,
        field: 'is_set'
      },
      product_parent_id: {
        type: DataTypes.INTEGER,
        field: 'product_parent_id'
      },
      product_parent_name: {
        type: DataTypes.STRING,
        field: 'product_parent_name'
      },
      in_box_quantity: {
        type: DataTypes.INTEGER,
        field: 'in_box_quantity'
      },
      scan_quantity: {
        type: DataTypes.INTEGER,
        field: 'scan_quantity'
      },
      scan_by_id: {
        type: DataTypes.INTEGER,
        field: 'scan_by_id'
      },
      scan_by_name: {
        type: DataTypes.STRING,
        field: 'scan_by_name'
      },
      scan_result: {
        type: DataTypes.STRING,
        field: 'scan_result'
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        field: 'deleted'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_order_scan_detail"
    }
  );
  OrderScanDetail.associate = ({
    OrderPrestashopDetail,
    OrderScan,
    OrderScanVersionDetail,
    Product
  }) => {
    // associations can be defined here
    OrderScanDetail.belongsTo(OrderScan, {
      foreignKey: "osid",
      targetKey: "id"
    });
    OrderScanDetail.hasMany(OrderScanVersionDetail, {
      foreignKey: "osdid"
    });
    OrderScanDetail.belongsTo(OrderPrestashopDetail, {
      foreignKey: "odid"
    });
    OrderScanDetail.belongsTo(Product, {
      foreignKey: "pid"
    });
  };
  return OrderScanDetail;
};
