'use strict';
module.exports = (sequelize, DataTypes) => {
    var Movement = sequelize.define('Movement', {
        oid: {
            type: DataTypes.INTEGER,
            field: 'oid'
        },
        odid: {
            type: DataTypes.INTEGER,
            field: 'odid'
        },
        user_id: {
            type: DataTypes.INTEGER,
            field: 'user_id'
        },
        user_name: {
            type: DataTypes.STRING,
            field: 'user_name'
        },
        pid: {
            type: DataTypes.INTEGER,
            field: 'pid'
        },
        product_id: {
            type: DataTypes.INTEGER,
            field: 'product_id'
        },
        product_name: {
            type: DataTypes.STRING,
            field: 'product_name'
        },
        product_reference: {
            type: DataTypes.STRING,
            field: 'product_reference'
        },
        product_barcode: {
            type: DataTypes.STRING,
            field: 'product_barcode'
        },
        action_id: {
            type: DataTypes.INTEGER,
            field: 'action_id'
        },
        action_name: {
            type: DataTypes.STRING,
            field: 'action_name'
        },
        action_type: {
            type: DataTypes.STRING,
            field: 'action_type'
        },
        location_id: {
            type: DataTypes.INTEGER,
            field: 'location_id'
        },
        location_name: {
            type: DataTypes.STRING,
            field: 'location_name'
        },
        instock_quantity: {
            type: DataTypes.INTEGER,
            field: 'instock_quantity'
        },
        action_quantity: {
            type: DataTypes.INTEGER,
            field: 'action_quantity'
        },
        after_action_quantity: {
            type: DataTypes.INTEGER,
            field: 'after_action_quantity'
        },
        reference_id: {
            type: DataTypes.STRING,
            field: 'reference_id'
        },
        remark: {
            type: DataTypes.STRING,
            field: 'remark'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_movement_logs_flat'
    });
    Movement.associate = ({
        Product,
        OrderPrestashop,
        OrderPrestashopDetail,
        OrderScanMovement,
        BranchLocation,
        AdjustmentDetail
    }) => {
        // associations can be defined here
        Movement.belongsTo(Product, {
            foreignKey: 'pid'
        })
        Movement.belongsTo(OrderPrestashop, {
            foreignKey: 'oid'
        })
        Movement.belongsTo(OrderPrestashopDetail, {
            foreignKey: 'odid'
        })
        Movement.hasOne(OrderScanMovement, {
            foreignKey: 'movement_id'
        })
        Movement.belongsTo(BranchLocation, {
            foreignKey: 'location_id'
        });
        Movement.hasOne(AdjustmentDetail, {
            foreignKey: 'movement_id'
        })
    };
    return Movement;
};