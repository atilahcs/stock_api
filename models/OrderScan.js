'use strict';
module.exports = (sequelize, DataTypes) => {
    var OrderScan = sequelize.define('OrderScan', {
        oid: {
            type: DataTypes.INTEGER,
            field: 'oid'
        },
        version_number: {
            type: DataTypes.INTEGER,
            field: 'version_number'
        },
        order_prestashop_id: {
            type: DataTypes.INTEGER,
            field: 'order_prestashop_id'
        },
        order_status: {
            type: DataTypes.STRING,
            field: 'order_status'
        },
        scan_by_id: {
            type: DataTypes.INTEGER,
            field: 'scan_by_id'
        },
        scan_by_name: {
            type: DataTypes.STRING,
            field: 'scan_by_name'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_order_scan'
    });
    OrderScan.associate = ({
        OrderPrestashop,
        OrderScanVersion,
        OrderScanDetail
    }) => {
        OrderScan.belongsTo(OrderPrestashop, {
            foreignKey: 'oid',
            targetKey: 'id'
        });
        OrderScan.hasMany(OrderScanVersion, {
            foreignKey: 'osid'
        });
        OrderScan.hasMany(OrderScanDetail, {
            foreignKey: 'osid'
        });
    };
    return OrderScan;
};