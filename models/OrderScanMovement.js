"use strict";
module.exports = (sequelize, DataTypes) => {
  var OrderScanMovement = sequelize.define(
    "OrderScanMovement",
    {
      oid: {
        type: DataTypes.INTEGER,
        field: 'oid'
      },
      odid: {
        type: DataTypes.INTEGER,
        field: 'odid'
      },
      osvid: {
        type: DataTypes.INTEGER,
        field: 'osvid'
      },
      osvdid: {
        type: DataTypes.INTEGER,
        field: 'osvdid'
      },
      movement_id: {
        type: DataTypes.INTEGER,
        field: 'movement_id'
      },
      order_prestashop_id: {
        type: DataTypes.INTEGER,
        field: 'order_prestashop_id'
      },
      version_number: {
        type: DataTypes.INTEGER,
        field: 'version_number'
      },
      scan_by_id: {
        type: DataTypes.INTEGER,
        field: 'scan_by_id'
      },
      scan_by_name: {
        type: DataTypes.STRING,
        field: 'scan_by_name'
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: 'product_id'
      },
      pid: {
        type: DataTypes.INTEGER,
        field: 'pid'
      },
      product_name: {
        type: DataTypes.STRING,
        field: 'product_name'
      },
      product_barcode: {
        type: DataTypes.STRING,
        field: 'product_barcode'
      },
      product_reference: {
        type: DataTypes.STRING,
        field: 'product_reference'
      },
      product_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_quantity'
      },
      product_in_set_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_in_set_quantity'
      },
      is_set: {
        type: DataTypes.BOOLEAN,
        field: 'is_set'
      },
      product_parent_id: {
        type: DataTypes.INTEGER,
        field: 'product_parent_id'
      },
      product_parent_name: {
        type: DataTypes.STRING,
        field: 'product_parent_name'
      },
      in_box_quantity: {
        type: DataTypes.INTEGER,
        field: 'in_box_quantity'
      },
      scan_quantity: {
        type: DataTypes.STRING,
        field: 'scan_quantity'
      },
      scan_result: {
        type: DataTypes.STRING,
        field: 'scan_result'
      },
      action_id: {
        type: DataTypes.INTEGER,
        field: 'action_id'
      },
      action_name: {
        type: DataTypes.STRING,
        field: 'action_name'
      },
      action_type: {
        type: DataTypes.STRING,
        field: 'action_type'
      },
      action_quantity: {
        type: DataTypes.INTEGER,
        field: 'action_quantity'
      },
      reason_id: {
        type: DataTypes.INTEGER,
        field: 'reason_id'
      },
      reason_name: {
        type: DataTypes.STRING,
        field: 'reason_name'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_order_scan_movement"
    }
  );
  OrderScanMovement.associate = ({
    OrderPrestashop,
    OrderPrestashopDetail,
    Movement,
    OrderScanVersion,
    OrderScanVersionDetail,
    Product
  }) => {
    OrderScanMovement.belongsTo(OrderPrestashop, {
      foreignKey: "oid",
      targetKey: "id"
    });
    OrderScanMovement.belongsTo(OrderPrestashopDetail, {
      foreignKey: "odid"
    });
    OrderScanMovement.belongsTo(Movement, {
      foreignKey: "movement_id",
      targetKey: "id"
    });
    OrderScanMovement.belongsTo(OrderScanVersion, {
      foreignKey: "osvid",
      targetKey: "id"
    });
    OrderScanMovement.belongsTo(OrderScanVersionDetail, {
      foreignKey: "osvdid",
      targetKey: "id"
    });
    OrderScanMovement.belongsTo(Product, {
      foreignKey: "pid"
    });
  };
  return OrderScanMovement;
};
