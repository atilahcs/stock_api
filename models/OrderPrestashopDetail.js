"use strict";
module.exports = (sequelize, DataTypes) => {
  var OrderPrestashopDetail = sequelize.define(
    "OrderPrestashopDetail",
    {
      oid: {
        type: DataTypes.INTEGER,
        field: 'oid'
      },
      order_prestashop_id: {
        type: DataTypes.INTEGER,
        field: 'order_prestashop_id'
      },
      pid: {
        type: DataTypes.INTEGER,
        field: 'pid'
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: 'product_id'
      },
      product_name: {
        type: DataTypes.STRING,
        field: 'product_name'
      },
      product_reference: {
        type: DataTypes.STRING,
        field: 'product_reference'
      },
      product_barcode: {
        type: DataTypes.STRING,
        field: 'product_barcode'
      },
      product_unit_price: {
        type: DataTypes.FLOAT,
        field: 'product_unit_price'
      },
      product_total_price: {
        type: DataTypes.FLOAT,
        field: 'product_total_price'
      },
      product_image: {
        type: DataTypes.STRING,
        field: 'product_image'
      },
      is_set: {
        type: DataTypes.BOOLEAN,
        field: 'is_set'
      },
      product_parent_id: {
        type: DataTypes.INTEGER,
        field: 'product_parent_id'
      },
      product_parent_name: {
        type: DataTypes.STRING,
        field: 'product_parent_name'
      },
      product_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_quantity'
      },
      product_in_set_quantity: {
        type: DataTypes.INTEGER,
        field: 'product_in_set_quantity'
      },
      product_quantity_instock: {
        type: DataTypes.INTEGER,
        field: 'product_quantity_instock'
      },
      in_box_quantity: {
        type: DataTypes.INTEGER,
        field: 'in_box_quantity'
      },
      scan_quantity: {
        type: DataTypes.INTEGER,
        field: 'scan_quantity'
      },
      scan_result: {
        type: DataTypes.STRING,
        field: 'scan_result'
      },
      scan_by_id: {
        type: DataTypes.INTEGER,
        field: 'scan_by_id'
      },
      scan_by_name: {
        type: DataTypes.STRING,
        field: 'scan_by_name'
      },
      remark: {
        type: DataTypes.STRING,
        field: 'remark'
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        field: 'deleted'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_order_prestashop_detail"
    }
  );
  OrderPrestashopDetail.associate = ({
    OrderPrestashop,
    OrderScanMovement,
    Movement,
    OrderScanDetail,
    OrderScanVersionDetail,
    Product
  }) => {
    OrderPrestashopDetail.belongsTo(OrderPrestashop, {
      foreignKey: "oid"
    });
    OrderPrestashopDetail.hasMany(OrderScanMovement, {
      foreignKey: "odid"
    });
    OrderPrestashopDetail.hasMany(Movement, {
      foreignKey: "odid"
    });
    OrderPrestashopDetail.hasOne(OrderScanDetail, {
      foreignKey: "odid"
    });
    OrderPrestashopDetail.hasMany(OrderScanVersionDetail, {
      foreignKey: "odid"
    });
    OrderPrestashopDetail.belongsTo(Product, {
      foreignKey: "pid"
    });
  };
  return OrderPrestashopDetail;
};
