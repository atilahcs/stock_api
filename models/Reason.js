"use strict";
module.exports = (sequelize, DataTypes) => {
  var Reason = sequelize.define(
    "Reason",
    {
      reason_detail: {
        type: DataTypes.STRING,
        field: 'reason_detail'
      },
      reason_parent: {
        type: DataTypes.INTEGER,
        field: 'reason_parent'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_reason"
    }
  );

  return Reason;
};
