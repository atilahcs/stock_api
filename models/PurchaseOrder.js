'use strict';
module.exports = (sequelize, DataTypes) => {
  var PurchaseOrder = sequelize.define(
    'PurchaseOrder',
    {
      supplier_id: {
        type: DataTypes.INTEGER,
        field: 'supplier_id'
      },
      sid: {
        type: DataTypes.INTEGER,
        field: 'sid'
      },
      supplier_name: {
        type: DataTypes.STRING,
        field: 'supplier_name'
      },
      address: {
        type: DataTypes.STRING,
        field: 'address'
      },
      tax_id: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'tax_id'
      },
      contact_name: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'contact_name'
      },
      contact_no: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'contact_no'
      },
      contact_email: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'contact_email'
      },
      delivery_address_id: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        field: 'delivery_address_id'
      },
      delivery_address_name: {
        type: DataTypes.STRING,
        defaultValue: 0,
        field: 'delivery_address_name'
      },
      delivery_address: {
        type: DataTypes.TEXT,
        defaultValue: '',
        field: 'delivery_address'
      },
      purchaser_name: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'purchaser_name'
      },
      purchaser_email: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'purchaser_email'
      },
      purchaser_no: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'purchaser_no'
      },
      order_date: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'order_date'
      },
      payment_type: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'payment_type'
      },
      credit_date: {
        type: DataTypes.INTEGER,
        field: 'credit_date'
      },
      expect_delivery_date: {
        type: DataTypes.DATE,
        field: 'expect_delivery_date'
      },
      total_price: {
        type: DataTypes.FLOAT,
        field: 'total_price'
      },
      inc_tax: {
        type: DataTypes.FLOAT,
        field: 'inc_tax'
      },
      final_price: {
        type: DataTypes.FLOAT,
        field: 'final_price'
      },
      internal_remark: {
        type: DataTypes.TEXT,
        field: 'internal_remark'
      },
      remark: {
        type: DataTypes.STRING,
        field: 'remark'
      },
      xero_id: {
        type: DataTypes.STRING,
        field: 'xero_id'
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        field: 'deleted'
      },
      status: {
        type: DataTypes.STRING,
        defaultValue: 'Waiting',
        field: 'status'
      },
      created_user: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'created_user'
      },
      latest_updated_user: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'latest_updated_user'
      },
      approved_user: {
        type: DataTypes.STRING,
        defaultValue: '',
        field: 'approved_user'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_po"
    }
  );
  PurchaseOrder.associate = ({
    PurchaseOrderDetail,
    PurchaseOrderLog,
    ReceiveOrder
  }) => {
    // associations can be defined here
    PurchaseOrder.hasMany(PurchaseOrderDetail, {
      foreignKey: "po_id"
    });
    PurchaseOrder.hasMany(PurchaseOrderLog, {
      foreignKey: "po_id"
    });
    PurchaseOrder.hasMany(ReceiveOrder, {
      foreignKey: "po_id"
    });
  };
  return PurchaseOrder;
}