'use strict';
module.exports = (sequelize, DataTypes) => {
  var Homebar = sequelize.define('Homebar', {
    company_name: {
      type: DataTypes.STRING,
      field: 'company_name'
    },
    homebar: {
      type: DataTypes.BOOLEAN,
      field: 'homebar'
    },
    address: {
      type: DataTypes.TEXT,
      field: 'address'
    },
    tax_id: {
      type: DataTypes.STRING,
      field: 'tax_id'
    },
    contact_name: {
      type: DataTypes.STRING,
      field: 'contact'
    },
    contact_no: {
      type: DataTypes.STRING,
      field: 'contact_no'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  }, {
      freezeTableName: true,
      tableName: 'ws_company'
    });
    Homebar.associate = ({
    BarOrder
  }) => {
    BarOrder = Homebar.hasMany(BarOrder, {
      foreignKey: 'bar_id'
    })
  }
  return Homebar;
};