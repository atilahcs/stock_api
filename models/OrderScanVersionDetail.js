"use strict";
module.exports = (sequelize, DataTypes) => {
  var OrderScanVersionDetail = sequelize.define(
    "OrderScanVersionDetail",
    {
      osvid: {
        type: DataTypes.INTEGER,
        field: "osvid"
      },
      odid: {
        type: DataTypes.INTEGER,
        field: "odid"
      },
      osdid: {
        type: DataTypes.INTEGER,
        field: "osdid"
      },
      order_prestashop_id: {
        type: DataTypes.INTEGER,
        field: "order_prestashop_id"
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: "product_id"
      },
      pid: {
        type: DataTypes.INTEGER,
        field: "pid"
      },
      product_name: {
        type: DataTypes.STRING,
        field: "product_name"
      },
      product_quantity: {
        type: DataTypes.INTEGER,
        field: "product_quantity"
      },
      product_in_set_quantity: {
        type: DataTypes.INTEGER,
        field: "product_in_set_quantity"
      },
      product_barcode: {
        type: DataTypes.STRING,
        field: "product_barcode"
      },
      product_reference: {
        type: DataTypes.STRING,
        field: "product_reference"
      },
      is_set: {
        type: DataTypes.BOOLEAN,
        field: "is_set"
      },
      product_parent_id: {
        type: DataTypes.INTEGER,
        field: "product_parent_id"
      },
      product_parent_name: {
        type: DataTypes.STRING,
        field: "product_parent_name"
      },
      in_box_quantity: {
        type: DataTypes.INTEGER,
        field: "in_box_quantity"
      },
      scan_quantity: {
        type: DataTypes.INTEGER,
        field: "scan_quantity"
      },
      scan_by_id: {
        type: DataTypes.INTEGER,
        field: "scan_by_id"
      },
      scan_by_name: {
        type: DataTypes.STRING,
        field: "scan_by_name"
      },
      scan_result: {
        type: DataTypes.STRING,
        field: "scan_result"
      },
      reason_id: {
        type: DataTypes.INTEGER,
        field: "reason_id"
      },
      reason_name: {
        type: DataTypes.STRING,
        field: "reason_name"
      },
      deleted: {
        type: DataTypes.BOOLEAN,
        field: "deleted"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_order_scan_version_detail"
    }
  );
  OrderScanVersionDetail.associate = ({
    OrderPrestashopDetail,
    OrderScanVersion,
    OrderScanMovement,
    OrderScanDetail,
    Product
  }) => {
    OrderScanVersionDetail.belongsTo(OrderScanVersion, {
      foreignKey: "osvid",
      targetKey: "id"
    });
    OrderScanVersionDetail.hasOne(OrderScanMovement, {
      foreignKey: "osvid"
    });
    OrderScanVersionDetail.belongsTo(OrderScanDetail, {
      foreignKey: "osdid",
      targetKey: "id"
    });
    OrderScanVersionDetail.belongsTo(OrderPrestashopDetail, {
      foreignKey: "odid",
      targetKey: "id"
    });
    OrderScanVersionDetail.belongsTo(Product, {
      foreignKey: "pid"
    });
  };
  return OrderScanVersionDetail;
};
