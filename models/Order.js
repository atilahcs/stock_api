'use strict';
module.exports = (sequelize, DataTypes) => {
    var Order = sequelize.define('Order', {
        order_id: {
            type: DataTypes.STRING,
            field: 'orderid_prestashop',
            primaryKey: true
        },
        status: {
            type: DataTypes.STRING,
            field: 'order_status'
        },
        remark: {
            type: DataTypes.STRING,
            field: 'order_remark'
        },
        customer_id: {
            type: DataTypes.INTEGER,
            field: 'customer_id'
        },
        scan_by_id: {
            type: DataTypes.INTEGER,
            field: 'scan_by_id'
        },
        scan_by_name: {
            type: DataTypes.STRING,
            field: 'scan_by_name'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_order'
    });
    Order.associate = ({
        OrderDetail
    }) => {
        Order.hasMany(OrderDetail, {
            foreignKey: 'order_id',
            sourceKey: 'order_id'
        })
    };
    return Order;
};