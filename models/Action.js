'use strict';
module.exports = (sequelize, DataTypes) => {
    var Action = sequelize.define('Action', {
        action_name: {
            type: DataTypes.STRING,
            field: 'action_name'
        },
        action_type: {
            type: DataTypes.STRING,
            field: 'action_type'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_action'
    });
    // Action.associate = ({
        
    // }) => {
    // };
    return Action;
};