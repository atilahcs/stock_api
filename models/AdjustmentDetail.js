"use strict";
module.exports = (sequelize, DataTypes) => {
  var AdjustmentDetail = sequelize.define(
    "AdjustmentDetail",
    {
      adjustment_id: {
        type: DataTypes.INTEGER,
        field: "adjustment_id"
      },
      product_id: {
        type: DataTypes.INTEGER,
        field: "product_id"
      },
      product_img: {
        type: DataTypes.STRING,
        field: "product_img"
      },
      pid: {
        type: DataTypes.INTEGER,
        field: "pid"
      },
      product_name: {
        type: DataTypes.STRING,
        field: "product_name"
      },
      product_barcode: {
        type: DataTypes.STRING,
        field: "product_barcode"
      },
      product_reference: {
        type: DataTypes.STRING,
        field: "product_reference"
      },
      purchasing_price: {
        type: DataTypes.FLOAT,
        field: "purchasing_price"
      },
      base_price: {
        type: DataTypes.FLOAT,
        field: "base_price"
      },
      location_id: {
        type: DataTypes.INTEGER,
        field: "location_id"
      },
      location_name: {
        type: DataTypes.STRING,
        field: "location_name"
      },
      instock_quantity: {
        type: DataTypes.INTEGER,
        field: "instock_quantity"
      },
      counter_quantity: {
        type: DataTypes.INTEGER,
        field: "counter_quantity"
      },
      difference_quantity: {
        type: DataTypes.INTEGER,
        field: "difference_quantity"
      },
      movement_id: {
        type: DataTypes.INTEGER,
        field: "movement_id"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_list_adjust"
    }
  );
  AdjustmentDetail.associate = ({ Adjustment, Product, Movement }) => {
    AdjustmentDetail.belongsTo(Adjustment, {
      foreignKey: "adjustment_id"
    });
    AdjustmentDetail.belongsTo(Product, {
      foreignKey: "pid"
    });
    AdjustmentDetail.belongsTo(Movement, {
      foreignKey: "movement_id"
    });
  };
  return AdjustmentDetail;
};
