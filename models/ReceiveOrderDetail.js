'use strict'
module.exports = (sequelize, DataTypes) => {
  var ReceiveOrderDetail = sequelize.define("ReceiveOrderDetail", {
    po_id: {
      type: DataTypes.INTEGER,
      field: 'po_id'
    },
    po_reference: {
      type: DataTypes.STRING,
      field: 'po_reference'
    },
    ro_id: {
      type: DataTypes.INTEGER,
      field: 'ro_id'
    },
    order_date: {
      type: DataTypes.DATE,
      field: 'order_date'
    },
    expected_delivery_date: {
      type: DataTypes.DATE,
      field: 'expected_delivery_date'
    },
    received_date: {
      type: DataTypes.DATE,
      field: 'received_date'
    },
    reference_invoice: {
      type: DataTypes.STRING,
      field: 'reference_invoice'
    },
    product_id: {
      type: DataTypes.INTEGER,
      field: 'product_id'
    },
    product_name: {
      type: DataTypes.STRING,
      field: 'product_name'
    },
    order_quantity: {
      type: DataTypes.INTEGER,
      field: 'order_quantity'
    },
    scan_quantity: {
      type: DataTypes.INTEGER,
      field: 'scan_quantity'
    },
    product_unit_price: {
      type: DataTypes.FLOAT,
      field: 'product_unit_price'
    },
    product_unit_discount: {
      type: DataTypes.STRING,
      field: 'product_unit_discount'
    },
    product_amount: {
      type: DataTypes.FLOAT,
      field: 'product_amount'
    },
    product_amount_after_discount: {
      type: DataTypes.FLOAT,
      field: 'product_amount_after_discount'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  },
    {
      freezeTableName: true,
      tableName: "ws_list_receiving"
    }
  );
  ReceiveOrderDetail.associate = ({
    ReceiveOrder,
    PurchaseOrder,
    Product
  }) => {
    // associations can be defined here
    ReceiveOrderDetail.belongsTo(ReceiveOrder, {
      foreignKey: 'ro_id'
    })
    ReceiveOrderDetail.belongsTo(PurchaseOrder, {
      foreignKey: 'po_id'
    })
    ReceiveOrderDetail.belongsTo(Product, {
      foreignKey: 'pid'
    })
  };
  return ReceiveOrderDetail;
}