'use strict';
module.exports = (sequelize, DataTypes) => {
    var OrderDetail = sequelize.define('OrderDetail', {
        order_id: {
            type: DataTypes.STRING,
            field: 'orderid_prestashop'
        },
        status: {
            type: DataTypes.STRING,
            field: 'order_status'
        },
        remark: {
            type: DataTypes.STRING,
            field: 'order_remark'
        },
        customer_id: {
            type: DataTypes.INTEGER,
            field: 'customer_id'
        },
        scan_by_id: {
            type: DataTypes.INTEGER,
            field: 'scan_by_id'
        },
        scan_by_name: {
            type: DataTypes.STRING,
            field: 'scan_by_name'
        },
        product_id: {
            type: DataTypes.INTEGER,
            field: 'product_id'
        },
        product_name: {
            type: DataTypes.STRING,
            field: 'product_name'
        },
        product_barcode: {
            type: DataTypes.STRING,
            field: 'product_barcode'
        },
        quantity: {
            type: DataTypes.INTEGER,
            field: 'quantity'
        },
        scan_qty: {
            type: DataTypes.INTEGER,
            field: 'scan_qty'
        },
        scan_result: {
            type: DataTypes,
            field: 'scan_result'
        },
        created_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'created_time'
        },
        updated_time: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
            field: 'updated_time'
        }
    }, {
        freezeTableName: true,
        tableName: 'ws_order_flat'
    });
    OrderDetail.associate = ({
        Order
    }) => {
        // associations can be defined here
        OrderDetail.belongsTo(Order, {
            foreignKey: 'order_id',
            target: 'order_id'
        })
    };
    return OrderDetail;
};