"use strict";
module.exports = (sequelize, DataTypes) => {
  var Product = sequelize.define(
    "Product",
    {
      name: {
        type: DataTypes.STRING,
        field: "name"
      },
      product_prestashop_id: {
        type: DataTypes.INTEGER,
        field: "idprestashop"
      },
      reference: {
        type: DataTypes.STRING,
        field: "reference"
      },
      status: {
        type: DataTypes.BOOLEAN,
        field: "status"
      },
      barcode: {
        type: DataTypes.STRING,
        field: "barcode"
      },
      product_img: {
        type: DataTypes.STRING,
        field: "product_img"
      },
      // shelf_location: {
      //   type: DataTypes.STRING
      // },
      product_type: {
        type: DataTypes.STRING,
        field: "product_type"
      },
      base_price: {
        type: DataTypes.FLOAT,
        field: "base_price"
      },
      bar_price: {
        type: DataTypes.FLOAT,
        field: "bar_price"
      },
      purchasing_price: {
        type: DataTypes.FLOAT,
        field: "purchasing_price"
      },
      wholesale_price: {
        type: DataTypes.FLOAT,
        field: "wholesale_price"
      },
      final_price: {
        type: DataTypes.FLOAT,
        field: "final_price"
      },
      default_supplier_id: {
        type: DataTypes.INTEGER,
        field: "default_supplier_id"
      },
      default_supplier_name: {
        type: DataTypes.STRING,
        field: "default_supplier_name"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_product"
    }
  );
  Product.associate = ({
    BranchLocation,
    ProductQuantity, // t
    Movement, // t
    OrderPrestashopDetail,
    OrderScanDetail,
    OrderScanMovement,
    OrderScanVersionDetail,
    PurchaseOrderDetail,
    ReceiveOrderDetail,
    AdjustmentDetail,
    Supplier, // t
    SupplierProduct // t
  }) => {
    Product.belongsToMany(BranchLocation, {
      through: ProductQuantity,
      foreignKey: "pid"
    });

    // associations can be defined here
    Product.hasMany(ProductQuantity, {
      foreignKey: "pid"
    });

    Product.hasMany(Movement, {
      foreignKey: "pid"
    });
    Product.hasMany(OrderPrestashopDetail, {
      foreignKey: "pid"
    });
    Product.hasMany(OrderScanDetail, {
      foreignKey: "pid"
    });
    Product.hasMany(OrderScanMovement, {
      foreignKey: "pid"
    });
    Product.hasMany(OrderScanVersionDetail, {
      foreignKey: "pid"
    });
    Product.hasMany(PurchaseOrderDetail, {
      foreignKey: "pid"
    });
    Product.hasMany(ReceiveOrderDetail, {
      foreignKey: "pid"
    });
    Product.hasMany(AdjustmentDetail, {
      foreignKey: "pid"
    });

    Product.hasMany(SupplierProduct, {
      foreignKey: "pid",
      sourceKey: "id"
    });
    Product.belongsToMany(Supplier, {
      through: {
        model: SupplierProduct,
        unique: false
      },
      foreignKey: "pid",
      constraints: true
    });
  };
  return Product;
};
