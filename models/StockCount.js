'use strict'
module.exports = (sequelize, DataTypes) => {
  var StockCount = sequelize.define('StockCount', {
    created_by: { //user_id J created
      type: DataTypes.INTEGER,
      field: 'created_by'
    },
    created_by_name: { //user_id J created
      type: DataTypes.STRING,
      field: 'created_by_name'
    },
    title: {
      type: DataTypes.STRING,
      field: 'title'
    },
    remark: {
      type: DataTypes.STRING,
      field: 'remark'
    },
    status: { //adjusted , pending
      type: DataTypes.STRING,
      field: 'status'
    },
    location_id: {
      type: DataTypes.INTEGER,
      field: 'location_id'
    },
    location_name: {
      type: DataTypes.STRING,
      field: 'location_name'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  },{
    freezeTableName: true,
    tableName: "ws_stock_count"
  });
  StockCount.associate = ({
    StockCountDetail,
    Adjustment
  }) => {
    StockCount.hasMany(StockCountDetail, {
      foreignKey: 'stock_count_id'
    });
    StockCount.hasOne(Adjustment, {
      foreignKey: 'scid'
    })
  }
  return StockCount;
}