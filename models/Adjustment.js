'use strict'
module.exports = (sequelize, DataTypes) => {
  var Adjustment = sequelize.define('Adjustment', {
    scid:{
      type: DataTypes.INTEGER,
      field: 'scid'
    },
    created_by: { //user_id J created
      type: DataTypes.INTEGER,
      field: 'created_by'
    },
    created_by_name: {
      type: DataTypes.STRING,
      field: 'created_by_name'
    },
    title: {
      type: DataTypes.STRING,
      field: 'title'
    },
    remark: {
      type: DataTypes.STRING,
      field: 'remark'
    },
    status: { //adjusted , pending
      type: DataTypes.STRING,
      field: 'status'
    },
    location_id: {
      type: DataTypes.INTEGER,
      field: 'location_id'
    },
    location_name: {
      type: DataTypes.STRING,
      field: 'location_name'
    },
    counted_by: { //user_id stock manger count
      type: DataTypes.INTEGER,
      field: 'counted_by'
    },
    counted_by_name: {
      type: DataTypes.STRING,
      field: 'counted_by_name'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  }, {
    freezeTableName: true,
    tableName: 'ws_adjustment'
  });
  Adjustment.associate = ({
    AdjustmentDetail,
    StockCount
  }) => {
    Adjustment.hasMany(AdjustmentDetail, {
      foreignKey: 'adjustment_id'
    });
    Adjustment.belongsTo(StockCount, {
      foreignKey: 'scid'
    });
  }
  return Adjustment;
}