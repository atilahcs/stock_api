'use strict';
module.exports = (sequelize, DataTypes) => {
  var AdminHistory = sequelize.define('AdminHistory', {
    admin_ps_id: {
      type: DataTypes.STRING,
      field: 'admin_ps_id'
    },
    admin_email: {
      type: DataTypes.STRING,
      field: 'admin_email'
    },
    admin_firstname: {
      type: DataTypes.STRING,
      field: 'admin_firstname'
    },
    admin_lastname: {
      type: DataTypes.STRING,
      field: 'admin_lastname'
    },
    admin_level: {
      type: DataTypes.INTEGER,
      field: 'admin_level'
    },
    agent: {
      type: DataTypes.STRING,
      field: 'agent'
    },
    action: {
      type: DataTypes.STRING,
      field: 'action'
    },
    created_time: {
      type: DataTypes.DATE,
      defaultValue: new Date(),
      field: 'created_time'
    },
    updated_time: {
      type: DataTypes.DATE,
      defaultValue: new Date(),
      field: 'updated_time'
    }
  }, {
      freezeTableName: true,
      tableName: 'ws_admin_history'
    });
  return AdminHistory;
};