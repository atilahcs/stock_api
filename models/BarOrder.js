'use strict';
module.exports = (sequelize, DataTypes) => {
  var BarOrder = sequelize.define('BarOrder', {
    bar_id: {
      type: DataTypes.INTEGER,
      field: 'bar_id'
    },
    bar_name: {
      type: DataTypes.STRING,
      field: 'bar_name'
    },
    address: {
      type: DataTypes.STRING,
      field: 'address'
    },
    tax_id: {
      type: DataTypes.STRING,
      field: 'tax_id'
    },
    contact_name: {
      type: DataTypes.STRING,
      field: 'contact_name'
    },
    contact_no: {
      type: DataTypes.STRING,
      field: 'contact_no'
    },
    order_date: {
      type: DataTypes.STRING,
      field: 'order_date',
      defaultValue: new Date()
    },
    payment_type: {
      type: DataTypes.STRING,
      field: 'payment_type'
    },
    due_date: {
      type: DataTypes.STRING,
      field: 'due_date'
    },
    credit_date: {
      type: DataTypes.STRING,
      field: 'credit_date'
    },
    expect_delivery_date: {
      type: DataTypes.STRING,
      field: 'expect_delivery_date'
    },
    total_price: {
      type: DataTypes.FLOAT,
      field: 'total_price'
    },
    inc_tax: {
      type: DataTypes.FLOAT,
      field: 'inc_tax'
    },
    final_price: {
      type: DataTypes.FLOAT,
      field: 'final_price'
    },
    internal_remark: {
      type: DataTypes.STRING,
      field: 'internal_remark'
    },
    remark: {
      type: DataTypes.STRING,
      field: 'remark'
    },
    status: {
      type: DataTypes.STRING,
      field: 'status'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  }, {
      freezeTableName: true,
      tableName: 'ws_bar_order'
    });
    BarOrder.associate = ({
      Homebar,
      BarOrderDetail
    }) => {
      BarOrder.belongsTo(Homebar, {
        foreignKey: 'bar_id'
      })
      BarOrder.hasMany(BarOrderDetail, {
        foreignKey: 'bar_order_id'
      })
    }
  return BarOrder;
};