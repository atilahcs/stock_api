'use strict';
module.exports = (sequelize, DataTypes) => {
  var PurchaseOrderLog = sequelize.define(
    'PurchaseOrderLog',
    {
      po_id: {
        type: DataTypes.INTEGER,
        field: 'po_id'
      },
      admin_id: {
        type: DataTypes.INTEGER,
        field: 'admin_id'
      },
      admin_name: {
        type: DataTypes.STRING,
        field: 'admin_name'
      },
      action: {
        type: DataTypes.STRING,
        field: 'action'
      },
      detail: {
        type: DataTypes.TEXT,
        field: 'detail'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_po_log"
    }
  );
  PurchaseOrderLog.associate = ({
    PurchaseOrder
  }) => {
    // associations can be defined here
    PurchaseOrderLog.belongsTo(PurchaseOrder, {
      foreignKey: "po_id"
    });
  };
  return PurchaseOrderLog;
}