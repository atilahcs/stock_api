"use strict";
module.exports = (sequelize, DataTypes) => {
  var ProductQuantity = sequelize.define(
    "ProductQuantity",
    {
      product_id: {
        type: DataTypes.INTEGER,
        field: "idprestashop"
      },
      ProductId: {
        type: DataTypes.INTEGER,
        field: "pid"
      },
      location_id: {
        type: DataTypes.INTEGER,
        field: "location_id"
      },
      location_name: {
        type: DataTypes.STRING,
        field: "location_name"
      },
      amount: {
        type: DataTypes.INTEGER,
        field: "amount"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_quantity"
    }
  );
  return ProductQuantity;
};
