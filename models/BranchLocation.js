"use strict";
module.exports = (sequelize, DataTypes) => {
  var BranchLocation = sequelize.define(
    "BranchLocation",
    {
      location_name: {
        type: DataTypes.STRING,
        field: 'location_name'
      },
      parent_id: {
        type: DataTypes.INTEGER,
        field: 'parent_id'
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_location"
    }
  );
  BranchLocation.associate = ({
    Product,
    ProductQuantity,
    Movement
  }) => {
    BranchLocation.belongsToMany(Product, {
      through: ProductQuantity,
      foreignKey: 'location_id'
    });
    BranchLocation.hasMany(Movement, {
      foreignKey: 'location_id'
    });
  }
  return BranchLocation;
};