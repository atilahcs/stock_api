"use strict";
module.exports = (sequelize, DataTypes) => {
  var OrderPrestashop = sequelize.define(
    "OrderPrestashop",
    {
      order_prestashop_id: {
        type: DataTypes.INTEGER,
        field: 'order_prestashop_id'
      },
      customer_id: {
        type: DataTypes.INTEGER,
        field: 'customer_id'
      },
      customer_firstname: {
        type: DataTypes.STRING,
        field: 'customer_firstname'
      },
      customer_lastname: {
        type: DataTypes.STRING,
        field: 'customer_lastname'
      },
      customer_email: {
        type: DataTypes.STRING,
        field: 'customer_email'
      },
      billing_address: {
        type: DataTypes.STRING,
        field: 'billing_address'
      },
      shipping_address: {
        type: DataTypes.STRING,
        field: 'shipping_address'
      },
      status: {
        type: DataTypes.STRING,
        field: 'status'
      },
      total_price: {
        type: DataTypes.FLOAT,
        field: 'total_price'
      },
      order_type: {
        type: DataTypes.STRING,
        field: 'order_type'
      },
      version_number: {
        type: DataTypes.INTEGER,
        field: 'version_number'
      },
      created_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'created_time'
      },
      updated_time: {
          type: DataTypes.DATE,
          defaultValue: new Date(),
          field: 'updated_time'
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_order_prestashop"
    }
  );
  OrderPrestashop.associate = ({
    OrderPrestashopDetail,
    Movement,
    OrderScanMovement,
    OrderScan,
    OrderScanVersion
  }) => {
    // associations can be defined here
    OrderPrestashop.hasMany(OrderPrestashopDetail, {
      foreignKey: "oid"
    });
    OrderPrestashop.hasMany(Movement, {
      foreignKey: "oid"
    });
    OrderPrestashop.hasMany(OrderScanMovement, {
      foreignKey: "oid"
    });
    OrderPrestashop.hasOne(OrderScan, {
      foreignKey: "oid"
    });
    OrderPrestashop.hasMany(OrderScanVersion, {
      foreignKey: "oid"
    });
  };
  return OrderPrestashop;
};
