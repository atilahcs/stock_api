"use strict";
module.exports = (sequelize, DataTypes) => {
  var PurchaseOrderDetail = sequelize.define(
    "PurchaseOrderDetail",
    {
      po_id: {
        type: DataTypes.INTEGER,
        field: "po_id"
      },
      supplier_id: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        field: "supplier_id"
      },
      supplier_name: {
        type: DataTypes.STRING,
        defaultValue: "",
        field: "supplier_name"
      },
      product_name: {
        type: DataTypes.STRING,
        defaultValue: "",
        field: "product_name"
      },
      product_barcode: {
        type: DataTypes.STRING,
        defaultValue: "",
        field: "product_barcode"
      },
      product_id: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        field: "product_id"
      },
      pid: {
        type: DataTypes.INTEGER,
        field: "pid"
      },
      product_unit: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        field: "product_unit"
      },
      product_unit_price: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        field: "product_unit_price"
      },
      product_unit_discount: {
        type: DataTypes.STRING,
        defaultValue: "",
        field: "product_unit_discount"
      },
      product_amount: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        field: "product_amount"
      },
      product_amount_after_discount: {
        type: DataTypes.FLOAT,
        defaultValue: 0,
        field: "product_amount_after_discount"
      },
      received_quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        field: "received_quantity"
      },
      created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_time"
      },
      updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_time"
      }
    },
    {
      freezeTableName: true,
      tableName: "ws_list_po"
    }
  );
  PurchaseOrderDetail.associate = ({ PurchaseOrder, Product }) => {
    // associations can be defined here
    PurchaseOrderDetail.belongsTo(PurchaseOrder, {
      foreignKey: "po_id"
    });
    PurchaseOrderDetail.belongsTo(Product, {
      foreignKey: "pid"
    });
  };
  return PurchaseOrderDetail;
};
