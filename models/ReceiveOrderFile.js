'use strict'
module.exports = (sequelize, DataTypes) => {
  var ReceiveOrderFile = sequelize.define("ReceiveOrderFile", {
    ro_id: {
      type: DataTypes.INTEGER,
      field: 'ro_id'
    },
    file_name: {
      type: DataTypes.STRING,
      field: 'file_name'
    },
    admin_id:{
      type: DataTypes.INTEGER,
      field: 'admin_id'
    },
    admin_name: {
      type: DataTypes.STRING,
      field: 'admin_name'
    },
    upload_date:{
      type: DataTypes.DATE,
      defaultValue: new Date(),
      field: 'upload_date'
    },
    created_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'created_time'
    },
    updated_time: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: 'updated_time'
    }
  },
    {
      freezeTableName: true,
      tableName: "ws_receiving_file_history"
    }
  );
  ReceiveOrderFile.associate = ({
    ReceiveOrder
  }) => {
    // associations can be defined here
    ReceiveOrderFile.belongsTo(ReceiveOrder, {
      foreignKey: 'ro_id'
    })
  };
  return ReceiveOrderFile;
}